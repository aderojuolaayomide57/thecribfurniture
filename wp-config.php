<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'thecrib' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '@v$53S9_ZbNDFPPtCA }X:LN#9N$x0/bQtZ^FWy}eUr7I~TIu690:2gOC!`O6FQ2' );
define( 'SECURE_AUTH_KEY',  '7IwSMatgxd;qO~lx@>Tl7z>{2)?vBix9W2n30B7.Bw}j>BH,$?lv*hTAuh7~<PUF' );
define( 'LOGGED_IN_KEY',    'M=e)SEJ[O2%?.5aGN=G-YNJGt32gru#@Huw~$Hk0Pi>%7#~,`LWc[Yq!)R~M:W+}' );
define( 'NONCE_KEY',        'L]y+w>?Ae#b Y;W]*sQxOs?M# >C>NLW:|a@DV)mS!oGU#]5i`f9Rw`S#0:f~i7T' );
define( 'AUTH_SALT',        'eX5/kaF}x4?YEr@K>$V@}W{2&FoA5{%+ajf/7O:rb;%t~<6[jw]eQgpoz)EMHaw>' );
define( 'SECURE_AUTH_SALT', 't]DyO^W !CDe{ot%c5gd3,AyEt8{E>0.UPy o$nib2%H/]zS7:GRgP5z{?xTsPZ|' );
define( 'LOGGED_IN_SALT',   'kS24U3sY33lhR-h)u#LIu>fs@nD=*sD;FiJ@71+dt]IZ_.T5VK:&3JSei/8 GOVl' );
define( 'NONCE_SALT',       '1N02aYBcP4uxT0*,8ud-=Q#fj<&@<HiC{]X=J7eNW[@@R0qu@*#QY<riGrrm]SC9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tcf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
