<?php

define( 'KONSEPT_ROOT', get_template_directory_uri() );
define( 'KONSEPT_ROOT_DIR', get_template_directory() );
define( 'KONSEPT_ASSETS_ROOT', KONSEPT_ROOT . '/assets' );
define( 'KONSEPT_ASSETS_ROOT_DIR', KONSEPT_ROOT_DIR . '/assets' );
define( 'KONSEPT_ASSETS_CSS_ROOT', KONSEPT_ASSETS_ROOT . '/css' );
define( 'KONSEPT_ASSETS_CSS_ROOT_DIR', KONSEPT_ASSETS_ROOT_DIR . '/css' );
define( 'KONSEPT_ASSETS_JS_ROOT', KONSEPT_ASSETS_ROOT . '/js' );
define( 'KONSEPT_ASSETS_JS_ROOT_DIR', KONSEPT_ASSETS_ROOT_DIR . '/js' );
define( 'KONSEPT_INC_ROOT', KONSEPT_ROOT . '/inc' );
define( 'KONSEPT_INC_ROOT_DIR', KONSEPT_ROOT_DIR . '/inc' );
