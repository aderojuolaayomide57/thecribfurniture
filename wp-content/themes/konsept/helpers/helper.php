<?php

if ( ! function_exists( 'konsept_is_installed' ) ) {
	/**
	 * Function that checks if forward plugin installed
	 *
	 * @param string $plugin - plugin name
	 *
	 * @return bool
	 */
	function konsept_is_installed( $plugin ) {
		
		switch ( $plugin ) {
			case 'framework';
				return class_exists( 'QodeFramework' );
				break;
			case 'core';
				return class_exists( 'KonseptCore' );
				break;
			case 'woocommerce';
				return class_exists( 'WooCommerce' );
				break;
			case 'gutenberg-page';
				$current_screen = function_exists( 'get_current_screen' ) ? get_current_screen() : array();
				
				return method_exists( $current_screen, 'is_block_editor' ) && $current_screen->is_block_editor();
				break;
			case 'gutenberg-editor':
				return class_exists( 'WP_Block_Type' );
				break;
			default:
				return false;
		}
	}
}

if ( ! function_exists( 'konsept_include_theme_is_installed' ) ) {
	/**
	 * Function that set case is installed element for framework functionality
	 *
	 * @param bool $installed
	 * @param string $plugin - plugin name
	 *
	 * @return bool
	 */
	function konsept_include_theme_is_installed( $installed, $plugin ) {
		
		if ( $plugin === 'theme' ) {
			return class_exists( 'KonseptHandler' );
		}
		
		return $installed;
	}
	
	add_filter( 'qode_framework_filter_is_plugin_installed', 'konsept_include_theme_is_installed', 10, 2 );
}

if ( ! function_exists( 'konsept_template_part' ) ) {
	/**
	 * Function that echo module template part.
	 *
	 * @param string $module name of the module from inc folder
	 * @param string $template full path of the template to load
	 * @param string $slug
	 * @param array  $params array of parameters to pass to template
	 */
	function konsept_template_part( $module, $template, $slug = '', $params = array() ) {
		echo konsept_get_template_part( $module, $template, $slug, $params );
	}
}

if ( ! function_exists( 'konsept_get_template_part' ) ) {
	/**
	 * Function that load module template part.
	 *
	 * @param string $module name of the module from inc folder
	 * @param string $template full path of the template to load
	 * @param string $slug
	 * @param array  $params array of parameters to pass to template
	 *
	 * @return string - string containing html of template
	 */
	function konsept_get_template_part( $module, $template, $slug = '', $params = array() ) {
		//HTML Content from template
		$html          = '';
		$template_path = KONSEPT_INC_ROOT_DIR . '/' . $module;
		
		$temp = $template_path . '/' . $template;
		if ( is_array( $params ) && count( $params ) ) {
			extract( $params );
		}
		
		$template = '';
		
		if ( ! empty( $temp ) ) {
			if ( ! empty( $slug ) ) {
				$template = "{$temp}-{$slug}.php";
				
				if ( ! file_exists( $template ) ) {
					$template = $temp . '.php';
				}
			} else {
				$template = $temp . '.php';
			}
		}
		
		if ( $template ) {
			ob_start();
			include( $template );
			$html = ob_get_clean();
		}
		
		return $html;
	}
}

if ( ! function_exists( 'konsept_get_page_id' ) ) {
	/**
	 * Function that returns current page id
	 * Additional conditional is to check if current page is any wp archive page (archive, category, tag, date etc.) and returns -1
	 *
	 * @return int
	 */
	function konsept_get_page_id() {
		$page_id = get_queried_object_id();
		
		if ( konsept_is_wp_template() ) {
			$page_id = -1;
		}
		
		return apply_filters( 'konsept_filter_page_id', $page_id );
	}
}

if ( ! function_exists( 'konsept_is_wp_template' ) ) {
	/**
	 * Function that checks if current page default wp page
	 *
	 * @return bool
	 */
	function konsept_is_wp_template() {
		return is_archive() || is_search() || is_404() || ( is_front_page() && is_home() );
	}
}

if ( ! function_exists( 'konsept_get_ajax_status' ) ) {
	/**
	 * Function that return status from ajax functions
	 *
	 * @param string $status - success or error
	 * @param string $message - ajax message value
	 * @param string|array $data - returned value
	 * @param string $redirect - url address
	 */
	function konsept_get_ajax_status( $status, $message, $data = null, $redirect = '' ) {
		$response = array(
			'status'   => esc_attr( $status ),
			'message'  => esc_html( $message ),
			'data'     => $data,
			'redirect' => ! empty( $redirect ) ? esc_url( $redirect ) : '',
		);
		
		$output = json_encode( $response );
		
		exit( $output );
	}
}

if ( ! function_exists( 'konsept_get_icon' ) ) {
	/**
	 * Function that return icon html
	 *
	 * @param string $icon - icon class name
	 * @param string $icon_pack - icon pack name
	 * @param string $backup_text - backup text label if framework is not installed
	 * @param array $params - icon parameters
	 *
	 * @return string|mixed
	 */
	function konsept_get_icon( $icon, $icon_pack, $backup_text, $params = array() ) {
		$value = konsept_is_installed( 'framework' ) && konsept_is_installed( 'core' ) ? qode_framework_icons()->render_icon( $icon, $icon_pack, $params ) : $backup_text;
		
		return $value;
	}
}

if ( ! function_exists( 'konsept_render_icon' ) ) {
	/**
	 * Function that render icon html
	 *
	 * @param string $icon - icon class name
	 * @param string $icon_pack - icon pack name
	 * @param string $backup_text - backup text label if framework is not installed
	 * @param array $params - icon parameters
	 */
	function konsept_render_icon( $icon, $icon_pack, $backup_text, $params = array() ) {
		echo konsept_get_icon( $icon, $icon_pack, $backup_text, $params );
	}
}

if ( ! function_exists( 'konsept_get_button_element' ) ) {
	/**
	 * Function that returns button with provided params
	 *
	 * @param array $params - array of parameters
	 *
	 * @return string - string representing button html
	 */
	function konsept_get_button_element( $params ) {
		if ( class_exists( 'KonseptCoreButtonShortcode' ) ) {
			return KonseptCoreButtonShortcode::call_shortcode( $params );
		} else {
			$link   = isset( $params['link'] ) ? $params['link'] : '#';
			$target = isset( $params['target'] ) ? $params['target'] : '_self';
			$text   = isset( $params['text'] ) ? $params['text'] : '';
			
			return '<a itemprop="url" class="qodef-theme-button" href="' . esc_url( $link ) . '" target="' . esc_attr( $target ) . '">' . esc_html( $text ) . '</a>';
		}
	}
}

if ( ! function_exists( 'konsept_render_button_element' ) ) {
	/**
	 * Function that render button with provided params
	 *
	 * @param array $params - array of parameters
	 */
	function konsept_render_button_element( $params ) {
		echo konsept_get_button_element( $params );
	}
}

if ( ! function_exists( 'konsept_get_social_share_element') ) {
    /**
     * Function that returns social share with provided params
     *
     * @param $params array - array of parameters
     *
     * @return string - string representing social share html
     */
    function konsept_get_social_share_element( $params ) {
        return KonseptCoreSocialShareShortcode::call_shortcode( $params );
    }
}

if ( ! function_exists( 'konsept_render_social_share_element' ) ) {
    /**
     * Function that render social share with provided params
     *
     * @param $params array - array of parameters
     */
    function konsept_render_social_share_element( $params ) {
        echo konsept_get_social_share_element( $params );
    }
}

if ( ! function_exists( 'konsept_class_attribute' ) ) {
	/**
	 * Function that render class attribute
	 *
	 * @param string|array $class
	 */
	function konsept_class_attribute( $class ) {
		echo konsept_get_class_attribute( $class );
	}
}

if ( ! function_exists( 'konsept_get_class_attribute' ) ) {
	/**
	 * Function that return class attribute
	 *
	 * @param string|array $class
	 *
	 * @return string|mixed
	 */
	function konsept_get_class_attribute( $class ) {
		$value = konsept_is_installed( 'framework' ) ? qode_framework_get_class_attribute( $class ) : '';
		
		return $value;
	}
}

if ( ! function_exists( 'konsept_get_post_value_through_levels' ) ) {
	/**
	 * Function that returns meta value if exists
	 *
	 * @param string $name name of option
	 * @param int    $post_id id of
	 *
	 * @return string value of option
	 */
	function konsept_get_post_value_through_levels( $name, $post_id = null ) {
		return konsept_is_installed( 'framework' ) && konsept_is_installed( 'core' ) ? konsept_core_get_post_value_through_levels( $name, $post_id ) : '';
	}
}

if ( ! function_exists( 'konsept_get_space_value' ) ) {
	/**
	 * Function that returns spacing value based on selected option
	 *
	 * @param string $text_value - textual value of spacing
	 *
	 * @return int
	 */
	function konsept_get_space_value( $text_value ) {
		return konsept_is_installed( 'core' ) ? konsept_core_get_space_value( $text_value ) : 0;
	}
}

if ( ! function_exists( 'konsept_wp_kses_html' ) ) {
	/**
	 * Function that does escaping of specific html.
	 * It uses wp_kses function with predefined attributes array.
	 *
	 * @see wp_kses()
	 *
	 * @param string $type - type of html element
	 * @param string $content - string to escape
	 *
	 * @return string escaped output
	 */
	function konsept_wp_kses_html( $type, $content ) {
		return konsept_is_installed( 'framework' ) ? qode_framework_wp_kses_html( $type, $content ) : $content;
	}
}

if ( ! function_exists( 'konsept_svg_icon' ) ) {
    /**
     * Function that echo svg html icon
     *
     * @param $name string - icon name
     * @param $class_name string - custom html tag class name
     */
    function konsept_svg_icon( $name, $class_name = '' ) {
        echo konsept_get_svg_icon( $name, $class_name );
    }
}

if ( ! function_exists( 'konsept_get_svg_icon' ) ) {
    /**
     * Returns svg html
     *
     * @param $name string - icon name
     * @param $class_name string - custom html tag class name
     *
     * @return string|html
     */
    function konsept_get_svg_icon( $name, $class_name = '' ) {
        $html  = '';
        $class = isset( $class_name ) && ! empty( $class_name ) ? $class_name : '';

        switch ( $name ) {
            case 'quote':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" width="137.982" height="125"><path class="qodef-quote-svg-blob" fill="#E6DEDC" d="M61.886 7.875S6.369-12.676 1.874 14.725c-4.496 27.4 7.492 121.593 56.016 107.322s62.795-37.105 50.235-68.503C95.566 22.145 75.158 11.87 61.886 7.875z"/><circle fill="none" stroke="#8B8B8B" stroke-miterlimit="10" cx="75.449" cy="61.993" r="61.4"/><path fill="#211F1F" d="M63.553 69.24c1.641-1.549 2.916-3.439 3.829-5.674a18.482 18.482 0 001.367-7.042c0-1.184-.137-2.369-.41-3.555l5.879-.957c.273 1.186.41 2.371.41 3.555 0 3.1-.73 6.132-2.188 9.093-1.459 2.963-3.374 5.4-5.743 7.314l-3.144-2.734zm12.716 0c1.641-1.549 2.917-3.439 3.829-5.674a18.482 18.482 0 001.367-7.042c0-1.184-.137-2.369-.41-3.555l5.879-.957c.273 1.186.41 2.371.41 3.555 0 3.1-.73 6.132-2.188 9.093-1.459 2.963-3.374 5.4-5.743 7.314l-3.144-2.734z"/></svg>';
                break;
            case 'link':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" width="137.983" height="125"><path fill="#D8E3E9" d="M61.886 7.874S6.369-12.677 1.874 14.724C-2.622 42.125 9.367 136.317 57.89 122.046s62.795-37.105 50.235-68.503C95.567 22.145 75.159 11.869 61.886 7.874z"/><g fill="none"><circle stroke="#8B8B8B" stroke-miterlimit="10" cx="75.449" cy="62.585" r="61.4"/><g stroke="#000" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"><path d="M73.449 63.585a5 5 0 007.54.54l3-3a5 5 0 00-7.069-7.07l-1.721 1.711"/><path d="M77.449 61.585a5 5 0 00-7.541-.54l-3 3a5 5 0 007.07 7.069l1.71-1.709"/></g></g></svg>';
                break;
            case 'arrow':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" width="10.749" height="19.997" viewBox="23.25 0 10.749 19.997"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="M33.232 19.181l-9.215-9.215L33.232.75"/><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="M33.232 19.181l-9.215-9.215L33.232.75"/></svg>';
                break;
            case 'search':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" width="17.977" height="17.686" viewBox="-4.311 -0.833 17.977 17.686"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"><circle cx="3.708" cy="7.13" r="7.348"/><path d="M12.891 16.313l-3.994-3.994"/></g></svg>';
                break;
            case 'heart':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" class="' . $class_name . '" x="0px" y="0px"	 width="17.5px" height="15.542px" viewBox="1.075 1.364 17.5 15.542" ><path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" d="M16.674,3.385	c-1.644-1.644-4.309-1.644-5.953,0c-0.001,0-0.001,0-0.001,0L9.907,4.198l-0.81-0.813c-1.645-1.644-4.313-1.644-5.956,0	c-1.645,1.644-1.645,4.312,0,5.957l0.812,0.81l5.954,5.956l5.956-5.956l0.811-0.81c1.644-1.645,1.646-4.311,0.002-5.955	C16.676,3.387,16.676,3.386,16.674,3.385z"/></svg>';
                break;
            case 'cart':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" class="' . $class_name . '" width="19.816" height="18" viewBox="51.878 -0.952 19.816 18"><g fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"><path d="M57.33-.197L54.843 3.12v11.61c0 .916.742 1.658 1.659 1.658h11.609c.917 0 1.659-.742 1.659-1.658V3.12L67.283-.197H57.33zM54.843 3.12h14.928M65.624 6.438a3.317 3.317 0 11-6.634 0"/></g></svg>';
                break;
            case 'user':
                $html = '
                <svg xmlns="http://www.w3.org/2000/svg" class="' . $class_name . '" width="15.331" height="17.478" viewBox="14.752 -0.625 15.331 17.478"><g fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"><path d="M29.488 16.051v-1.768a3.536 3.536 0 00-3.535-3.536h-7.071a3.536 3.536 0 00-3.535 3.536v1.768"/><circle cx="22.418" cy="3.677" r="3.535"/></g></svg>';
                break;
            case 'send':
                $html = '<svg xmlns="http://www.w3.org/2000/svg" width="21.188" height="17.25" viewBox="1.406 3.375 21.188 17.25"><g fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"/><path d="M22 6l-10 7L2 6"/></g></svg>';
                break;
            case 'shoppingbag':
                $html = '<svg xmlns="http://www.w3.org/2000/svg" class="' . $class_name . '" width="15.125px" height="16.563px" viewBox="1.563 4.813 15.125 16.563"><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="M4.634,5.51L2.346,8.562v10.679 c0,0.845,0.683,1.526,1.526,1.526h10.681c0.842,0,1.525-0.682,1.525-1.526V8.562L13.789,5.51H4.634z"/><line fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" x1="2.346" y1="8.562" x2="16.078" y2="8.562"/><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" d="M12.264,11.614 c0,1.686-1.366,3.051-3.051,3.051c-1.687,0-3.052-1.365-3.052-3.051"/></svg>';
                break;
        }

        return $html;
    }
}