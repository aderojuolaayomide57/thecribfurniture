1.1
- Added WordPress 5.5 compatibility
- Added WooCommerce 4.4.1 compatibility
- Updated Revolution Slider to 6.2.22
- Updated Konsept Core to 1.1
- Updated Qode Framework to 1.1.3
- Fixed bug with cut price numbers in Firefox

1.0.2

- Fixed membership plugin activation

1.0.1

- Minor style fixes

