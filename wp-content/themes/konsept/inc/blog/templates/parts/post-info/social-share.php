<?php
    $params = array(
        'layout' => 'list',
    );
?>
<?php if ( konsept_is_installed('core') ) { ?>
    <div class="qodef-e-info-item qodef-e-info-share">
        <?php konsept_render_social_share_element( $params ); ?>
    </div>
<?php } ?>
