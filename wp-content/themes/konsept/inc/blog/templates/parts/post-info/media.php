<div class="qodef-e-media">
	<?php switch ( get_post_format() ) {
		case 'gallery':
			konsept_template_part( 'blog', 'templates/parts/post-format/gallery' );
			break;
		case 'video':
			konsept_template_part( 'blog', 'templates/parts/post-format/video' );
			break;
		case 'audio':
			konsept_template_part( 'blog', 'templates/parts/post-format/audio' );
			break;
		default:
			konsept_template_part( 'blog', 'templates/parts/post-info/image' );
			break;
	} ?>
</div>