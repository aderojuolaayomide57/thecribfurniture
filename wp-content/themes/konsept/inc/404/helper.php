<?php

if ( ! function_exists( 'konsept_set_404_page_inner_classes' ) ) {
	/**
	 * Function that return classes for the page inner div from header.php
	 *
	 * @param string $classes
	 *
	 * @return string
	 */
	function konsept_set_404_page_inner_classes( $classes ) {
		
		if ( is_404() ) {
			$classes = 'qodef-content-full-width';
		}
		
		return $classes;
	}
	
	add_filter( 'konsept_filter_page_inner_classes', 'konsept_set_404_page_inner_classes' );
}

if ( ! function_exists( 'konsept_get_404_page_parameters' ) ) {
	/**
	 * Function that set 404 page area content parameters
	 */
	function konsept_get_404_page_parameters() {
		
		$params = array(
			'title'       => esc_html__( 'Error Page', 'konsept' ),
			'text'        => esc_html__( 'The page you are looking for doesn\'t exist. It may have been moved or removed altogether. Please try searching for some other page, or return to the website\'s homepage to find what you\'re looking for.', 'konsept' ),
			'button_text' => esc_html__( 'Back to home', 'konsept' ),
		);
		
		return apply_filters( 'konsept_filter_404_page_template_params', $params );
	}
}
