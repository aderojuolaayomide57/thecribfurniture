<?php

if ( ! function_exists( 'konsept_include_comments_in_templates' ) ) {
	/**
	 * Function which includes comments templates on pages/posts
	 */
	function konsept_include_comments_in_templates() {

		// Include comments template
		comments_template();
	}

	add_action( 'konsept_action_after_page_content', 'konsept_include_comments_in_templates', 40 ); // permission 40 is set to comments template to define template position
	add_action( 'konsept_action_after_blog_post_item', 'konsept_include_comments_in_templates', 40 );
}

if ( ! function_exists( 'konsept_is_page_comments_enabled' ) ) {
	/**
	 * Function that check is module enabled
	 */
	function konsept_is_page_comments_enabled() {
		$is_enabled = apply_filters( 'konsept_filter_enable_page_comments', true );

		return $is_enabled;
	}
}

if ( ! function_exists( 'konsept_load_page_comments' ) ) {
	/**
	 * Function which loads page template module
	 */
	function konsept_load_page_comments() {

		if ( konsept_is_page_comments_enabled() ) {
			konsept_template_part( 'comments', 'templates/comments' );
		}
	}

	add_action( 'konsept_action_page_comments_template', 'konsept_load_page_comments' );
}

if ( ! function_exists( 'konsept_get_comments_list_template' ) ) {
	/**
	 * Function which modify default wordpress comments list template
	 *
	 * @param object $comment
	 * @param array $args
	 * @param int $depth
	 *
	 * @return string that contains comments list html
	 */
	function konsept_get_comments_list_template( $comment, $args, $depth ) {
		global $post;
		$GLOBALS['comment'] = $comment;

		$classes = array();

		$is_author_comment = $post->post_author == $comment->user_id;
		if ( $is_author_comment ) {
			$classes[] = 'qodef-comment--author';
		}

		$is_specific_comment = $comment->comment_type == 'pingback' || $comment->comment_type == 'trackback';
		if ( $is_specific_comment ) {
			$classes[] = 'qodef-comment--no-avatar';
			$classes[] = 'qodef-comment--' . esc_attr( $comment->comment_type );
		}
		?>
    <li class="qodef-comment-item qodef-e <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
        <div id="comment-<?php comment_ID(); ?>" class="qodef-e-inner">
			<?php if ( ! $is_specific_comment ) { ?>
                <div class="qodef-e-image">
                    <?php
                    $avatarargs = array(
                        'width' => '80',
                        'height' => '100'
                    );

                    echo get_avatar( $comment, 80, '', '', $avatarargs); ?>
                </div>
			<?php } ?>
            <div class="qodef-e-content">
                <div class="qodef-e-links">
                    <?php
                    comment_reply_link( array_merge( $args, array(
                        'reply_text' => esc_html__( 'Reply', 'konsept' ),
                        'depth'      => $depth,
                        'max_depth'  => $args['max_depth'],
                    ) ) );

                    edit_comment_link( esc_html__( 'Edit', 'konsept' ) ); ?>
                </div>
                <div class="qodef-e-date commentmetadata">
                    <a href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>"><?php comment_time( get_option( 'date_format' ) ); ?></a>
                </div>
                <h4 class="qodef-e-title vcard"><?php echo sprintf( '<span class="fn">%s%s</span>', $is_specific_comment ? sprintf( '%s: ', esc_attr( ucwords( $comment->comment_type ) ) ) : '', get_comment_author_link() ); ?></h4>
				<?php if ( ! $is_specific_comment ) { ?>
                    <div class="qodef-e-text"><?php comment_text(); ?></div>
				<?php } ?>
            </div>
        </div>
		<?php //li tag will be closed by WordPress after looping through child elements ?>
		<?php
	}
}

if ( ! function_exists( 'konsept_get_comment_form_args' ) ) {
    /**
     * Function that define new comment form args in order to override default wordpress comment form
     *
     * @param array $attr - additional array which override default values
     *
     * @return array
     */
    function konsept_get_comment_form_args( $attr = array()) {
	    $qodef_commenter      = wp_get_current_commenter();
	    $qodef_required_attr  = get_option( 'require_name_email' ) ? ' required="required"' : '';
	    $qodef_required_label = get_option( 'require_name_email' ) ? '*' : '';
	    
	    $comment_placeholder = isset( $attr['comment_placeholder'] ) && ! empty( $attr['comment_placeholder'] ) ? esc_attr( $attr['comment_placeholder'] ) : esc_attr__( 'Your Comment *', 'konsept' );
	    $grid_cols = isset( $attr['grid_cols'] ) && ! empty( $attr['grid_cols'] ) ? esc_attr( $attr['grid_cols'] ) : 3;

	    $args = array(
		    'title_reply_before' => '<h3 id="reply-title" class="comment-reply-title">',
		    'title_reply_after'  => '</h3>',
		    'comment_field'      => '<p class="comment-form-comment">
                                    <textarea id="comment" name="comment" placeholder="' . $comment_placeholder . '" cols="45" rows="8" maxlength="65525" required="required"></textarea>
                                    </p>',
		    'fields'             => array(
		        'grid_open'     => '<div class="qodef-grid qodef-layout--columns qodef-gutter--small qodef-col-num--' . $grid_cols . '"><div class="qodef-grid-inner">',
			    'author'        => '<div class="qodef-grid-item"><p class="comment-form-author">
                                    <input id="author" name="author" placeholder="' . sprintf( '%1s%2s', esc_attr__( 'Your Name', 'konsept' ), esc_attr( $qodef_required_label ) ) . '" type="text" value="' . esc_attr( $qodef_commenter['comment_author'] ) . '" size="30" maxlength="245" ' . $qodef_required_attr . ' />
                                    </p></div>',
			    'email'         => '<div class="qodef-grid-item"><p class="comment-form-email">
                                    <input id="email" name="email" placeholder="' . sprintf( '%1s%2s', esc_attr__( 'Your Email', 'konsept' ), esc_attr( $qodef_required_label ) ) . '" type="text" value="' . esc_attr( $qodef_commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes" ' . $qodef_required_attr . ' />
                                    </p></div>',
			    'url'           => '<div class="qodef-grid-item"><p class="comment-form-url">
                                    <input id="url" name="url" placeholder="' . esc_attr__( 'Website', 'konsept' ) . '" type="text" value="' . esc_attr( $qodef_commenter['comment_author_url'] ) . '" size="30" maxlength="200" />
                                    </p></div>',
                'grid_close'    => '</div></div>',
		    ),
		    'submit_button'      => '<button name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s"><span class="qodef-m-text">%4$s</span></button>',
		    'class_submit'       => 'qodef-button qodef-layout--outlined',
		    'class_form'         => 'qodef-comment-form',
	    );
	    
	    return apply_filters( 'konsept_filter_comment_form_args', $args );
    }
}