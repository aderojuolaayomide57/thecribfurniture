<?php

if ( ! function_exists( 'konsept_load_page_mobile_header' ) ) {
	/**
	 * Function which loads page template module
	 */
	function konsept_load_page_mobile_header() {
		// Include mobile header template
		echo apply_filters( 'konsept_filter_mobile_header_template', konsept_get_template_part( 'mobile-header', 'templates/mobile-header' ) );
	}
	
	add_action( 'konsept_action_page_header_template', 'konsept_load_page_mobile_header' );
}

if ( ! function_exists( 'konsept_register_mobile_navigation_menus' ) ) {
	/**
	 * Function which registers navigation menus
	 */
	function konsept_register_mobile_navigation_menus() {
		$navigation_menus = apply_filters( 'konsept_filter_register_mobile_navigation_menus', array( 'mobile-navigation' => esc_html__( 'Mobile Navigation', 'konsept' ) ) );
		
		if ( ! empty( $navigation_menus ) ) {
			register_nav_menus( $navigation_menus );
		}
	}
	
	add_action( 'konsept_action_after_include_modules', 'konsept_register_mobile_navigation_menus' );
}