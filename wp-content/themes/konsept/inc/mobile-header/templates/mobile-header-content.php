<div id="qodef-page-mobile-header-inner-wrapper">
    <?php
// Include mobile logo
konsept_template_part( 'mobile-header', 'templates/parts/mobile-logo' );

// Include mobile navigation opener
konsept_template_part( 'mobile-header', 'templates/parts/mobile-navigation-opener' ); ?>
</div>
<?php
// Include mobile navigation
konsept_template_part( 'mobile-header', 'templates/parts/mobile-navigation' ); ?>