<?php

if ( ! function_exists( 'konsept_membership_get_dashboard_navigation_pages' ) ) {
	/**
	 * Function that return main dashboard page navigation items
	 *
	 * @return array
	 */
	function konsept_membership_get_dashboard_navigation_pages() {
		$dashboard_url = konsept_membership_get_dashboard_page_url();
		
		$items = array(
			'profile'      => array(
				'url'         => esc_url( add_query_arg( array( 'user-action' => 'profile' ), $dashboard_url ) ),
				'text'        => esc_html__( 'Profile', 'konsept-membership' ),
				'user_action' => 'profile',
				'icon'        => '<svg xmlns="http://www.w3.org/2000/svg" width="15.331" height="17.478" viewBox="14.752 -0.625 15.331 17.478"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round"><path d="M29.488 16.051v-1.768a3.536 3.536 0 00-3.535-3.536h-7.071a3.536 3.536 0 00-3.535 3.536v1.768"/><circle cx="22.418" cy="3.677" r="3.535"/></g></svg>'
			),
			'edit-profile' => array(
				'url'         => esc_url( add_query_arg( array( 'user-action' => 'edit-profile' ), $dashboard_url ) ),
				'text'        => esc_html__( 'Edit Profile', 'konsept-membership' ),
				'user_action' => 'edit-profile',
				'icon'        => konsept_get_icon( 'ion-ios-color-wand', 'ionicons', ''),
			),
			'log-out' => array(
				'url'         => wp_logout_url( konsept_membership_get_membership_redirect_url() ),
				'text'        => esc_html__( 'Log Out', 'konsept-membership' ),
				'user_action' => 'log-out',
				'icon'        => konsept_get_icon( 'ion-ios-log-out', 'ionicons', ''),
			)
		);
		
		$items = apply_filters( 'konsept_membership_filter_dashboard_navigation_pages', $items, $dashboard_url );

		return $items;
	}
}

if ( ! function_exists( 'konsept_membership_get_dashboard_pages' ) ) {
	/**
	 * Function that return content for main dashboard page item
	 *
	 * @return string that contains html of content
	 */
	function konsept_membership_get_dashboard_pages() {
		$action = isset( $_GET['user-action'] ) && ! empty( $_GET['user-action'] ) ? sanitize_text_field( $_GET['user-action'] ) : 'profile';
		
		$params = array();
		if ( $action == 'profile' || $action == 'edit-profile' ) {
			$params = konsept_membership_get_user_params( $action );
		}
		
		switch ( $action ) {
			case 'profile':
				$html = konsept_membership_get_template_part( 'general', 'page-templates/parts/profile', '', $params );
				break;
			case 'edit-profile':
				$html = konsept_membership_get_template_part( 'general', 'page-templates/parts/edit-profile', '', $params );
				break;
			default:
				$html = konsept_membership_get_template_part( 'general', 'page-templates/parts/profile', '', $params );
				break;
		}
		
		return apply_filters( 'konsept_membership_filter_dashboard_page', $html, $action );
	}
}

if ( ! function_exists( 'konsept_membership_get_user_params' ) ) {
	/**
	 * Function that return user attributes for main dashboard page
	 *
	 * @param string $action
	 *
	 * @return array
	 */
	function konsept_membership_get_user_params( $action ) {
		$params = array();

		$user = wp_get_current_user();
		$user_id                 = $user->data->ID;

		$params['user']    		 = $user;
		$params['first_name']    = get_the_author_meta( 'first_name', $user_id );
		$params['last_name']     = get_the_author_meta( 'last_name', $user_id );
		$params['email']         = get_the_author_meta( 'user_email', $user_id );
		$params['website']       = get_the_author_meta( 'user_url', $user_id );
		$params['description']   = get_the_author_meta( 'description', $user_id );
		$params['profile_image'] = get_avatar( $user_id, 96 );
		$params['action']        = $action;

		return apply_filters( 'konsept_membership_filter_user_params', $params );
	}
}

if ( ! function_exists( 'konsept_membership_add_rest_api_update_user_meta_global_variables' ) ) {
	/**
	 * Extend main rest api variables with new case
	 *
	 * @param array $global - list of variables
	 * @param string $namespace - rest namespace url
	 *
	 * @return array
	 */
	function konsept_membership_add_rest_api_update_user_meta_global_variables( $global, $namespace ) {
		$global['updateUserRestRoute'] = $namespace . '/edit-profile';

		return $global;
	}

	add_filter( 'qode_framework_filter_rest_api_global_variables', 'konsept_membership_add_rest_api_update_user_meta_global_variables', 10, 2 );
}

if ( ! function_exists( 'konsept_membership_add_rest_api_update_user_meta_route' ) ) {
	/**
	 * Extend main rest api routes with new case
	 *
	 * @param array $routes - list of rest routes
	 *
	 * @return array
	 */
	function konsept_membership_add_rest_api_update_user_meta_route( $routes ) {
		$routes['edit-profile'] = array(
			'route'    => 'edit-profile',
			'methods'  => WP_REST_Server::CREATABLE,
			'callback' => 'konsept_membership_update_user_profile',
			'args'     => array(
				'options' => array(
					'required'          => true,
					'validate_callback' => function ( $param, $request, $key ) {
						// Simple solution for validation can be 'is_array' value instead of callback function
						return is_array( $param ) ? $param : (array) $param;
					},
					'description'       => esc_html__( 'Options data is array with reaction and id values', 'konsept-membership' )
				)
			)
		);

		return $routes;
	}

	add_filter( 'qode_framework_filter_rest_api_routes', 'konsept_membership_add_rest_api_update_user_meta_route' );
}

if ( ! function_exists( 'konsept_membership_update_user_profile' ) ) {
	/**
	 * Function that update user profile
	 */
	function konsept_membership_update_user_profile() {
		
		if ( ! isset( $_POST['options'] ) || empty( $_POST['options'] ) || ! is_user_logged_in() ) {
			qode_framework_get_ajax_status( 'error', esc_html__( 'You are not authorized.', 'konsept-core' ) );
		} else {
			$options = isset( $_POST['options'] ) ? $_POST['options'] : array();

			if ( ! empty( $options ) ) {
				parse_str( $options, $options );
	
				$user_id = get_current_user_id();
				
				if ( ! empty( $user_id ) ) {
					$user_fields = array();
					
					if ( isset( $options['user_password'] ) && ! empty( $options['user_password'] ) ) {
						if ( $options['user_password'] === $options['user_confirm_password'] ) {
							$user_fields['user_pass'] = esc_attr( $options['user_password'] );
						} else {
							qode_framework_get_ajax_status( 'error', esc_html__( 'Password and confirm password doesn\'t match.', 'konsept-membership' ) );
						}
					}
					
					if ( isset( $options['user_email'] ) && ! empty( $options['user_email'] ) ) {
						
						if ( ! is_email( $options['user_email'] ) ) {
							qode_framework_get_ajax_status( 'error', esc_html__( 'Please provide a valid email address.', 'konsept-membership' ) );
						}
						
						$current_user_object = get_user_by( 'email', $options['user_email'] );
						if ( ! empty( $current_user_object ) && $current_user_object->ID !== $user_id && email_exists( $options['user_email'] ) ) {
							qode_framework_get_ajax_status( 'error', esc_html__( 'An account is already registered with this email address. Please fill another one.', 'konsept-membership' ) );
						} else {
							$user_fields['user_email'] = sanitize_email( $options['user_email'] );
						}
					}
					
					$simple_fields = array(
						'first_name'  => array(
							'escape' => 'attr'
						),
						'last_name'   => array(
							'escape' => 'attr'
						),
						'user_url'    => array(
							'escape' => 'url'
						),
						'description' => array(
							'escape' => 'attr'
						)
					);
					
					foreach ( $simple_fields as $key => $value ) {
						if ( isset( $options[ $key ] ) && ! empty( $options[ $key ] ) ) {
							$escape = 'esc_' . $value['escape'];
							
							$user_fields[ $key ] = $escape( $options[ $key ] );
						}
					}

					do_action( 'konsept_membership_action_update_user_profile', $options, $user_id );
					
					if ( ! empty( $user_fields ) ) {
						wp_update_user( array_merge(
							array( 'ID' => $user_id ),
							$user_fields
						) );
						
						qode_framework_get_ajax_status( 'success', esc_html__( 'Your profile is successfully updated.', 'konsept-membership' ), null, konsept_membership_get_membership_redirect_url() );
					} else {
						qode_framework_get_ajax_status( 'error', esc_html__( 'Change your information in order to update your profile.', 'konsept-membership' ) );
					}
				} else {
					qode_framework_get_ajax_status( 'error', esc_html__( 'You are unauthorized to perform this action.', 'konsept-membership' ) );
				}
			} else {
				qode_framework_get_ajax_status( 'error', esc_html__( 'Data are invalid.', 'konsept-membership' ) );
			}
		}
	}
}