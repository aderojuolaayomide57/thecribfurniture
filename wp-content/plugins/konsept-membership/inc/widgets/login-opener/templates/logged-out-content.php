<a href="#" class="qodef-login-opener">
    <span class="qodef-login-opener-icon"></span>
	<span class="qodef-login-opener-text"><?php esc_html_e( 'Login / Register', 'konsept-membership' ); ?></span>
</a>