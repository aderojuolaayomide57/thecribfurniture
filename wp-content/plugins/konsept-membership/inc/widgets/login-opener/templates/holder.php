<?php

if ( is_user_logged_in() ) {
	konsept_membership_template_part( 'widgets/login-opener', 'templates/logged-in-content' );
} else {
	konsept_membership_template_part( 'widgets/login-opener', 'templates/logged-out-content' );
}