<?php

define( 'KONSEPT_CORE_VERSION', '1.1' );
define( 'KONSEPT_CORE_ABS_PATH', dirname( __FILE__ ) );
define( 'KONSEPT_CORE_REL_PATH', dirname( plugin_basename( __FILE__ ) ) );
define( 'KONSEPT_CORE_URL_PATH', plugin_dir_url( __FILE__ ) );
define( 'KONSEPT_CORE_ASSETS_PATH', KONSEPT_CORE_ABS_PATH . '/assets' );
define( 'KONSEPT_CORE_ASSETS_URL_PATH', KONSEPT_CORE_URL_PATH . 'assets' );
define( 'KONSEPT_CORE_INC_PATH', KONSEPT_CORE_ABS_PATH . '/inc' );
define( 'KONSEPT_CORE_INC_URL_PATH', KONSEPT_CORE_URL_PATH . 'inc' );
define( 'KONSEPT_CORE_CPT_PATH', KONSEPT_CORE_INC_PATH . '/post-types' );
define( 'KONSEPT_CORE_CPT_URL_PATH', KONSEPT_CORE_INC_URL_PATH . '/post-types' );
define( 'KONSEPT_CORE_SHORTCODES_PATH', KONSEPT_CORE_INC_PATH . '/shortcodes' );
define( 'KONSEPT_CORE_SHORTCODES_URL_PATH', KONSEPT_CORE_INC_URL_PATH . '/shortcodes' );
define( 'KONSEPT_CORE_PLUGINS_PATH', KONSEPT_CORE_INC_PATH . '/plugins' );
define( 'KONSEPT_CORE_PLUGINS_URL_PATH', KONSEPT_CORE_INC_URL_PATH . '/plugins' );
define( 'KONSEPT_CORE_HEADER_LAYOUTS_PATH', KONSEPT_CORE_INC_PATH . '/header/layouts' );
define( 'KONSEPT_CORE_HEADER_LAYOUTS_URL_PATH', KONSEPT_CORE_INC_URL_PATH . '/header/layouts' );
define( 'KONSEPT_CORE_HEADER_ASSETS_PATH', KONSEPT_CORE_INC_PATH . '/header/assets' );
define( 'KONSEPT_CORE_HEADER_ASSETS_URL_PATH', KONSEPT_CORE_INC_URL_PATH . '/header/assets' );

define( 'KONSEPT_CORE_MENU_NAME', 'konsept_core_menu' );
define( 'KONSEPT_CORE_OPTIONS_NAME', 'konsept_core_options' );

define( 'KONSEPT_CORE_PROFILE_SLUG', 'elated' );