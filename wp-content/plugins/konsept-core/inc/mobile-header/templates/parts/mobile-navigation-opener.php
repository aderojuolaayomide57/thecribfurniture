<a class="qodef-mobile-header-opener" href="#">
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="28.229px" height="13.188px" viewBox="0 0 28.229 13.188" enable-background="new 0 0 28.229 13.188" xml:space="preserve">
        <line fill="none" stroke="#1D1D1D" stroke-miterlimit="10" x1="0.122" y1="0.552" x2="17.059" y2="0.552"/>
        <line fill="none" stroke="#1D1D1D" stroke-miterlimit="10" x1="18.934" y1="0.552" x2="28.087" y2="0.552"/>
        <line fill="none" stroke="#1D1D1D" stroke-miterlimit="10" x1="28.087" y1="6.589" x2="11.149" y2="6.589"/>
        <line fill="none" stroke="#1D1D1D" stroke-miterlimit="10" x1="28.087" y1="12.625" x2="0.122" y2="12.625"/>
        <line fill="none" stroke="#1D1D1D" stroke-miterlimit="10" x1="9.274" y1="6.589" x2="0.122" y2="6.589"/>
    </svg>
</a>