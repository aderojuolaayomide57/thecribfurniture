<?php

if ( ! function_exists( 'konsept_core_add_cube_spinner_layout_option' ) ) {
	/**
	 * Function that set new value into page spinner layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function konsept_core_add_cube_spinner_layout_option( $layouts ) {
		$layouts['cube'] = esc_html__( 'Cube', 'konsept-core' );
		
		return $layouts;
	}
	
	add_filter( 'konsept_core_filter_page_spinner_layout_options', 'konsept_core_add_cube_spinner_layout_option' );
}