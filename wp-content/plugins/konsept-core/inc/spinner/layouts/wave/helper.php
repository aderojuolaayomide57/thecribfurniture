<?php

if ( ! function_exists( 'konsept_core_add_wave_spinner_layout_option' ) ) {
	/**
	 * Function that set new value into page spinner layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function konsept_core_add_wave_spinner_layout_option( $layouts ) {
		$layouts['wave'] = esc_html__( 'Wave', 'konsept-core' );
		
		return $layouts;
	}
	
	add_filter( 'konsept_core_filter_page_spinner_layout_options', 'konsept_core_add_wave_spinner_layout_option' );
}