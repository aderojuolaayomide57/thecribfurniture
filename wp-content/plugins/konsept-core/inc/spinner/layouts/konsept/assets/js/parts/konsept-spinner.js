(function ($) {
	"use strict";

	$(document).ready(function () {
		qodefKonseptSpinner.init();
	});

	$(window).on('elementor/frontend/init', function () {
		var isEditMode = Boolean(elementorFrontend.isEditMode());
		if (isEditMode) {
			qodefKonseptSpinner.init(isEditMode);
		}
	});

	var qodefKonseptSpinner = {
		init: function (isEditMode) {
			this.holder = $('#qodef-page-spinner.qodef-layout--konsept');

			if (this.holder.length) {
				qodefKonseptSpinner.animateSpinner(this.holder, isEditMode);
			}
		},
		animateSpinner: function ($holder, isEditMode) {
			var $letter = $holder.find('.qodef-m-konsept-text span'),
				tl = new TimelineMax({ repeat: -1, repeatDelay: 0 })

			tl.staggerFromTo($letter, 1, {
				opacity: 0,
				y: -10,
			}, {
				opacity: 1,
				y: 0,
				ease: Power3.easeInOut
			}, .3);
			tl.staggerTo($letter, 1, {
				opacity: 0,
				delay: 2,
				ease: Power2.easeInOut
			}, .05);

			isEditMode && qodefKonseptSpinner.finishAnimation($holder);

			$(window).on('load', function () {
				tl.eventCallback("onRepeat", function () {
					tl.pause().kill();
					qodefKonseptSpinner.finishAnimation($holder);
				});
			});
		},
		finishAnimation: function ($holder) {
			qodefKonseptSpinner.fadeOutLoader($holder);
			setTimeout(function () {
				var landingRev = $('#qodef-landing-rev').find('rs-module');
				landingRev.length && landingRev.revstart();
			}, 300);
		},
		fadeOutLoader: function ($holder, speed, delay, easing) {
			speed = speed ? speed : 600;
			delay = delay ? delay : 0;
			easing = easing ? easing : 'linear';

			$holder.delay(delay).fadeOut(speed, easing);

			$(window).on('bind', 'pageshow', function (event) {
				if (event.originalEvent.persisted) {
					$holder.fadeOut(speed, easing);
				}
			});
		}
	};

})(jQuery);