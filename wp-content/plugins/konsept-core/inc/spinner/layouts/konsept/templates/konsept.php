<?php
    $qodef_spinner_text = konsept_core_get_post_value_through_levels( 'qodef_spinner_text', qode_framework_get_page_id() );
?>

<div class="qodef-m-konsept">
    <span class="qodef-m-konsept-text">
    <?php 
    $strArray = str_split($qodef_spinner_text);
    foreach($strArray as $item):
        echo '<span>'.$item.'</span>';
    endforeach;
    ?>
    </span>
    <svg class="qodef-m-konsept-svg" xmlns="http://www.w3.org/2000/svg" width="88.209" height="63.333"><path fill="#CCD9DC" d="M83.191 45.15c1.604-2.132 4.816-5.928 4.577-6.16C76.846 28.389 65.611 18.097 54.417 7.765c-.729 2.784-1.463 5.566-2.196 8.345 3.033 12.193 6.063 24.387 9.099 36.573 7.287-2.511 14.581-5.02 21.871-7.533z"/><path fill="#F8EEE4" d="M45.547 46.024c.038-10.976 1.199-22.098-.173-32.893C43.771.488 35.598-2.825 24.243 3.24c-6.228 3.324-12.157 7.199-18.219 10.834-1.744 2.266-3.488 4.54-5.232 6.813 2.668 8.434 2.993 19.242 8.564 24.735 7.932 7.828 19.709 11.761 29.842 17.361.931-1.894 2.055-3.722 2.76-5.691 1.318-3.712 2.404-7.512 3.589-11.268z"/></svg>
</div>