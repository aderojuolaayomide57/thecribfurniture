<?php

if ( ! function_exists( 'konsept_core_add_konsept_spinner_layout_option' ) ) {
	/**
	 * Function that set new value into page spinner layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function konsept_core_add_konsept_spinner_layout_option( $layouts ) {
		$layouts['konsept'] = esc_html__( 'Konsept', 'konsept-core' );
		
		return $layouts;
	}
	
	add_filter( 'konsept_core_filter_page_spinner_layout_options', 'konsept_core_add_konsept_spinner_layout_option' );
}

if ( ! function_exists( 'konsept_core_set_konsept_spinner_layout_as_default_option' ) ) {
	/**
	 * Function that set default value for page spinner layout options map
	 *
	 * @param string $default_value
	 *
	 * @return string
	 */
	function konsept_core_set_konsept_spinner_layout_as_default_option( $default_value ) {
		return 'konsept';
	}
	
	add_filter( 'konsept_core_filter_page_spinner_default_layout_option', 'konsept_core_set_konsept_spinner_layout_as_default_option' );
}