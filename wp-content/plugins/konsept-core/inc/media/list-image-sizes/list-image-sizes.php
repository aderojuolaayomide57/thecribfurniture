<?php

if ( ! function_exists( 'konsept_core_add_list_image_sizes' ) ) {
	function konsept_core_add_list_image_sizes( $image_sizes ) {
		$image_sizes[] = array(
			'slug'           => 'konsept_image_size_square',
			'label'          => esc_html__( 'Square Size', 'konsept-core' ),
			'label_simple'   => esc_html__( 'Square', 'konsept-core' ),
			'default_crop'   => true,
			'default_width'  => 650,
			'default_height' => 650
		);
		
		$image_sizes[] = array(
			'slug'           => 'konsept_image_size_landscape',
			'label'          => esc_html__( 'Landscape Size', 'konsept-core' ),
			'label_simple'   => esc_html__( 'Landscape', 'konsept-core' ),
			'default_crop'   => true,
			'default_width'  => 1300,
			'default_height' => 650
		);
		
		$image_sizes[] = array(
			'slug'           => 'konsept_image_size_portrait',
			'label'          => esc_html__( 'Portrait Size', 'konsept-core' ),
			'label_simple'   => esc_html__( 'Portrait', 'konsept-core' ),
			'default_crop'   => true,
			'default_width'  => 650,
			'default_height' => 1300
		);
		
		$image_sizes[] = array(
			'slug'           => 'konsept_image_size_huge-square',
			'label'          => esc_html__( 'Huge Square Size', 'konsept-core' ),
			'label_simple'   => esc_html__( 'Huge Square', 'konsept-core' ),
			'default_crop'   => true,
			'default_width'  => 1300,
			'default_height' => 1300
		);

        $image_sizes[] = array(
            'slug'           => 'konsept_image_size_huge-landscape',
            'label'          => esc_html__( 'Huge Landscape Size', 'konsept-core' ),
            'label_simple'   => esc_html__( 'Huge Landscape', 'konsept-core' ),
            'default_crop'   => true,
            'default_width'  => 1950,
            'default_height' => 1300
        );
		
		return $image_sizes;
	}
	
	add_filter( 'qode_framework_filter_populate_image_sizes', 'konsept_core_add_list_image_sizes' );
}

if ( ! function_exists( 'konsept_core_add_pool_masonry_list_image_sizes' ) ) {
	function konsept_core_add_pool_masonry_list_image_sizes( $options, $type ) {
		if ( $type == 'masonry_image_dimension' ) {
			$options['konsept_image_size_square']      = esc_html__( 'Square', 'konsept-core' );
			$options['konsept_image_size_landscape']   = esc_html__( 'Landscape', 'konsept-core' );
			$options['konsept_image_size_portrait']    = esc_html__( 'Portrait', 'konsept-core' );
			$options['konsept_image_size_huge-square'] = esc_html__( 'Huge Square', 'konsept-core' );
			$options['konsept_image_size_huge-landscape'] = esc_html__( 'Huge Landscape', 'konsept-core' );
		}
		
		return $options;
	}
	
	add_filter( 'konsept_core_filter_select_type_option', 'konsept_core_add_pool_masonry_list_image_sizes', 10, 2 );
}

if ( ! function_exists( 'konsept_core_get_custom_image_size_class_name' ) ) {
	function konsept_core_get_custom_image_size_class_name( $image_size ) {
		return ! empty( $image_size ) ? 'qodef-item--' . str_replace( 'konsept_image_size_', '', $image_size ) : '';
	}
}

if ( ! function_exists( 'konsept_core_get_custom_image_size_meta' ) ) {
	function konsept_core_get_custom_image_size_meta( $type, $name, $post_id ) {
		$image_size_meta  = qode_framework_get_option_value( '', $type, $name, '', $post_id );
		$image_size       = ! empty( $image_size_meta ) ? esc_attr( $image_size_meta ) : 'full';

		return array(
			'size'  => $image_size,
			'class' => konsept_core_get_custom_image_size_class_name( $image_size )
		);
	}
}