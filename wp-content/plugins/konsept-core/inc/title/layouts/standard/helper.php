<?php

if ( ! function_exists( 'konsept_core_register_standard_title_layout' ) ) {
	function konsept_core_register_standard_title_layout( $layouts ) {
		$layouts['standard'] = 'KonseptCoreStandardTitle';
		
		return $layouts;
	}
	
	add_filter( 'konsept_core_filter_register_title_layouts', 'konsept_core_register_standard_title_layout');
}

if ( ! function_exists( 'konsept_core_add_standard_title_layout_option' ) ) {
	/**
	 * Function that set new value into title layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function konsept_core_add_standard_title_layout_option( $layouts ) {
		$layouts['standard'] = esc_html__( 'Standard', 'konsept-core' );
		
		return $layouts;
	}
	
	add_filter( 'konsept_core_filter_title_layout_options', 'konsept_core_add_standard_title_layout_option' );
}

if ( ! function_exists( 'konsept_core_get_standard_title_layout_subtitle_text' ) ) {
	/**
	 * Function that render current page subtitle text
	 */
	function konsept_core_get_standard_title_layout_subtitle_text() {
		$subtitle_meta = konsept_core_get_post_value_through_levels( 'qodef_page_title_subtitle' );
		$subtitle      = array( 'subtitle' => ! empty( $subtitle_meta ) ? $subtitle_meta : '' );
		
		return apply_filters( 'konsept_core_filter_standard_title_layout_subtitle_text', $subtitle );
	}
}
