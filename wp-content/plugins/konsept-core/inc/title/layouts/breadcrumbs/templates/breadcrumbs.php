<?php
// Load title image template
konsept_core_get_page_title_image(); ?>
<div class="qodef-m-content <?php echo esc_attr( konsept_core_get_page_title_content_classes() ); ?>">
	<?php
	// Load breadcrumbs template
	konsept_core_breadcrumbs(); ?>
</div>

