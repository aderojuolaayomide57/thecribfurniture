<?php

if ( ! function_exists( 'konsept_core_register_standard_with_breadcrumbs_title_layout' ) ) {
	function konsept_core_register_standard_with_breadcrumbs_title_layout( $layouts ) {
		$layouts['standard-with-breadcrumbs'] = 'KonseptCoreStandardWithBreadcrumbsTitle';

		return $layouts;
	}

	add_filter( 'konsept_core_filter_register_title_layouts', 'konsept_core_register_standard_with_breadcrumbs_title_layout' );
}

if ( ! function_exists( 'konsept_core_add_standard_with_breadcrumbs_title_layout_option' ) ) {
	/**
	 * Function that set new value into title layout options map
	 *
	 * @param array $layouts  - module layouts
	 *
	 * @return array
	 */
	function konsept_core_add_standard_with_breadcrumbs_title_layout_option( $layouts ) {
		$layouts['standard-with-breadcrumbs'] = esc_html__( 'Standard with breadcrumbs', 'konsept-core' );

		return $layouts;
	}

	add_filter( 'konsept_core_filter_title_layout_options', 'konsept_core_add_standard_with_breadcrumbs_title_layout_option' );
}

