<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/accordion/accordion.php';
include_once KONSEPT_CORE_SHORTCODES_PATH . '/accordion/accordion-child.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/accordion/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}