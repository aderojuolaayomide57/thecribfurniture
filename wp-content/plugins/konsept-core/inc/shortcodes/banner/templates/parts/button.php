<?php if ( class_exists( 'KonseptCoreButtonShortcode' ) ) { ?>
	<div class="qodef-m-button">
		<?php echo KonseptCoreButtonShortcode::call_shortcode( $button_params ); ?>
	</div>
<?php } ?>