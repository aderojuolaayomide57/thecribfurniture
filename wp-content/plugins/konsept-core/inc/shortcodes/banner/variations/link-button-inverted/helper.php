<?php

if ( ! function_exists( 'konsept_core_add_banner_variation_link_button_inverted' ) ) {
    function konsept_core_add_banner_variation_link_button_inverted( $variations ) {

        $variations['link-button-inverted'] = esc_html__( 'Link Button Inverted', 'konsept-core' );

        return $variations;
    }

    add_filter( 'konsept_core_filter_banner_layouts', 'konsept_core_add_banner_variation_link_button_inverted' );
}
