<div <?php qode_framework_class_attribute( $holder_classes ); ?>>
	<?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/image', '', $params ) ?>
	<div class="qodef-m-content">
		<div class="qodef-m-content-inner">
            <div class="qodef-m-content-holder">
                <?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/subtitle', '', $params ) ?>
                <?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/title', '', $params ) ?>
                <?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/text', '', $params ) ?>
                <?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/button', '', $params ) ?>
                </div>
            <?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/tagline', '', $params ) ?>
		</div>
    </div>
    <?php konsept_core_template_part( 'shortcodes/banner', 'templates/parts/link', '', $params ) ?>
</div>