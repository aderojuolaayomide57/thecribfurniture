<?php

if ( ! function_exists( 'konsept_core_add_banner_variation_link_button' ) ) {
    function konsept_core_add_banner_variation_link_button( $variations ) {

        $variations['link-button'] = esc_html__( 'Link Button', 'konsept-core' );

        return $variations;
    }

    add_filter( 'konsept_core_filter_banner_layouts', 'konsept_core_add_banner_variation_link_button' );
}