<?php

if ( ! function_exists( 'konsept_core_add_banner_variation_link_overlay' ) ) {
	function konsept_core_add_banner_variation_link_overlay( $variations ) {
		
		$variations['link-overlay'] = esc_html__( 'Link Overlay', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_banner_layouts', 'konsept_core_add_banner_variation_link_overlay' );
}
