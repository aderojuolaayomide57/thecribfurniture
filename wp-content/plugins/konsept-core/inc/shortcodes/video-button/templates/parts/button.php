<?php if ( ! empty( $video_link ) ) { ?>
	<a itemprop="url" class="qodef-m-play qodef-magnific-popup qodef-popup-item" <?php echo qode_framework_get_inline_style( $play_button_styles ); ?> href="<?php echo esc_url( $video_link ); ?>" data-type="iframe">
		<span class="qodef-m-play-inner">
			<svg class="qodef-svg-circle"><circle cx="50%" cy="50%" r="50%"></circle><circle cx="50%" cy="50%" r="50%"></circle></svg>
            <span class="qodef-m-play-square"></span> <span><?php echo esc_html('Play','konsept-core'); ?></span>
		</span>
	</a>
<?php } ?>