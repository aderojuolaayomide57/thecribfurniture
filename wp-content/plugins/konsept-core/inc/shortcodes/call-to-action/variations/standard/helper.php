<?php

if ( ! function_exists( 'konsept_core_add_call_to_action_variation_standard' ) ) {
	function konsept_core_add_call_to_action_variation_standard( $variations ) {
		
		$variations['standard'] = esc_html__( 'Standard', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_call_to_action_layouts', 'konsept_core_add_call_to_action_variation_standard' );
}
