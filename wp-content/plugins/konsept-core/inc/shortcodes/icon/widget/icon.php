<?php

if ( ! function_exists( 'konsept_core_add_icon_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_icon_widget( $widgets ) {
		$widgets[] = 'KonseptCoreIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_icon_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreIconWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'konsept_core_icon'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'konsept_core_icon' );
				$this->set_name( esc_html__( 'Konsept Icon', 'konsept-core' ) );
				$this->set_description( esc_html__( 'Add a icon element into widget areas', 'konsept-core' ) );
			}
		}
		
		public function render( $atts ) {
			
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[konsept_core_icon $params]" ); // XSS OK
		}
	}
}
