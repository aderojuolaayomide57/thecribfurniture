<?php if ( ! empty( $title ) ) { ?>
	<div class="qodef-m-title">
		<h3 <?php qode_framework_inline_style( $title_styles ); ?>><?php echo esc_html( $title ); ?></h3>
	</div>
<?php } ?>