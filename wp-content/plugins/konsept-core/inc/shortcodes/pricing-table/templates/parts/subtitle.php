<?php if ( ! empty( $subtitle ) ) { ?>
	<div class="qodef-m-subtitle">
		<span><?php echo esc_html( $subtitle ); ?></span>
	</div>
<?php } ?>