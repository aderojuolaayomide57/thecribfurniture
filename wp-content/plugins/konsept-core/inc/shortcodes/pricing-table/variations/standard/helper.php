<?php

if ( ! function_exists( 'konsept_core_add_pricing_table_variation_standard' ) ) {
	function konsept_core_add_pricing_table_variation_standard( $variations ) {
		
		$variations['standard'] = esc_html__( 'Standard', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_pricing_table_layouts', 'konsept_core_add_pricing_table_variation_standard' );
}
