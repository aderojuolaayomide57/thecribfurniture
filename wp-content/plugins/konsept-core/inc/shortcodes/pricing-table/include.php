<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/pricing-table/pricing-table.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/pricing-table/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}