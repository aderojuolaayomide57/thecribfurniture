<?php

if ( ! function_exists( 'konsept_core_add_image_with_text_variation_text_below' ) ) {
	function konsept_core_add_image_with_text_variation_text_below( $variations ) {
		
		$variations['text-below'] = esc_html__( 'Text Below', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_image_with_text_layouts', 'konsept_core_add_image_with_text_variation_text_below' );
}
