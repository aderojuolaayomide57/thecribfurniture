<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/tabs/tab.php';
include_once KONSEPT_CORE_SHORTCODES_PATH . '/tabs/tab-child.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/tabs/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}