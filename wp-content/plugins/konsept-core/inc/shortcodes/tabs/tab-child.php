<?php

if ( ! function_exists( 'konsept_core_add_tabs_child_shortcode' ) ) {
	/**
	 * Function that add shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes
	 *
	 * @return array
	 */
	function konsept_core_add_tabs_child_shortcode( $shortcodes ) {
		$shortcodes[] = 'KonseptCoreTabsChildShortcode';
		
		return $shortcodes;
	}
	
	add_filter( 'konsept_core_filter_register_shortcodes', 'konsept_core_add_tabs_child_shortcode' );
}

if ( class_exists( 'KonseptCoreShortcode' ) ) {
	class KonseptCoreTabsChildShortcode extends KonseptCoreShortcode {
		
		public function map_shortcode() {
			$this->set_shortcode_path( KONSEPT_CORE_SHORTCODES_URL_PATH . '/tabs' );
			$this->set_base( 'konsept_core_tabs_child' );
			$this->set_name( esc_html__( 'Tabs Child', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Shortcode that adds tab child to tabs holder', 'konsept-core' ) );
			$this->set_category( esc_html__( 'Konsept Core', 'konsept-core' ) );
			$this->set_is_child_shortcode( true );
			$this->set_parent_elements( array(
				'konsept_core_tabs'
			) );
			$this->set_is_parent_shortcode( true );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'tab_title',
				'title'      => esc_html__( 'Title', 'konsept-core' ),
			) );
			$this->set_option( array(
				'field_type'    => 'text',
				'name'          => 'layout',
				'title'         => esc_html__( 'Layout', 'konsept-core' ),
				'default_value' => '',
				'visibility'    => array('map_for_page_builder' => false)
			) );
		}
		
		public function render( $options, $content = null ) {
			parent::render( $options );
			$atts = $this->get_atts();
			
			$atts['tab_title'] = $atts['tab_title'] . '-' . wp_unique_id();
			$atts['content']   = $content;

			return konsept_core_get_template_part( 'shortcodes/tabs', 'variations/'.$atts['layout'].'/templates/child', '', $atts );
		}
	}
}