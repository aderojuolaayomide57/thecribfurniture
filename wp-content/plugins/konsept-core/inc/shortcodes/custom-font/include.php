<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/custom-font/custom-font.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/custom-font/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}