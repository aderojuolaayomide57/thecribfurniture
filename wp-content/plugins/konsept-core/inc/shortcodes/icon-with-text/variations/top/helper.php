<?php

if ( ! function_exists( 'konsept_core_add_icon_with_text_variation_top' ) ) {
	function konsept_core_add_icon_with_text_variation_top( $variations ) {
		
		$variations['top'] = esc_html__( 'Top', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_icon_with_text_layouts', 'konsept_core_add_icon_with_text_variation_top' );
}

if ( ! function_exists( 'konsept_core_add_icon_with_text_options_text_align' ) ) {
	function konsept_core_add_icon_with_text_options_text_align( $options, $default_layout ) {
		$icon_with_text_options   = array();
		
		$alignment_option = array(
			'field_type' => 'select',
			'name'       => 'content_alignment',
			'title'      => esc_html__( 'Content Alignment', 'konsept-core' ),
			'options'       => array(
				''       => esc_html__( 'Default', 'konsept-core' ),
				'left'   => esc_html__( 'Left', 'konsept-core' ),
				'center' => esc_html__( 'Center', 'konsept-core' ),
				'right'  => esc_html__( 'Right', 'konsept-core' )
			),
			'dependency' => array(
				'show' => array(
					'layout' => array(
						'values'        => 'top',
						'default_value' => $default_layout
					)
				)
			),
			'group'      => esc_html__( 'Content', 'konsept-core' )
		);
		
		$icon_with_text_options[] = $alignment_option;
		
		return array_merge( $options, $icon_with_text_options );
	}
	
	add_filter( 'konsept_core_filter_icon_with_text_extra_options', 'konsept_core_add_icon_with_text_options_text_align', 10, 2 );
}

if ( ! function_exists( 'konsept_core_add_icon_with_text_classes_alignment' ) ) {
	
	function konsept_core_add_icon_with_text_classes_alignment( $holder_classes, $atts ) {
		
		if( $atts['layout'] == 'top' ) {
			$holder_classes[] = ! empty( $atts['content_alignment'] ) ?  'qodef-alignment--' . $atts['content_alignment'] : 'qodef-alignment--center';
		}
		
		return $holder_classes;
	}
	
	add_filter( 'konsept_core_filter_icon_with_text_variation_classes', 'konsept_core_add_icon_with_text_classes_alignment', 10, 2 );
}