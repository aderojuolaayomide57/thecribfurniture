<?php if ( ! empty( $title ) ) { ?>
	<<?php echo esc_attr( $title_tag ); ?> class="qodef-m-title" <?php qode_framework_inline_style( $title_styles ); ?>>
        <span class="qodef-m-title-inner">
            <span class="qodef-m-icon-wrapper">
                <?php konsept_core_template_part( 'shortcodes/icon-with-text/variations/before-title', 'templates/parts/' . $icon_type, '', $params ) ?>
            </span>
            <span class="qodef-m-title-text">
                <?php echo esc_html( $title ); ?>
            </span>
        </span>
	</<?php echo esc_attr( $title_tag ); ?>>
<?php } ?>