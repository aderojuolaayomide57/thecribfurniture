<?php

if ( ! function_exists( 'konsept_core_add_icon_with_text_variation_before_title' ) ) {
	function konsept_core_add_icon_with_text_variation_before_title( $variations ) {
		
		$variations['before-title'] = esc_html__( 'Before Title', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_icon_with_text_layouts', 'konsept_core_add_icon_with_text_variation_before_title' );
}
