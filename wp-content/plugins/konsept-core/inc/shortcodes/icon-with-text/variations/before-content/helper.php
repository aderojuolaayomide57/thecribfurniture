<?php

if ( ! function_exists( 'konsept_core_add_icon_with_text_variation_before_content' ) ) {
	function konsept_core_add_icon_with_text_variation_before_content( $variations ) {
		
		$variations['before-content'] = esc_html__( 'Before Content', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_icon_with_text_layouts', 'konsept_core_add_icon_with_text_variation_before_content' );
}
