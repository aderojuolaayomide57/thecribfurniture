<?php if ( ! empty( $button_text ) ) { ?>
    <?php if ( ! empty( $link ) ) : ?>
        <a class="qodef-m-link qodef-button qodef-layout--textual" itemprop="url" href="<?php echo esc_url( $link ); ?>" target="<?php echo esc_attr( $target ); ?>">
    <?php endif; ?>
	        <span class="qodef-m-text" <?php qode_framework_inline_style( $button_styles ); ?>><?php echo esc_html( $button_text ); ?></span>
    <?php if ( ! empty( $link ) ) : ?>
        </a>
    <?php endif; ?>
<?php } ?>