<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/countdown/countdown.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/countdown/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}