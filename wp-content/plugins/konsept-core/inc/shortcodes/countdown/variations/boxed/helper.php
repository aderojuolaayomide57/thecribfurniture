<?php

if ( ! function_exists( 'konsept_core_add_countdown_variation_boxed' ) ) {
	function konsept_core_add_countdown_variation_boxed( $variations ) {
		
		$variations['boxed'] = esc_html__( 'Boxed', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_countdown_layouts', 'konsept_core_add_countdown_variation_boxed' );
}
if ( ! function_exists( 'konsept_core_add_countdown_variation_exclude_boxed' ) ) {
	function konsept_core_add_countdown_variation_exclude_boxed( $variations ) {

		unset($variations['boxed']);

		return $variations;
	}

	add_filter( 'konsept_core_filter_countdown_options_map_layouts', 'konsept_core_add_countdown_variation_exclude_boxed' );
}
