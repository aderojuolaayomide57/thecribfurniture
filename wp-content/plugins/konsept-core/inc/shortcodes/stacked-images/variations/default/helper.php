<?php

if ( ! function_exists( 'konsept_core_add_stacked_images_variation_default' ) ) {
	function konsept_core_add_stacked_images_variation_default( $variations ) {
		$variations['default'] = esc_html__( 'Default', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_stacked_images_layouts', 'konsept_core_add_stacked_images_variation_default' );
}
