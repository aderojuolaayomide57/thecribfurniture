<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/stacked-images/stacked-images.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/stacked-images/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}