<?php

if ( ! function_exists( 'konsept_core_add_button_variation_filled' ) ) {
	function konsept_core_add_button_variation_filled( $variations ) {
		
		$variations['filled'] = esc_html__( 'Filled', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_button_layouts', 'konsept_core_add_button_variation_filled' );
}
