<?php

if ( ! function_exists( 'konsept_core_add_button_variation_textual' ) ) {
	function konsept_core_add_button_variation_textual( $variations ) {
		
		$variations['textual'] = esc_html__( 'Textual', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_button_layouts', 'konsept_core_add_button_variation_textual' );
}
