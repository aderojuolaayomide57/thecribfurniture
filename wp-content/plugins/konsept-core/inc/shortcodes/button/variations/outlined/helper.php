<?php

if ( ! function_exists( 'konsept_core_add_button_variation_outlined' ) ) {
	function konsept_core_add_button_variation_outlined( $variations ) {
		
		$variations['outlined'] = esc_html__( 'Outlined', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_button_layouts', 'konsept_core_add_button_variation_outlined' );
}
