(function ($) {
	"use strict";

	qodefCore.shortcodes.konsept_core_button = {};

	$(document).ready(function () {
		qodefButton.init();
		qodefButton.otherOutlineButtons();
	});

	$('#yith-quick-view-content').on('DOMNodeInserted', function () {
		qodefButton.otherOutlineButtons();
	});

	$(document).on('wc_cart_button_updated', function () {
		qodefButton.otherOutlineButtons();
	});

	var qodefButton = {
		init: function () {
			this.buttons = $('.qodef-button');

			if (this.buttons.length) {
				this.buttons.each(function () {
					var $thisButton = $(this);

					qodefButton.buttonHoverColor($thisButton);
					qodefButton.buttonHoverBgColor($thisButton);
					qodefButton.buttonHoverBorderColor($thisButton);
					qodefButton.buttonOutlineAnimation($thisButton);
				});
			}
		},
		buttonHoverColor: function ($button) {
			if (typeof $button.data('hover-color') !== 'undefined') {
				var hoverColor = $button.data('hover-color');
				var originalColor = $button.css('color');

				$button.on('mouseenter', function () {
					qodefButton.changeColor($button, 'color', hoverColor);
				});
				$button.on('mouseleave', function () {
					qodefButton.changeColor($button, 'color', originalColor);
				});
			}
		},
		buttonHoverBgColor: function ($button) {
			if (typeof $button.data('hover-background-color') !== 'undefined') {
				var hoverBackgroundColor = $button.data('hover-background-color');
				var originalBackgroundColor = $button.css('background-color');

				$button.on('mouseenter', function () {
					qodefButton.changeColor($button, 'background-color', hoverBackgroundColor);
				});
				$button.on('mouseleave', function () {
					qodefButton.changeColor($button, 'background-color', originalBackgroundColor);
				});
			}
		},
		buttonHoverBorderColor: function ($button) {
			if (typeof $button.data('hover-border-color') !== 'undefined' && !$button.hasClass('qodef-layout--outlined')) {
				var hoverBorderColor = $button.data('hover-border-color');
				var originalBorderColor = $button.css('borderTopColor');

				$button.on('mouseenter', function () {
					qodefButton.changeColor($button, 'border-color', hoverBorderColor);
				});
				$button.on('mouseleave', function () {
					qodefButton.changeColor($button, 'border-color', originalBorderColor);
				});
			}
		},
		changeColor: function ($button, cssProperty, color) {
			$button.css(cssProperty, color);
		},
		buttonOutlineAnimation: function ($button) {
			if ($button.hasClass('qodef-layout--outlined')) {
				var borderColor = $button.data('border-color'),
					borderHoverColor = $button.data('hover-border-color');

				qodefButton.appendRectSVG($button);

				setTimeout(function () {
					$button.css('border-color', borderColor);
					$button.find('span:not(.qodef-m-text)').css('background-color', borderHoverColor);
					$button.addClass('qodef-layout--outlined-animated');
				}, 10);
			}
		},
		otherOutlineButtons: function () {
			var $buttons = $('.single_add_to_cart_button, .qodef-pvd .button, .qodef-pvd .added_to_cart, #qodef-membership-login-modal .qodef-m-action-button');

			if ($buttons.length) {
				$buttons.each(function () {
					var $button = $(this);
					if (!$button.hasClass('qodef-layout--outlined-animated')) {
						$button.addClass('qodef-layout--outlined-animated');
						qodefButton.appendRectSVG($button);
					}
				});
			}
		},
		appendRectSVG: function ($element) {
			$element.prepend('<span class="qodef-top-border"></span><span class="qodef-right-border"></span><span class="qodef-bottom-border"></span><span class="qodef-left-border"></span>');
		}
	};

	qodefCore.shortcodes.konsept_core_button.qodefButton = qodefButton;


})(jQuery);