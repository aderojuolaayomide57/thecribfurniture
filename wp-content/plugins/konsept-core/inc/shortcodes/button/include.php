<?php

include_once KONSEPT_CORE_SHORTCODES_PATH . '/button/button.php';

foreach ( glob( KONSEPT_CORE_SHORTCODES_PATH . '/button/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}