<?php if ($image_frame === 'yes') { ?>
<div class="qodef-image-gallery-with-frame">
<img class="qodef-image-frame" src="<?php echo KONSEPT_CORE_SHORTCODES_URL_PATH ?>/image-gallery/assets/img/image_frame.png" alt="<?php esc_html_e('Image Frame', 'konsept-core') ?>" />
<?php } ?>

<div <?php qode_framework_class_attribute( $holder_classes ); ?> <?php qode_framework_inline_attr( $slider_attr, 'data-options' ); ?>>
	<div class="swiper-wrapper">
			<?php
			// Include items
			if ( ! empty( $images ) ) {
				foreach ( $images as $image ) {
					$image['item_classes'] = $item_classes;
					$image['image_action'] = $image_action;
					$image['target']       = $target;
					
					konsept_core_template_part( 'shortcodes/image-gallery', 'templates/parts/image', '', $image );
				}
			}
			?>
	</div>
	<?php if ( $slider_navigation !== 'no' ) { ?>
		<div class="swiper-button-next"><?php konsept_svg_icon('arrow'); ?></div>
		<div class="swiper-button-prev"><?php konsept_svg_icon('arrow'); ?></div>
	<?php } ?>
	<?php if ( $slider_pagination !== 'no' ) { ?>
		<div class="swiper-pagination"></div>
	<?php } ?>
</div>
<?php if ($image_frame === 'yes') { ?>
</div>
<?php } ?>