(function ($) {
	"use strict";
	qodefCore.shortcodes.konsept_core_image_gallery = {};
	qodefCore.shortcodes.konsept_core_image_gallery.qodefSwiper = qodef.qodefSwiper;
	qodefCore.shortcodes.konsept_core_image_gallery.qodefMasonryLayout = qodef.qodefMasonryLayout;

    $(window).on('load', function(){
        qodefImageGallery.init();
    });

    var qodefImageGallery = {
        init: function () {
            var $holder = $('.qodef-image-gallery.qodef-swiper-container.qodef-swiper--initialized');

            if ($holder.length) {
                $holder.each(function () {
                    var $thisHolder = $(this);

                    // Custom Pagination Functionality
                    if ($thisHolder.hasClass('qodef-swiper-has-bullets')) {
                        qodefImageGallery.pagination($thisHolder);
                    }
                });
            }
        },
        pagination: function ($holder) {
            var swiperInstance = $holder[0].swiper,
                holderData = holderData = JSON.parse($holder.attr('data-options')),
                slidesPerView = parseInt(holderData.slidesPerView),
                $bullets = $holder.find('.swiper-pagination .swiper-pagination-bullet');

            if (!isNaN(slidesPerView)) {
                $bullets.filter(':nth-child(' + slidesPerView + 'n+1)').addClass('qodef-pagination-bullet--isVisible');
                $bullets.not('.qodef-pagination-bullet--isVisible').css('display', 'none');

                swiperInstance.on('slideChange', function () {
                    var activePagIndex = $bullets.filter('.swiper-pagination-bullet-active').index(),
                        calcedPagIndex = Math.floor(activePagIndex / slidesPerView);
                    $bullets.removeClass('qodef-pagination-bullet--isActive');
                    $bullets.filter('.qodef-pagination-bullet--isVisible').eq(calcedPagIndex).addClass('qodef-pagination-bullet--isActive');
                });
            }
        }
    }

})(jQuery);