<?php

if ( ! function_exists( 'konsept_core_add_fonts_options' ) ) {
	/**
	 * Function that add options for this module
	 */
	function konsept_core_add_fonts_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => KONSEPT_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'fonts',
				'title'       => esc_html__( 'Fonts', 'konsept-core' ),
				'description' => esc_html__( 'Global Fonts Options', 'konsept-core' ),
				'icon'        => 'fa fa-cog'
			)
		);

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_enable_google_fonts',
					'title'         => esc_html__( 'Enable Google Fonts', 'konsept-core' ),
					'default_value' => 'yes',
					'args'          => array(
						'custom_class' => 'qodef-enable-google-fonts'
					)
				)
			);

			$google_fonts_section = $page->add_section_element(
				array(
					'name'       => 'qodef_google_fonts_section',
					'title'      => esc_html__( 'Google Fonts Options', 'konsept-core' ),
					'dependency' => array(
						'show' => array(
							'qodef_enable_google_fonts' => array(
								'values'        => 'yes',
								'default_value' => ''
							)
						)
					)
				)
			);

			$page_repeater = $google_fonts_section->add_repeater_element(
				array(
					'name'        => 'qodef_choose_google_fonts',
					'title'       => esc_html__( 'Google Fonts to Include', 'konsept-core' ),
					'description' => esc_html__( 'Choose Google Fonts which you want to use on your website', 'konsept-core' ),
					'button_text' => esc_html__( 'Add New Google Font', 'konsept-core' )
				)
			);

			$page_repeater->add_field_element( array(
				'field_type'  => 'googlefont',
				'name'        => 'qodef_choose_google_font',
				'title'       => esc_html__( 'Google Font', 'konsept-core' ),
				'description' => esc_html__( 'Choose Google Font', 'konsept-core' ),
				'args'        => array(
					'include' => 'google-fonts'
				)
			) );

			$google_fonts_section->add_field_element(
				array(
					'field_type'  => 'checkbox',
					'name'        => 'qodef_google_fonts_weight',
					'title'       => esc_html__( 'Google Fonts Weight', 'konsept-core' ),
					'description' => esc_html__( 'Choose a default Google Fonts weights for your website. Impact on page load time', 'konsept-core' ),
					'options'     => array(
						'100'  => esc_html__( '100 Thin', 'konsept-core' ),
						'100i' => esc_html__( '100 Thin Italic', 'konsept-core' ),
						'200'  => esc_html__( '200 Extra-Light', 'konsept-core' ),
						'200i' => esc_html__( '200 Extra-Light Italic', 'konsept-core' ),
						'300'  => esc_html__( '300 Light', 'konsept-core' ),
						'300i' => esc_html__( '300 Light Italic', 'konsept-core' ),
						'400'  => esc_html__( '400 Regular', 'konsept-core' ),
						'400i' => esc_html__( '400 Regular Italic', 'konsept-core' ),
						'500'  => esc_html__( '500 Medium', 'konsept-core' ),
						'500i' => esc_html__( '500 Medium Italic', 'konsept-core' ),
						'600'  => esc_html__( '600 Semi-Bold', 'konsept-core' ),
						'600i' => esc_html__( '600 Semi-Bold Italic', 'konsept-core' ),
						'700'  => esc_html__( '700 Bold', 'konsept-core' ),
						'700i' => esc_html__( '700 Bold Italic', 'konsept-core' ),
						'800'  => esc_html__( '800 Extra-Bold', 'konsept-core' ),
						'800i' => esc_html__( '800 Extra-Bold Italic', 'konsept-core' ),
						'900'  => esc_html__( '900 Ultra-Bold', 'konsept-core' ),
						'900i' => esc_html__( '900 Ultra-Bold Italic', 'konsept-core' )
					)
				)
			);

			$google_fonts_section->add_field_element(
				array(
					'field_type'  => 'checkbox',
					'name'        => 'qodef_google_fonts_subset',
					'title'       => esc_html__( 'Google Fonts Style', 'konsept-core' ),
					'description' => esc_html__( 'Choose a default Google Fonts style for your website. Impact on page load time', 'konsept-core' ),
					'options'     => array(
						'latin'        => esc_html__( 'Latin', 'konsept-core' ),
						'latin-ext'    => esc_html__( 'Latin Extended', 'konsept-core' ),
						'cyrillic'     => esc_html__( 'Cyrillic', 'konsept-core' ),
						'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'konsept-core' ),
						'greek'        => esc_html__( 'Greek', 'konsept-core' ),
						'greek-ext'    => esc_html__( 'Greek Extended', 'konsept-core' ),
						'vietnamese'   => esc_html__( 'Vietnamese', 'konsept-core' )
					)
				)
			);

			$page_repeater = $page->add_repeater_element(
				array(
					'name'        => 'qodef_custom_fonts',
					'title'       => esc_html__( 'Custom Fonts', 'konsept-core' ),
					'description' => esc_html__( 'Add custom fonts', 'konsept-core' ),
					'button_text' => esc_html__( 'Add New Custom Font', 'konsept-core' )
				)
			);

			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_ttf',
				'title'      => esc_html__( 'Custom Font TTF', 'konsept-core' ),
				'args'       => array(
					'allowed_type' => 'application/octet-stream'
				)
			) );

			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_otf',
				'title'      => esc_html__( 'Custom Font OTF', 'konsept-core' ),
				'args'       => array(
					'allowed_type' => 'application/octet-stream'
				)
			) );

			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_woff',
				'title'      => esc_html__( 'Custom Font WOFF', 'konsept-core' ),
				'args'       => array(
					'allowed_type' => 'application/octet-stream'
				)
			) );

			$page_repeater->add_field_element( array(
				'field_type' => 'file',
				'name'       => 'qodef_custom_font_woff2',
				'title'      => esc_html__( 'Custom Font WOFF2', 'konsept-core' ),
				'args'       => array(
					'allowed_type' => 'application/octet-stream'
				)
			) );

			$page_repeater->add_field_element( array(
				'field_type' => 'text',
				'name'       => 'qodef_custom_font_name',
				'title'      => esc_html__( 'Custom Font Name', 'konsept-core' ),
			) );

			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_page_fonts_options_map', $page );
		}
	}

	add_action( 'konsept_core_action_default_options_init', 'konsept_core_add_fonts_options', konsept_core_get_admin_options_map_position( 'fonts' ) );
}