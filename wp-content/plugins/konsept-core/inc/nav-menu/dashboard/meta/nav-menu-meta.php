<?php

if ( ! function_exists( 'konsept_core_nav_menu_meta_options' ) ) {
	function konsept_core_nav_menu_meta_options( $page ) {
		
		if ( $page ) {
			
			$section = $page->add_section_element(
				array(
					'name'  => 'qodef_nav_menu_section',
					'title' => esc_html__( 'Main Menu', 'konsept-core' )
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_dropdown_top_position',
					'title'       => esc_html__( 'Dropdown Position', 'konsept-core' ),
					'description' => esc_html__( 'Enter value in percentage of entire header height', 'konsept-core' ),
				)
			);
		}
	}
	
	add_action( 'konsept_core_action_after_page_header_meta_map', 'konsept_core_nav_menu_meta_options' );
}