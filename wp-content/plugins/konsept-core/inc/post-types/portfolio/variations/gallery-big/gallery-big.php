<?php

if ( ! function_exists( 'konsept_core_add_portfolio_single_variation_gallery_big' ) ) {
	function konsept_core_add_portfolio_single_variation_gallery_big( $variations ) {
		$variations['gallery-big'] = esc_html__( 'Gallery - Big', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_single_layout_options', 'konsept_core_add_portfolio_single_variation_gallery_big' );
}