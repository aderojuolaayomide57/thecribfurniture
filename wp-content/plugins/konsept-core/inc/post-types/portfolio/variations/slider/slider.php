<?php

if ( ! function_exists( 'konsept_core_add_portfolio_single_variation_slider' ) ) {
	function konsept_core_add_portfolio_single_variation_slider( $variations ) {
		$variations['slider'] = esc_html__( 'Slider', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_single_layout_options', 'konsept_core_add_portfolio_single_variation_slider' );
}

if ( ! function_exists( 'konsept_core_add_portfolio_single_slider' ) ) {
	function konsept_core_add_portfolio_single_slider() {
		if ( konsept_core_get_post_value_through_levels( 'qodef_portfolio_single_layout' ) == 'slider' ) {
			konsept_core_template_part( 'post-types/portfolio', 'variations/slider/layout/parts/slider' );
		}
	}
	
	add_action( 'konsept_action_before_page_inner', 'konsept_core_add_portfolio_single_slider' );
}