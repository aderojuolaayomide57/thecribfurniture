<?php

if ( ! function_exists( 'konsept_core_add_portfolio_single_variation_images_big' ) ) {
	function konsept_core_add_portfolio_single_variation_images_big( $variations ) {
		$variations['images-big'] = esc_html__( 'Images - Big', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_single_layout_options', 'konsept_core_add_portfolio_single_variation_images_big' );
}

if ( ! function_exists( 'konsept_core_set_default_portfolio_single_variation_compact' ) ) {
	function konsept_core_set_default_portfolio_single_variation_compact() {
		return 'images-big';
	}
	
	add_filter( 'konsept_core_filter_portfolio_single_layout_default_value', 'konsept_core_set_default_portfolio_single_variation_compact' );
}