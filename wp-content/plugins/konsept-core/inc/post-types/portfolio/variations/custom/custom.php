<?php

if ( ! function_exists( 'konsept_core_add_portfolio_single_variation_custom' ) ) {
	function konsept_core_add_portfolio_single_variation_custom( $variations ) {
		$variations['custom'] = esc_html__( 'Custom', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_single_layout_options', 'konsept_core_add_portfolio_single_variation_custom' );
}