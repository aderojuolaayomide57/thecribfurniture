<?php

if ( ! function_exists( 'konsept_core_add_portfolio_single_variation_gallery_small' ) ) {
	function konsept_core_add_portfolio_single_variation_gallery_small( $variations ) {
		$variations['gallery-small'] = esc_html__( 'Gallery - Small', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_single_layout_options', 'konsept_core_add_portfolio_single_variation_gallery_small' );
}