<?php

if ( ! function_exists( 'konsept_core_include_portfolio_single_post_navigation_template' ) ) {
	/**
	 * Function which includes additional module on single portfolio page
	 */
	function konsept_core_include_portfolio_single_post_navigation_template() {
		konsept_core_template_part( 'post-types/portfolio', 'templates/single/single-navigation/templates/single-navigation' );
	}
	
	add_action( 'konsept_core_action_after_portfolio_single_item', 'konsept_core_include_portfolio_single_post_navigation_template' );
}