<?php

if ( ! function_exists( 'konsept_core_add_portfolio_single_navigation_options' ) ) {
	function konsept_core_add_portfolio_single_navigation_options( $page ) {

		if ( $page ) {

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_portfolio_enable_navigation',
					'title'         => esc_html__( 'Navigation', 'konsept-core' ),
					'description'   => esc_html__( 'Enabling this option will turn on portfolio navigation functionality', 'konsept-core' ),
					'default_value' => 'yes'
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_portfolio_navigation_through_same_category',
					'title'         => esc_html__( 'Navigation Through Same Category', 'konsept-core' ),
					'description'   => esc_html__( 'Enabling this option will make portfolio navigation sort through current category', 'konsept-core' ),
					'default_value' => 'no',
					'dependency'    => array(
						'show' => array(
							'qodef_portfolio_enable_navigation' => array(
								'values'        => 'yes',
								'default_value' => 'yes'
							)
						)
					)
				)
			);

            $page->add_field_element(
                array(
                    'field_type'  => 'select',
                    'name'        => 'qodef_portfolio_single_back_to_link',
                    'title'       => esc_html__( 'Back To Link', 'konsept-core' ),
                    'description' => esc_html__( 'Choose default "Back To" page to link from portfolio single', 'konsept-core' ),
                    'options'     => qode_framework_get_pages( true ),
                )
            );
		}
	}

	add_action( 'konsept_core_action_after_portfolio_options_single', 'konsept_core_add_portfolio_single_navigation_options' );
}