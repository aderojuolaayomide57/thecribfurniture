<div class="qodef-e qodef-info--social-share">
	<?php if ( class_exists( 'KonseptCoreSocialShareShortcode' ) ) {
		$params = array(
			'title'  => esc_html__( 'Share:', 'konsept-core' ),
			'layout' => 'list'
		);
		
		echo KonseptCoreSocialShareShortcode::call_shortcode( $params );
	} ?>
</div>