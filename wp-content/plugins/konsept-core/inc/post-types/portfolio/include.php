<?php

include_once KONSEPT_CORE_CPT_PATH . '/portfolio/helper.php';

foreach ( glob( KONSEPT_CORE_CPT_PATH . '/portfolio/dashboard/admin/*.php' ) as $module ) {
	include_once $module;
}

foreach ( glob( KONSEPT_CORE_CPT_PATH . '/portfolio/dashboard/meta-box/*.php' ) as $module ) {
	include_once $module;
}

foreach ( glob( KONSEPT_CORE_CPT_PATH . '/portfolio/variations/*/include.php' ) as $variation ) {
	include_once $variation;
}

foreach ( glob( KONSEPT_CORE_CPT_PATH . '/portfolio/templates/single/*/include.php' ) as $single_part ) {
	include_once $single_part;
}

if ( ! function_exists( 'konsept_core_include_portfolio_tax_fields' ) ) {
	function konsept_core_include_portfolio_tax_fields() {
		include_once KONSEPT_CORE_CPT_PATH . '/portfolio/dashboard/taxonomy/taxonomy-options.php';
	}
	
	add_action( 'konsept_core_action_include_cpt_tax_fields', 'konsept_core_include_portfolio_tax_fields' );
}