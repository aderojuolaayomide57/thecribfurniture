<?php

if ( ! function_exists( 'konsept_core_register_portfolio_for_meta_options' ) ) {
	function konsept_core_register_portfolio_for_meta_options( $post_types ) {
		$post_types[] = 'portfolio-item';
		
		return $post_types;
	}
	
	add_filter( 'qode_framework_filter_meta_box_save', 'konsept_core_register_portfolio_for_meta_options' );
	add_filter( 'qode_framework_filter_meta_box_remove', 'konsept_core_register_portfolio_for_meta_options' );
}

if ( ! function_exists( 'konsept_core_add_portfolio_custom_post_type' ) ) {
	/**
	 * Function that adds portfolio custom post type
	 *
	 * @param array $cpts
	 *
	 * @return array
	 */
	function konsept_core_add_portfolio_custom_post_type( $cpts ) {
		$cpts[] = 'KonseptCorePortfolioCPT';
		
		return $cpts;
	}
	
	add_filter( 'konsept_core_filter_register_custom_post_types', 'konsept_core_add_portfolio_custom_post_type' );
}

if ( class_exists( 'QodeFrameworkCustomPostType' ) ) {
	class KonseptCorePortfolioCPT extends QodeFrameworkCustomPostType {
		
		public function map_post_type() {
			$name = esc_html__( 'Portfolio', 'konsept-core' );
			$this->set_base( 'portfolio-item' );
			$this->set_menu_position( 10 );
			$this->set_menu_icon( 'dashicons-grid-view' );
			$this->set_slug( 'portfolio-item' );
			$this->set_name( $name );
			$this->set_path( KONSEPT_CORE_CPT_PATH . '/portfolio' );
			$this->set_labels( array(
				'name'          => esc_html__( 'Konsept Portfolio', 'konsept-core' ),
				'singular_name' => esc_html__( 'Portfolio Item', 'konsept-core' ),
				'add_item'      => esc_html__( 'New Portfolio Item', 'konsept-core' ),
				'add_new_item'  => esc_html__( 'Add New Portfolio Item', 'konsept-core' ),
				'edit_item'     => esc_html__( 'Edit Portfolio Item', 'konsept-core' )
			) );
			$this->add_post_taxonomy( array(
				'base'          => 'portfolio-category',
				'slug'          => 'portfolio-category',
				'singular_name' => esc_html__( 'Category', 'konsept-core' ),
				'plural_name'   => esc_html__( 'Categories', 'konsept-core' ),
			) );
			$this->add_post_taxonomy( array(
				'base'          => 'portfolio-tag',
				'slug'          => 'portfolio-tag',
				'singular_name' => esc_html__( 'Tag', 'konsept-core' ),
				'plural_name'   => esc_html__( 'Tags', 'konsept-core' ),
			) );
		}
	}
}