<?php

if ( ! function_exists( 'konsept_core_filter_portfolio_list_info_below_tilt' ) ) {
	function konsept_core_filter_portfolio_list_info_below_tilt( $variations ) {
		$variations['tilt'] = esc_html__( 'Tilt', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_list_info_below_animation_options', 'konsept_core_filter_portfolio_list_info_below_tilt' );
}