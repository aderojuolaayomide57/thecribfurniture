<?php

if ( ! function_exists( 'konsept_core_add_portfolio_list_variation_info_top_left' ) ) {
	function konsept_core_add_portfolio_list_variation_info_top_left( $variations ) {
		
		$variations['info-top-left'] = esc_html__( 'Info Top Left', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_list_layouts', 'konsept_core_add_portfolio_list_variation_info_top_left' );
}