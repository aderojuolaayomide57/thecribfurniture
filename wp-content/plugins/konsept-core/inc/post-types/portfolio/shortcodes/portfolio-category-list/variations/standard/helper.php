<?php

if ( ! function_exists( 'konsept_core_add_portfolio_category_list_variation_standard' ) ) {
	function konsept_core_add_portfolio_category_list_variation_standard( $variations ) {
		$variations['standard'] = esc_html__( 'Standard', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_category_list_layouts', 'konsept_core_add_portfolio_category_list_variation_standard' );
}
