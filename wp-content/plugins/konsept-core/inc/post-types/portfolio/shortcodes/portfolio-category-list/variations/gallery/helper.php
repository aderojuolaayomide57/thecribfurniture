<?php

if ( ! function_exists( 'konsept_core_add_portfolio_category_list_variation_gallery' ) ) {
	function konsept_core_add_portfolio_category_list_variation_gallery( $variations ) {
		$variations['gallery'] = esc_html__( 'Gallery', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_portfolio_category_list_layouts', 'konsept_core_add_portfolio_category_list_variation_gallery' );
}
