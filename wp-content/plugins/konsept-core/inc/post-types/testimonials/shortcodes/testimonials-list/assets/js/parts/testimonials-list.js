(function ($) {
    "use strict";

    qodefCore.shortcodes.konsept_core_testimonials_list = {};

    $(window).on('load', function(){
        qodefTestimonials.init();
    });

    var qodefTestimonials = {
        init: function () {
            var $holder = $('.qodef-testimonials-list .qodef-swiper-container.qodef-swiper--initialized');

            if ($holder.length) {
                $holder.each(function () {
                    var $thisHolder = $(this);

                    qodefTestimonials.calcMinHeight($thisHolder);

                    $(window).on('resize', function () {
                        qodefTestimonials.calcMinHeight($thisHolder);
                    });
                })
            }
        },
        calcMinHeight: function($holder) {
            var thisSwiper = $holder[0].swiper,
                maxHeight = 100,
                swiperSlide = $holder.find('.qodef-e-inner');

            // Clear set values
            $holder.css({
                'max-height': '',
            });

            setTimeout(function () {
                swiperSlide.each(function() {
                    if ($(this).height() > maxHeight) {
                        maxHeight = $(this).outerHeight(true);
                    }
                });

                maxHeight = parseInt(maxHeight) + 'px';

                $holder.css('max-height', maxHeight);

                thisSwiper.update();
            }, 10);
        }
    }

    qodefCore.shortcodes.konsept_core_testimonials_list.qodefTestimonials = qodefTestimonials;

})(jQuery);