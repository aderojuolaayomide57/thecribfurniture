<div class="qodef-testimonials-list">
    <div class="qodef-e-testimonials-icon"><?php konsept_svg_icon('quote') ?></div>
    <div>
        <div class="qodef-e-testimonials-title"><?php echo esc_html__('Testimonials','konsept-core'); ?></div>

        <div <?php qode_framework_class_attribute( $holder_classes ); ?> <?php qode_framework_inline_attr( $slider_attr, 'data-options' ); ?>>
            <div class="swiper-wrapper">
                <?php
                // Include items
                konsept_core_template_part( 'post-types/testimonials/shortcodes/testimonials-list', 'templates/loop', '', $params );
                ?>
            </div>
        </div>
		<?php if ( $slider_pagination !== 'no' ) { ?>
            <div class="swiper-pagination"></div>
		<?php } ?>
    </div>
</div>