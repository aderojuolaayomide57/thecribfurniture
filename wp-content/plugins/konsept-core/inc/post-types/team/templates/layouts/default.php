<article <?php post_class( 'qodef-team-list-item qodef-e' ); ?>>
	<div class="qodef-e-inner">
		<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/image' ); ?>
		<div class="qodef-e-content">
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/title' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/role' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/social-icons' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/address' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/birth-date' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/education' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/email' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/resume' ); ?>
			<?php konsept_core_template_part( 'post-types/team', 'templates/parts/post-info/excerpt' ); ?>
		</div>
	</div>
</article>