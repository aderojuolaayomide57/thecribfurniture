<?php

if ( ! function_exists( 'konsept_core_add_team_list_variation_info_on_hover' ) ) {
	function konsept_core_add_team_list_variation_info_on_hover( $variations ) {
		
		$variations['info-on-hover'] = esc_html__( 'Info on Hover', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_team_list_layouts', 'konsept_core_add_team_list_variation_info_on_hover' );
}