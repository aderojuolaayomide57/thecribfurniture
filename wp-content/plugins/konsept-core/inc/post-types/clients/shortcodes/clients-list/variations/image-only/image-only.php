<?php

if ( ! function_exists( 'konsept_core_add_clients_list_variation_image_only' ) ) {
	function konsept_core_add_clients_list_variation_image_only( $variations ) {
		
		$variations['image-only'] = esc_html__( 'Image Only', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_clients_list_layouts', 'konsept_core_add_clients_list_variation_image_only' );
}