<?php
if ( ! function_exists( 'konsept_core_filter_clients_list_image_only_fade_in' ) ) {
	function konsept_core_filter_clients_list_image_only_fade_in( $variations ) {
		
		$variations['fade-in'] = esc_html__( 'Fade In', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_clients_list_image_only_animation_options', 'konsept_core_filter_clients_list_image_only_fade_in' );
}