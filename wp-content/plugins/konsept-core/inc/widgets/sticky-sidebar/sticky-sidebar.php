<?php

if ( ! function_exists( 'konsept_core_add_sticky_sidebar_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_sticky_sidebar_widget( $widgets ) {
		$widgets[] = 'KonseptCoreStickySidebarWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_sticky_sidebar_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreStickySidebarWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_base( 'konsept_core_sticky_sidebar' );
			$this->set_name( esc_html__( 'Konsept Sticky Sidebar', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Use this widget to make the sidebar sticky. Drag it into the sidebar above the widget which you want to be the first element in the sticky sidebar', 'konsept-core' ) );
		}
		
		public function render( $atts ) {
		}
	}
}
