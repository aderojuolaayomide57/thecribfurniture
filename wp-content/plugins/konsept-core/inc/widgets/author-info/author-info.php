<?php

if ( ! function_exists( 'konsept_core_add_author_info_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_author_info_widget( $widgets ) {
		$widgets[] = 'KonseptCoreAuthorInfoWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_author_info_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreAuthorInfoWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_base( 'konsept_core_author_info' );
			$this->set_name( esc_html__( 'Konsept Author Info', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Add author info element into widget areas', 'konsept-core' ) );
			$this->set_widget_option(
				array(
					'field_type' => 'text',
					'name'       => 'author_username',
					'title'      => esc_html__( 'Author Username', 'konsept-core' )
				)
			);
            $this->set_widget_option(
                array(
                    'field_type' => 'select',
                    'name'       => 'show_avatar',
                    'options'    => konsept_core_get_select_type_options_pool( 'no_yes' ),
                    'title'      => esc_html__( 'Display Author Image', 'konsept-core' )
                )
            );
			$this->set_widget_option(
                array(
                    'field_type' => 'text',
                    'name'       => 'author_description_length',
                    'title'      => esc_html__( 'Author Description Length (in words)', 'konsept-core' )
                )
            );
			$this->set_widget_option(
				array(
					'field_type' => 'color',
					'name'       => 'author_color',
					'title'      => esc_html__( 'Author Color', 'konsept-core' )
				)
			);
			$this->set_widget_option(
				array(
					'field_type' => 'iconpack',
					'name'       => 'icon_pack_author',
					'title'      => esc_html__( 'Author Icon', 'konsept-core' )
				)
			);
		}
		
		public function render( $atts ) {
			$author_id = 1;
			if ( ! empty( $atts['author_username'] ) ) {
				$author = get_user_by( 'login', $atts['author_username'] );
				
				if ( ! empty( $author ) ) {
					$author_id = $author->ID;
				}
			}
			
			$author_link = get_author_posts_url( $author_id );
			$author_bio_length = $atts['author_description_length'];
			$author_bio_length = !empty($author_bio_length) ? $author_bio_length : -1;
			$author_bio = wp_trim_words(strip_tags(get_the_author_meta('description', $author_id)), $author_bio_length, '');
			?>
			<div class="widget qodef-author-info">
                <?php if ( $atts['show_avatar'] !== 'no' ) { ?>
                    <a itemprop="url" class="qodef-author-info-image" href="<?php echo esc_url( $author_link ); ?>">
                        <?php echo get_avatar( $author_id, 272 ); ?>
                    </a>
                <?php } ?>
				<?php if ( ! empty( $author_bio ) ) { ?>
					<h4 class="qodef-author-info-name vcard author">
						<a itemprop="url" href="<?php echo esc_url( $author_link ); ?>">
							<span class="fn"><?php echo esc_html( get_the_author_meta( 'display_name', $author_id ) ); ?></span>
						</a>
					</h4>
					<p itemprop="description" class="qodef-author-info-description"><?php echo esc_html( $author_bio ); ?></p>
				<?php } ?>
			</div>
			<?php
		}
	}
}
