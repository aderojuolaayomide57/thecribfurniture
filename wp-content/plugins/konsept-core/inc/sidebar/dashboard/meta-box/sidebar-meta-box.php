<?php

if ( ! function_exists( 'konsept_core_add_page_sidebar_meta_box' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function konsept_core_add_page_sidebar_meta_box( $page ) {
		
		if ( $page ) {
			
			$sidebar_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-sidebar',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Sidebar Settings', 'konsept-core' ),
					'description' => esc_html__( 'Sidebar layout settings', 'konsept-core' )
				)
			);
			
			$sidebar_tab->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_sidebar_layout',
					'title'         => esc_html__( 'Sidebar Layout', 'konsept-core' ),
					'description'   => esc_html__( 'Choose a sidebar layout', 'konsept-core' ),
					'options'       => konsept_core_get_select_type_options_pool( 'sidebar_layouts' )
				)
			);
			
			$custom_sidebars = konsept_core_get_custom_sidebars();
			if ( ! empty( $custom_sidebars ) && count( $custom_sidebars ) > 1 ) {
				$sidebar_tab->add_field_element(
					array(
						'field_type'  => 'select',
						'name'        => 'qodef_page_custom_sidebar',
						'title'       => esc_html__( 'Custom Sidebar', 'konsept-core' ),
						'description' => esc_html__( 'Choose a custom sidebar', 'konsept-core' ),
						'options'     => $custom_sidebars
					)
				);
			}
			
			$sidebar_tab->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_sidebar_grid_gutter',
					'title'       => esc_html__( 'Set Grid Gutter', 'konsept-core' ),
					'description' => esc_html__( 'Choose grid gutter size to set space between content and sidebar', 'konsept-core' ),
					'options'     => konsept_core_get_select_type_options_pool( 'items_space' )
				)
			);
			
			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_page_sidebar_meta_box_map', $sidebar_tab );
		}
	}
	
	add_action( 'konsept_core_action_after_general_meta_box_map', 'konsept_core_add_page_sidebar_meta_box' );
}

if ( ! function_exists( 'konsept_core_add_general_page_sidebar_meta_box_callback' ) ) {
	/**
	 * Function that set current meta box callback as general callback functions
	 *
	 * @param array $callbacks
	 *
	 * @return array
	 */
	function konsept_core_add_general_page_sidebar_meta_box_callback( $callbacks ) {
		$callbacks['page-sidebar'] = 'konsept_core_add_page_sidebar_meta_box';
		
		return $callbacks;
	}
	
	add_filter( 'konsept_core_filter_general_meta_box_callbacks', 'konsept_core_add_general_page_sidebar_meta_box_callback' );
}