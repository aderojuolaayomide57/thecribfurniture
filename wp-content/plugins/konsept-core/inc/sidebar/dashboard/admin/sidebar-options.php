<?php

if ( ! function_exists( 'konsept_core_add_page_sidebar_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function konsept_core_add_page_sidebar_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => KONSEPT_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'sidebar',
				'icon'        => 'fa fa-book',
				'title'       => esc_html__( 'Sidebar', 'konsept-core' ),
				'description' => esc_html__( 'Global Sidebar Options', 'konsept-core' )
			)
		);

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_page_sidebar_layout',
					'title'         => esc_html__( 'Sidebar Layout', 'konsept-core' ),
					'description'   => esc_html__( 'Choose a default sidebar layout for pages', 'konsept-core' ),
					'options'       => konsept_core_get_select_type_options_pool( 'sidebar_layouts', false ),
					'default_value' => 'no-sidebar'
				)
			);

			$custom_sidebars = konsept_core_get_custom_sidebars();
			if ( ! empty( $custom_sidebars ) && count( $custom_sidebars ) > 1 ) {
				$page->add_field_element(
					array(
						'field_type'  => 'select',
						'name'        => 'qodef_page_custom_sidebar',
						'title'       => esc_html__( 'Custom Sidebar', 'konsept-core' ),
						'description' => esc_html__( 'Choose a custom sidebar to display on pages', 'konsept-core' ),
						'options'     => $custom_sidebars
					)
				);
			}

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_sidebar_grid_gutter',
					'title'       => esc_html__( 'Set Grid Gutter', 'konsept-core' ),
					'description' => esc_html__( 'Choose grid gutter size to set space between content and sidebar', 'konsept-core' ),
					'options'     => konsept_core_get_select_type_options_pool( 'items_space' )
				)
			);

			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_page_sidebar_options_map', $page );
		}
	}

	add_action( 'konsept_core_action_default_options_init', 'konsept_core_add_page_sidebar_options', konsept_core_get_admin_options_map_position( 'sidebar' ) );
}