<?php

if ( ! class_exists( 'KonseptCoreWooCommerceYITHQuickView' ) ) {
	class KonseptCoreWooCommerceYITHQuickView {
		private static $instance;
		
		public function __construct() {
			
			if ( qode_framework_is_installed( 'yith-quick-view' ) ) {
				// Init
				add_action( 'after_setup_theme', array( $this, 'init' ) );
			}
		}
		
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			
			return self::$instance;
		}
		
		function init() {
			
			// Unset default templates modules
			$this->replace_templates_modules();
		}

        function replace_templates_modules() {

            $wcqv = new YITH_WCQV;

            if($wcqv ->load_frontend()) { //Prevent from showing after product on list
                remove_action( 'woocommerce_after_shop_loop_item', array( YITH_WCQV_Frontend(), 'yith_add_quick_view_button' ), 15 );
            }

            remove_action( 'yith_wcqv_product_image', 'woocommerce_show_product_sale_flash', 10 );
            add_action( 'yith_wcqv_product_image', 'woocommerce_show_product_sale_flash', 20 );

            remove_action('yith_wcqv_product_summary',  'woocommerce_template_single_meta', 30);
            add_action( 'yith_wcqv_product_summary', 'woocommerce_template_wcqw_product_link', 31 );

            remove_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_rating', 10 );
            add_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_rating', 15 );

            remove_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_price', 15 );
            add_action( 'yith_wcqv_product_summary', 'woocommerce_template_single_price', 10 );
        }
	}
	
	KonseptCoreWooCommerceYITHQuickView::get_instance();
}