(function($) {
    "use strict";

    $(window).on('load', function () {
        qodefWooWCCLChangeColor.init();
    });

    /*
     **	Re-init scripts on gallery loaded
     */
	$(document).on('yith_wccl_product_gallery_loaded', function () {
		
		if (typeof qodefCore.qodefWooMagnificPopup === "function") {
			qodefCore.qodefWooMagnificPopup.init();
		}

        qodefWooWCCLChangeColor.init()
	});

    var qodefWooWCCLChangeColor = {
        init: function() {
            var colorCircle = $('.select_option_colorpicker');

            colorCircle.on('click', function () {
                var thisColor = $(this).find('.yith_wccl_value').css('background');

                if($(this).hasClass('selected')) {
                    $(this).css({'border-color' : thisColor});
                } else {
                    $(this).css({'border-color' : 'transparent'});
                }
            })
        }
	}

})(jQuery);