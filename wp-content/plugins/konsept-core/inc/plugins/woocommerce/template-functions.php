<?php

if ( ! function_exists( 'konsept_core_woo_change_product_tabs_position' ) ) {
    /**
     * Puts product tabs below product summary
     *
     * @return void
     */
    function konsept_core_woo_change_product_tabs_position(){

        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
        add_action('woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 55);
    }
}
if ( ! function_exists( 'konsept_core_woo_open_summary' ) ) {
    /**
     * Adds a wrapper div fo sticky product summary
     */
    function konsept_core_woo_open_summary(){
        echo '<div class="qodef-sticky-product-summary">';
    }
}

if ( ! function_exists( 'konsept_core_woo_close_summary' ) ) {
    /**
     * Adds a wrapper div fo sticky product summary
     */
    function konsept_core_woo_close_summary(){
        echo '</div>';
    }
}

if ( ! function_exists( 'konsept_core_woo_wrap_sticky_summary' ) ) {
    /**
     * Wrap sticky product summary
     */
    function konsept_core_woo_wrap_sticky_summary(){
        add_action( 'woocommerce_single_product_summary', 'konsept_core_woo_open_summary', 1  );
        add_action( 'woocommerce_after_single_product_summary', 'konsept_core_woo_close_summary', 1  );
    }
}

if ( ! function_exists( 'konsept_core_woo_get_product_single_layout' ) ) {
    /**
     * Checks out the layout of product single page
     *
     * @return string
     */
    function konsept_core_woo_get_product_single_layout() {

        if (is_product() && get_post_type() === 'product'){ // is_product() is set so classes aren't added when product is in list
            $product_page_layout = get_post_meta( get_the_ID(), 'qodef_product_layout', true );

            return $product_page_layout;
        }
    }
}

if ( ! function_exists( 'konsept_core_product_single_layout_class' ) ) {

    function konsept_core_product_single_layout_class($classes){

        $layout = konsept_core_woo_get_product_single_layout();

        $classes[] = 'qodef-product-single-' . $layout . '-layout';

        return $classes;

    }

    add_filter('body_class', 'konsept_core_product_single_layout_class');
}

if ( ! function_exists( 'konsept_core_woo_get_product_render_gallery_layout' ) ) {
    /**
     * Creates image gallery for "Small Gallery" product layout
     *
     * @return gallery HTML
     */
    function konsept_core_woo_get_product_render_gallery_layout() {

        if(get_post_type() === 'product' || konsept_core_woo_get_product_single_layout() == 'small-gallery'){
            $product_gallery = array();
            $product_gallery = get_post_meta( get_the_ID(), 'qodef_product_images_gallery', true );
            $product_gallery = explode(',', $product_gallery);
            $html = '';


            $html .= '<div class="qodef-grid qodef-layout--columns  qodef-gutter--normal qodef-col-num--2">';
            $html .= '<div class="qodef-grid-inner clear">';

            foreach($product_gallery as $image){
                $html .= '<div class="qodef-product-gallery-image qodef-grid-item">';
                $html .= wp_get_attachment_image($image, 'full');
                $html .= '</div>';
            }

            $html .= '</div></div>';

            echo $html;
        }
    }
}

if ( ! function_exists( 'konsept_core_woo_get_product_render_big_images_layout' ) ) {
    /**
     * Creates image gallery for "Small Gallery" product layout
     *
     * @return void
     */
    function konsept_core_woo_get_product_render_big_images_layout() {

        if(get_post_type() === 'product' || konsept_core_woo_get_product_single_layout() == 'gallery'){
            $product_gallery = get_post_meta( get_the_ID(), 'qodef_product_images_gallery', true );
            $product_gallery = explode(',', $product_gallery);
            $html = '';


            $html .= '<div class="qodef-grid qodef-layout--columns  qodef-gutter--normal qodef-col-num--1">';
            $html .= '<div class="qodef-grid-inner clear">';

            foreach($product_gallery as $image){
                $html .= '<div class="qodef-product-gallery-image qodef-grid-item">';
                $html .= wp_get_attachment_image($image, 'full');
                $html .= '</div>';
            }

            $html .= '</div></div>';

            echo $html;

            konsept_core_woo_change_product_tabs_position();
        }
    }
}

if ( ! function_exists( 'konsept_core_woo_get_product_render_sticky_info_layout' ) ) {
    /**
     * Creates image gallery for "Slider" product layout
     *
     * @return void
     */
    function konsept_core_woo_get_product_render_sticky_info_layout() {

        if(get_post_type() === 'product' || konsept_core_woo_get_product_single_layout() == 'sticky-info'){
            $product_gallery = get_post_meta( get_the_ID(), 'qodef_product_images_gallery', true );
            $product_gallery = explode(',', $product_gallery);
            $html = '';


            foreach($product_gallery as $image){
                $html .= '<div class="qodef-product-gallery-image qodef-grid-item">';
                $html .= wp_get_attachment_image($image, 'full');
                $html .= '</div>';
            }

            echo $html;

            konsept_core_woo_change_product_tabs_position();
            konsept_core_woo_wrap_sticky_summary();
        }
    }
}

if ( ! function_exists( 'konsept_core_woo_get_product_image_gallery' ) ) {
    /**
     * Creates image gallery for product single page
     *
     * @return gallery HTML
     */
    function konsept_core_woo_get_product_image_gallery() {

        if(get_post_type() === 'product'){
            $product_layout =  konsept_core_woo_get_product_single_layout();



            switch($product_layout){
                case 'gallery':
                    konsept_core_woo_get_product_render_gallery_layout();
                    break;
                case 'big-images':
                    konsept_core_woo_get_product_render_big_images_layout();
                    break;
                case 'sticky-info':
                    konsept_core_woo_get_product_render_sticky_info_layout();
                    break;
                default: add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 29  );
            }
        }
    }
}
if ( ! function_exists( 'konsept_core_woo_has_some_additional_mark' ) ) {
	/**
	 * Function that check is some mark enabled on product
	 *
	 * @return bool
	 */
	function konsept_core_woo_has_some_additional_mark() {
		return qode_framework_is_installed( 'theme' ) ? konsept_woo_has_some_additional_mark() : '';
	}
}