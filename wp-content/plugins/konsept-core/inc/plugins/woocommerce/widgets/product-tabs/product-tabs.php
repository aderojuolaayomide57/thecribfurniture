<?php

if ( ! function_exists( 'konsept_core_add_product_category_tabs_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param $widgets array
	 *
	 * @return array
	 */
	function konsept_core_add_product_category_tabs_widget( $widgets ) {
		$widgets[] = 'KonseptCorePostCategoryTabsWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_product_category_tabs_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCorePostCategoryTabsWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
		    $this->set_widget_option(
                array(
                    'field_type'    => 'text',
                    'name'          => 'widget_title',
                    'title'         => esc_html__( 'Title', 'konsept-core' )
                )
            );
			$this->set_base( 'konsept_core_product_category_tabs' );
			$this->set_name( esc_html__( 'Konsept Product Category Tabs', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Display products of selected categories in tabs', 'konsept-core' ) );

			$categories_widget_array   = array();
			$categories_widget_array[] = esc_html__( 'None', 'konsept-core' );
			$term_args = array (
				'taxonomy'               => array( 'product_cat' )
			);

			$term_query = new \WP_Term_Query( $term_args );

			if ( ! empty( $term_query ) && ! is_wp_error( $term_query ) ) {
                if( !empty($term_query->terms) ) {
					foreach ($term_query->terms as $term) {
						$categories_widget_array[$term->slug] = $term->name;
					}
				}
			}

			$numeration = array(
				esc_html__( 'First', 'konsept-core' ),
				esc_html__( 'Second', 'konsept-core' ),
				esc_html__( 'Third', 'konsept-core' ),
				esc_html__( 'Fourth', 'konsept-core' ),
				esc_html__( 'Fifth', 'konsept-core' ),
				esc_html__( 'Sixth', 'konsept-core' ),
				esc_html__( 'Seventh', 'konsept-core' ),
				esc_html__( 'Eighth', 'konsept-core' ),
			);
			
			$this->set_widget_option(
				array(
					'field_type'    => 'select',
					'name'          => 'columns',
					'title'         => esc_html__( 'Number of Columns', 'konsept-core' ),
					'options'       => konsept_core_get_select_type_options_pool( 'columns_number' ),
					'default_value' => '4',
				)
			);
			
			for ( $i = 1; $i <= 8; $i ++ ) {
				$this->set_widget_option(
					array(
						'field_type' => 'select',
						'name'       => 'category_' . $i . '_slug',
						'title'      => $numeration[ $i - 1 ] . esc_html__( ' Category', 'konsept-core' ),
						'options'    => $categories_widget_array
					)
				);
			}
			
			$this->set_widget_option( array(
				'field_type'    => 'select',
				'name'          => 'space',
				'title'         => esc_html__( 'Space Between Items', 'konsept-core' ),
				'options'       => konsept_core_get_select_type_options_pool( 'items_space' ),
				'default_value' => 'normal'
			) );
			
			$this->set_widget_option( array(
				'field_type'    => 'select',
				'name'          => 'images_proportion',
				'default_value' => 'full',
				'title'         => esc_html__( 'Image Proportions', 'konsept-core' ),
				'options'       => konsept_core_get_select_type_options_pool( 'list_image_dimension', false ),
			) );
			
			$this->set_widget_option( array(
				'field_type'  => 'text',
				'name'        => 'custom_image_width',
				'title'       => esc_html__( 'Custom Image Width', 'konsept-core' ),
				'description' => esc_html__( 'Enter image width in px', 'konsept-core' ),
				'dependency'  => array(
					'show' => array(
						'images_proportion' => array(
							'values'        => 'custom',
							'default_value' => 'full'
						)
					)
				)
			) );
			
			$this->set_widget_option( array(
				'field_type'  => 'text',
				'name'        => 'custom_image_height',
				'title'       => esc_html__( 'Custom Image Height', 'konsept-core' ),
				'description' => esc_html__( 'Enter image height in px', 'konsept-core' ),
				'dependency'  => array(
					'show' => array(
						'images_proportion' => array(
							'values'        => 'custom',
							'default_value' => 'full'
						)
					)
				)
			) );
			
			$this->set_widget_option( array(
				'field_type'    => 'select',
				'name'          => 'title_tag',
				'title'         => esc_html__( 'Title Tag', 'konsept-core' ),
				'options'       => konsept_core_get_select_type_options_pool( 'title_tag' ),
				'default_value' => 'h5',
				'group'         => esc_html__( 'Layout', 'konsept-core' )
			) );
			
			$this->set_widget_option( array(
				'field_type' => 'text',
				'name'       => 'title_length',
				'title'      => esc_html__( 'Title Length', 'konsept-core' ),
				'group'      => esc_html__( 'Layout', 'konsept-core' )
			) );

            $this->set_widget_option( array(
                'field_type' => 'select',
                'name'       => 'additional_buttons',
                'title'      => esc_html__( 'Enable Quick View and Wishlist', 'konsept-core' ),
                'options'     => konsept_core_get_select_type_options_pool( 'no_yes' ),
            ) );
		}
		
		public function render( $atts ) {
			
			$atts['additional_params'] = 'tax';
			$atts['tax']               = 'product_cat';
			$atts['posts_per_page']    = $atts['columns'];

			$widget_title = !empty($atts[ 'widget_title']) ? esc_attr( $atts[ 'widget_title']) : esc_html__( 'Shop by', 'konsept-core' );
			
			$categories_html = '<h4 class="qodef-e-tabs-title">' .$widget_title. '</h4>';
			$products_html      = '';
			//create category and product html
			for ( $i = 1; $i <= 8; $i ++ ) {
				if ( ! empty( $atts[ 'category_' . $i . '_slug' ] ) ) {
				    $cat_slug = $atts[ 'category_' . $i . '_slug' ];

                    $cat_name = get_term_by('slug', $cat_slug, 'product_cat') -> name;

					$categories_html .= '<div class="qodef-e-tab"><a href="#"><span class="item_text">' . esc_attr( $cat_name ) . '</span></a></div>';
					
					$atts['tax_slug'] = $atts[ 'category_' . $i . '_slug' ];
					$atts['layout']   = 'info-below';
					$products_html       .= '<div class="qodef-e-tab-content">';
                    $products_html       .= qode_framework_call_shortcode( 'konsept_core_product_list', $atts );
					$products_html       .= '</div>';
				}
			}
			?>
			<div class="qodef-e-tabs-inner">
				<div class="qodef-e-tabs-holder">
					<?php echo wp_kses_post( $categories_html ); ?>
				</div>
				<div class="qodef-e-tabs-content">
					<?php echo qode_framework_wp_kses_html('html', $products_html ); ?>
				</div>
			</div>
			<?php
		}
	}
}