(function($) {
	"use strict";

    $(document).ready( function () {
		qodefProductCategoryTabs.init();
	} );

    /**
     *  Init Product Tabs
     *
     */
	var qodefProductCategoryTabs  = {
    	init: function() {
			var $productTabs = $('.widget.widget_konsept_core_product_category_tabs');

			if( $productTabs.length ) {
				$productTabs.each( function() {
					var $categories = $(this).find('.qodef-e-tabs-holder .qodef-e-tab'),
						$categoriesLinks = $categories.find('a'),
						$contentTabs = $(this).find('.qodef-e-tabs-content .qodef-e-tab-content');

					//initially opened content
					$contentTabs.eq(0).addClass('active-content');
					$categories.eq(0).addClass('active-category');

					$categoriesLinks.off('click');

					$categories.each( function (n) {
						$(this).on( 'hover', function(e) {
							$(this).siblings().removeClass('active-category');
							$contentTabs.removeClass('active-content');
							$(this).addClass('active-category');
							$contentTabs.eq(n).addClass('active-content');
						});
					});

				});
			}
		}
    }


})(jQuery);