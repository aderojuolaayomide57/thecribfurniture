<div class="qodef-m-action">
	<a itemprop="url" href="<?php echo esc_url( wc_get_cart_url() ); ?>" class="qodef-m-action-link qodef-m-action-cart"><?php esc_html_e( 'View cart', 'konsept-core' ); ?></a>
	<a itemprop="url" href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="qodef-m-action-link qodef-m-action-checkout"><?php esc_html_e( 'Checkout', 'konsept-core' ); ?></a>
</div>