<a class="qodef-m-opener" href="javascript: void(0)">
	<span class="qodef-m-opener-icon"><?php echo konsept_get_svg_icon('cart', 'qodef-dropdown-cart-svg'); ?></span>
	<span class="qodef-m-opener-count"><?php echo WC()->cart->cart_contents_count; ?></span>
</a>