<?php

if ( ! function_exists( 'konsept_core_add_woo_side_area_cart_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_woo_side_area_cart_widget( $widgets ) {
		$widgets[] = 'KonseptCoreWooSideAreaCartWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_woo_side_area_cart_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreWooSideAreaCartWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_base( 'konsept_core_woo_side_area_cart' );
			$this->set_name( esc_html__( 'Konsept WooCommerce Side Area Cart', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Display a shop cart icon with that shows products count that are in the cart', 'konsept-core' ) );
			$this->set_widget_option(
				array(
					'field_type'  => 'text',
					'name'        => 'widget_padding',
					'title'       => esc_html__( 'Widget Padding', 'konsept-core' ),
					'description' => esc_html__( 'Insert padding in format: top right bottom left', 'konsept-core' )
				)
			);
		}
		
		public function load_assets() {
			wp_enqueue_style( 'perfect-scrollbar', KONSEPT_CORE_URL_PATH . 'assets/plugins/perfect-scrollbar/perfect-scrollbar.css', array() );
			wp_enqueue_script( 'perfect-scrollbar', KONSEPT_CORE_URL_PATH . 'assets/plugins/perfect-scrollbar/perfect-scrollbar.jquery.min.js', array( 'jquery' ), false, true );
		}
		
		public function render( $atts ) {
			$styles = array();
			
			if ( ! empty( $atts['widget_padding'] ) ) {
				$styles[] = 'padding: ' . $atts['widget_padding'];
			}
			?>
			<div class="qodef-woo-side-area-cart qodef-m" <?php qode_framework_inline_style( $styles ) ?>>
				<div class="qodef-woo-side-area-cart-inner qodef-m-inner">
					<?php konsept_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/opener' ); ?>
				</div>
			</div>
			<?php
		}
	}
}



if ( ! function_exists( 'konsept_core_is_side_area_cart_enabled' ) ) {
	/**
	 * Function that check is module enabled
	 */
	function konsept_core_is_side_area_cart_enabled() {
		$is_enabled = is_active_widget( false, false, 'konsept_core_woo_side_area_cart' );

		return apply_filters( 'konsept_core_filter_enable_side_area_cart', $is_enabled );
	}
}

if ( ! function_exists( 'konsept_core_load_side_area_cart_content' ) ) {
	/**
	 * Loads side area cart HTML
	 */
	function konsept_core_load_side_area_cart_content() {

		if ( konsept_core_is_side_area_cart_enabled() ) { ?>
			<div class="qodef-woo-side-area-cart-content-holder">
				<?php konsept_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/content' ); ?>
			</div>
		<?php }
	}

	add_action( 'konsept_action_before_wrapper_close_tag', 'konsept_core_load_side_area_cart_content', 10 );
}
if ( ! function_exists( 'konsept_core_woo_side_area_cart_add_to_cart_fragment' ) ) {
	function konsept_core_woo_side_area_cart_add_to_cart_fragment( $fragments ) {
		ob_start();
		?>
		<div class="qodef-woo-side-area-cart-inner qodef-m-inner">
			<?php konsept_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/parts/opener' ); ?>
		</div>
		<?php
		$fragments['.qodef-woo-side-area-cart-inner'] = ob_get_clean();
		
		return $fragments;
	}
	
	add_filter( 'woocommerce_add_to_cart_fragments', 'konsept_core_woo_side_area_cart_add_to_cart_fragment' );
}
if ( ! function_exists( 'konsept_core_woo_side_area_cart_content_add_to_cart_fragment' ) ) {
	function konsept_core_woo_side_area_cart_content_add_to_cart_fragment( $fragments ) {
		ob_start();
		?>
        <div class="qodef-woo-side-area-cart-content-holder">
			<?php konsept_core_template_part( 'plugins/woocommerce/widgets/side-area-cart', 'templates/content' ); ?>
        </div>
		<?php
		$fragments['.qodef-woo-side-area-cart-content-holder'] = ob_get_clean();

		return $fragments;
	}

	add_filter( 'woocommerce_add_to_cart_fragments', 'konsept_core_woo_side_area_cart_content_add_to_cart_fragment' );
}
