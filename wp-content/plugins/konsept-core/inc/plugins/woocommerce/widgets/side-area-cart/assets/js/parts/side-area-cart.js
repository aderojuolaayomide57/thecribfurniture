(function ($) {
	"use strict";

	$(document).ready(function () {
		qodefSideAreaCart.init();
	});

	var qodefSideAreaCart = {
		init: function () {
			var $holder = $('.qodef-woo-side-area-cart');

			if ($holder.length) {
				$holder.each(function () {
					var $thisHolder = $(this);

					qodefSideAreaCart.trigger($thisHolder);
					if (!$('.qodef-woo-side-area-cart-cover').length) {
						$('#qodef-page-wrapper').prepend('<div class="qodef-woo-side-area-cart-cover"/>');
					}

					qodefCore.body.on('added_to_cart', function () {
						qodefSideAreaCart.trigger($thisHolder);
					});
				});
			}
		},
		trigger: function ($holder) {
			var $opener = $holder.find('.qodef-m-opener'),
				$close = $holder.find('.qodef-m-close'),
				$items = $('.qodef-woo-side-area-cart-content-holder').find('.qodef-m-items');

			// Open Side Area
			$opener.on('click', function (e) {
				e.preventDefault();

				if (!$holder.hasClass('qodef--opened')) {
					qodefSideAreaCart.openSideArea($holder);

					$(document).keyup(function (e) {
						if (e.keyCode === 27) {
							qodefSideAreaCart.closeSideArea($holder);
						}
					});
				} else {
					qodefSideAreaCart.closeSideArea($holder);
				}
			});

			$close.on('click', function (e) {
				e.preventDefault();

				qodefSideAreaCart.closeSideArea($holder);
			});

			if ($items.length && typeof qodefCore.qodefPerfectScrollbar === 'object') {
				qodefCore.qodefPerfectScrollbar.init($items);
			}
		},
		openSideArea: function ($holder) {
			qodefCore.qodefScroll.disable();

			$holder.addClass('qodef--opened');
			$('.qodef-woo-side-area-cart-content-holder').addClass('qodef--opened');
			$('.qodef-svg-close-cursor').length ? $('.qodef-svg-close-cursor').addClass('qodef--visible') : null;
			qodef.body.addClass('qodef--side-cart-opened');

			$('.qodef-woo-side-area-cart-cover').on('click', function (e) {
				e.preventDefault();

				qodefSideAreaCart.closeSideArea($holder);
			});
		},
		closeSideArea: function ($holder) {
			if ($holder.hasClass('qodef--opened')) {
				qodefCore.qodefScroll.enable();
				$('.qodef-woo-side-area-cart-content-holder').removeClass('qodef--opened');
				$('.qodef-svg-close-cursor').length ? $('.qodef-svg-close-cursor').removeClass('qodef--visible') : null;
				$holder.removeClass('qodef--opened');
				qodef.body.removeClass('qodef--side-cart-opened');
			}
		}
	};

})(jQuery);
