<?php

if ( ! function_exists( 'konsept_core_add_yith_wishlist_widget' && qode_framework_is_installed('yith-wishlist') ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_yith_wishlist_widget( $widgets ) {
		$widgets[] = 'KonseptCoreYITHWishlistWidget';

		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_yith_wishlist_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreYITHWishlistWidget extends QodeFrameworkWidget {

		public function map_widget() {
			$this->set_base( 'konsept_core_yith_wishlist' );
			$this->set_name( esc_html__( 'Konsept YITH Wishlist Link', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Display a link with counter to the YITH Plugin Wishlist', 'konsept-core' ) );
            $this->set_widget_option(
                array(
                    'field_type' => 'text',
                    'name'       => 'wishlist_link',
                    'title'      => esc_html__( 'Link to Wishlist Page', 'konsept-core' )
                )
            );
		}
		
		public function render( $atts ) {

		    if(qode_framework_is_installed('yith-wishlist')) {

                if ( ! empty( $atts['wishlist_link'] ) ) {
                    $link = $atts['wishlist_link'];
                } else {
                    $link = '#';
                }

                $wishlist_count = YITH_WCWL()->count_products();

                ?>
                <div class="qodef-woo-yith-wishlist-link">
                    <a class="qodef-m-link" href="<?php echo esc_html($link); ?>">
                        <?php echo konsept_get_svg_icon('heart', 'qodef-wishlist-heart-svg'); ?>
                        <span class="qodef-m-link-count"><?php echo esc_html($wishlist_count); ?></span>
                    </a>
                </div>
                <?php
            }
		}
	}
}
?>