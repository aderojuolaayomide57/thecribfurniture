(function ($) {
    "use strict";

    var shortcode = 'konsept_core_product_list';

    qodefCore.shortcodes[shortcode] = {};

    if (typeof qodefCore.listShortcodesScripts === 'object') {
        $.each(qodefCore.listShortcodesScripts, function (key, value) {
            qodefCore.shortcodes[shortcode][key] = value;
        });
    }

    $(window).on( 'load', function() {
        qodefProductListOffset.init();
    });

    var qodefProductListOffset = {
        init: function () {
            var $holder = $('.qodef-woo-product-list.qodef-swiper-container.qodef-product-slider-offset.qodef-swiper--initialized');

            if ($holder.length) {
                $holder.each(function () {
                    var $thisHolder = $(this);

                    // Custom Pagination Functionality
                    if ($thisHolder.hasClass('qodef-swiper-has-bullets')) {
                        qodefProductListOffset.pagination($thisHolder);
                    }

                    // Calculate Swiper Offset
                    if (qodefCore.windowWidth > 680) {
                        qodefProductListOffset.calcSwiperOffset($thisHolder);

                        $(window).on('resize', function () {
                            qodefProductListOffset.calcSwiperOffset($thisHolder);
                        });
                    }
                });
            }
        },
        calcSwiperOffset: function ($holder) {
            var thisSwiper = $holder[0].swiper,
                $singleSlide = $holder.find('.swiper-slide'),
                rightMargin = 170;

            // Clear set values
            $holder.css({
                'margin-right': '',
                'width': '',
            });

            setTimeout(function () {
                rightMargin = parseInt($singleSlide.css('width')) * 0.61;
                rightMargin = parseInt(rightMargin) + 'px';

                $holder.css({
                    'margin-right': '-' + rightMargin,
                    'width': 'calc(100% + ' + rightMargin + ')',
                });

                thisSwiper.update();
            }, 10);
        },
        pagination: function ($holder) {
            var swiperInstance = $holder[0].swiper,
                holderData = holderData = JSON.parse($holder.attr('data-options')),
                slidesPerView = parseInt(holderData.slidesPerView),
                $bullets = $holder.find('.swiper-pagination .swiper-pagination-bullet');

            if (!isNaN(slidesPerView)) {
                $bullets.filter(':nth-child(' + slidesPerView + 'n+1)').addClass('qodef-pagination-bullet--isVisible');
                $bullets.not('.qodef-pagination-bullet--isVisible').css('display', 'none');

                swiperInstance.on('slideChange', function () {
                    var activePagIndex = $bullets.filter('.swiper-pagination-bullet-active').index(),
                        calcedPagIndex = Math.floor(activePagIndex / slidesPerView);
                    $bullets.removeClass('qodef-pagination-bullet--isActive');
                    $bullets.filter('.qodef-pagination-bullet--isVisible').eq(calcedPagIndex).addClass('qodef-pagination-bullet--isActive');
                });
            }
        }
    }

})(jQuery);