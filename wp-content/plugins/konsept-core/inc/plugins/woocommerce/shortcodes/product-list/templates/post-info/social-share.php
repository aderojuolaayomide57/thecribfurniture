<?php if ( class_exists( 'KonseptCoreSocialShareShortcode' ) ) { ?>
	<div class="qodef-woo-product-social-share">
		<?php
		$params = array();
		$params['title'] = esc_html__( 'Share:', 'konsept-core' );
		
		echo KonseptCoreSocialShareShortcode::call_shortcode( $params ); ?>
	</div>
<?php } ?>