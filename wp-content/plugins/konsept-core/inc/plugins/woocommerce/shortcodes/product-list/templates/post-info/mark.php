<?php
if(konsept_core_woo_has_some_additional_mark()) { ?>
	<div class="qodef-woo-product-marks-holder">
        <?php do_action( 'konsept_core_action_woo_product_mark_info' ); ?>
    </div>
 <?php } else {
	// Hook to include product mark
	do_action('konsept_core_action_woo_product_mark_info');
}