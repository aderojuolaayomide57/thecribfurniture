<?php

if ( ! function_exists( 'konsept_core_add_product_list_variation_presentational' ) ) {
	function konsept_core_add_product_list_variation_presentational( $variations ) {
		$variations['presentational'] = esc_html__( 'Presentational', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_product_list_layouts', 'konsept_core_add_product_list_variation_presentational' );
}

if ( ! function_exists( 'konsept_core_register_shop_list_presentational_actions' ) ) {
	function konsept_core_register_shop_list_presentational_actions() {
		
		// Add additional tags around product list item
		add_action( 'woocommerce_before_shop_loop_item', 'konsept_add_product_list_item_holder', 5 ); // permission 5 is set because woocommerce_template_loop_product_link_open hook is added on 10
		add_action( 'woocommerce_after_shop_loop_item', 'konsept_add_product_list_item_holder_end', 30 ); // permission 30 is set because woocommerce_template_loop_add_to_cart hook is added on 10
		
		// Add additional tags around product list item image
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_image_holder', 5 ); // permission 5 is set because woocommerce_show_product_loop_sale_flash hook is added on 10
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_image_holder_end', 26 ); // permission 30 is set because woocommerce_template_loop_product_thumbnail hook is added on 10

		// Add additional tags around content inside product list item image
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_additional_image_holder_end', 18 );

        add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 18 );
        add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 18 );

		// Change title position on product list
		remove_action( 'woocommerce_shop_loop_item_title', 'konsept_woo_shop_loop_item_title', 10 ); // permission 10 is default

        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_on_image', 27 ); //info splitter holder opened
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_left_holder', 28 ); //left part of info opened
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_woo_shop_loop_item_title', 29 ); // permission > 15 is set because konsept_add_product_list_item_additional_image_holder hook is added on 15
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_core_woo_get_product_categories', 30 ); // permission > 15 is set because konsept_add_product_list_item_additional_image_holder hook is added on 15
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_end', 31 ); // left part of info closed

		// Change price position on product list
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 ); // permission 10 is default

        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_right_holder', 32 ); //right part of info opened
        add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 33 ); // permission >19 is set because konsept_woo_shop_loop_item_title hook is added on 19
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_end', 35 ); //right part of info closed
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_end', 35 ); //info splitter holder opened

        // Change add to cart position on product list
//		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 ); // permission 10 is default
//		add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 18 ); // permission 18 is set because konsept_add_product_list_item_additional_image_holder hook is added on 15
	}
	
	add_action( 'konsept_core_action_shop_list_item_layout_info-on-image', 'konsept_core_register_shop_list_presentational_actions' );
}