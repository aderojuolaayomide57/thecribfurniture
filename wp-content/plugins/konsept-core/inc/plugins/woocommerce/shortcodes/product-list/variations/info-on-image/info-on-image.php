<?php

if ( ! function_exists( 'konsept_core_add_product_list_variation_info_on_image' ) ) {
	function konsept_core_add_product_list_variation_info_on_image( $variations ) {
		$variations['info-on-image'] = esc_html__( 'Info On Image', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_product_list_layouts', 'konsept_core_add_product_list_variation_info_on_image' );
}

if ( ! function_exists( 'konsept_core_register_shop_list_info_on_image_actions' ) ) {
	function konsept_core_register_shop_list_info_on_image_actions() {
		
		// Add additional tags around product list item
		add_action( 'woocommerce_before_shop_loop_item', 'konsept_add_product_list_item_holder', 5 ); // permission 5 is set because woocommerce_template_loop_product_link_open hook is added on 10
		add_action( 'woocommerce_after_shop_loop_item', 'konsept_add_product_list_item_holder_end', 30 ); // permission 30 is set because woocommerce_template_loop_add_to_cart hook is added on 10
		
		// Add additional tags around product list item image
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_image_holder', 5 ); // permission 5 is set because woocommerce_show_product_loop_sale_flash hook is added on 10
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_image_holder_end', 26 ); // permission 30 is set because woocommerce_template_loop_product_thumbnail hook is added on 10

		// Add additional tags around content inside product list item image
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_additional_image_holder', 15 ); // permission 15 is set because woocommerce_template_loop_product_thumbnail hook is added on 10
		add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_additional_image_holder_end', 19 ); // permission 25 is set because konsept_add_product_list_item_image_holder_end hook is added on 30

        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_core_get_yith_holder', 16 );
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_core_get_yith_wishlist_shortcode', 16 );
        add_action( 'konsept_core_action_product_list_additional_buttons', 'konsept_core_get_yith_wishlist_shortcode', 16 );


        if( konsept_core_include_yith_quick_view_plugin_is_installed(true, 'yith-quick-view') ) {
            $wcqv = new YITH_WCQV;

            if($wcqv ->load_frontend()) {
                add_action( 'woocommerce_before_shop_loop_item_title', array( YITH_WCQV_Frontend(), 'yith_add_quick_view_button' ), 17 );
                add_action( 'konsept_core_action_product_list_additional_buttons', array( YITH_WCQV_Frontend(), 'yith_add_quick_view_button' ), 17 );

            }
        }
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_additional_image_holder_end', 18 );

        add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 18 );
        add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 18 );

		// Change title position on product list
		remove_action( 'woocommerce_shop_loop_item_title', 'konsept_woo_shop_loop_item_title', 10 ); // permission 10 is default

        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_on_image', 27 ); //info splitter holder opened
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_left_holder', 28 ); //left part of info opened
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_woo_shop_loop_item_title', 29 ); // permission > 15 is set because konsept_add_product_list_item_additional_image_holder hook is added on 15
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_core_woo_get_product_categories', 30 ); // permission > 15 is set because konsept_add_product_list_item_additional_image_holder hook is added on 15
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_end', 31 ); // left part of info closed

		// Change price position on product list
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 ); // permission 10 is default

        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_right_holder', 32 ); //right part of info opened
        add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_price', 33 ); // permission >19 is set because konsept_woo_shop_loop_item_title hook is added on 19
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_end', 35 ); //right part of info closed
        add_action( 'woocommerce_before_shop_loop_item_title', 'konsept_add_product_list_item_splitter_holder_end', 35 ); //info splitter holder opened

        if( konsept_core_include_yith_color_and_label_variations_plugin_is_installed(true, 'yith-color-and-label-variations') ) {
            if ( ! is_admin() && function_exists( 'YITH_WCCL_Frontend' ) ) {
                remove_action( 'woocommerce_loop_add_to_cart_link', array( YITH_WCCL_Frontend(), 'add_select_options' ), 100 );
                add_action( 'woocommerce_before_shop_loop_item_title', array( YITH_WCCL_Frontend(), 'print_select_options' ), 30 );
                add_action( 'konsept_core_action_product_list_item_additional_content', array( YITH_WCCL_Frontend(), 'print_select_options' ), 30 );
            }
        }

        // Change add to cart position on product list
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 ); // permission 10 is default
		add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 18 ); // permission 18 is set because konsept_add_product_list_item_additional_image_holder hook is added on 15
	}
	
	add_action( 'konsept_core_action_shop_list_item_layout_info-on-image', 'konsept_core_register_shop_list_info_on_image_actions' );
}