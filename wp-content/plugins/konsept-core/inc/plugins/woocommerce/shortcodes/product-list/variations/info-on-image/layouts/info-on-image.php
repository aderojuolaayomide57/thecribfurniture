<div <?php wc_product_class( $item_classes ); ?>>
    <div class="qodef-woo-product-inner">
		<?php if ( has_post_thumbnail() ) { ?>
            <div class="qodef-woo-product-image">
				<?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/mark' ); ?>
				<?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/image', '', $params ); ?>
                <div class="qodef-woo-product-image-inner" <?php echo qode_framework_inline_style($item_styles); ?>>
                    <div class="qodef-woo-product-additional-icons">
                        <?php // Hook to include additional buttons for Wishlist & Quick View
                        if($params['additional_buttons'] === 'yes') {
                            do_action( 'konsept_core_action_product_list_additional_buttons' );
                        }
                        ?>
                    </div>
                    <?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/add-to-cart' ); ?>
					<?php
					// Hook to include additional content inside product list item image
					do_action( 'konsept_core_action_product_list_item_additional_image_content' );
					?>

                    <?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/link' ); ?>
                </div>
                <div class="qodef-woo-split-cols">
                    <div class="qodef-woo-product-info">
                        <?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/title', '', $params ); ?>
                        <?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/category', '', $params ); ?>
                        <?php //konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/rating', '', $params ); ?>
                        <?php // Hook to include additional buttons for Color Variations
                        do_action( 'konsept_core_action_product_list_item_additional_content' );
                        ?>
                    </div>
                    <div class="qodef-woo-product-price-holder">
                        <?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-list', 'templates/post-info/price', '', $params ); ?>
                    </div>
                </div>
            </div>
		<?php } ?>
    </div>
</div>