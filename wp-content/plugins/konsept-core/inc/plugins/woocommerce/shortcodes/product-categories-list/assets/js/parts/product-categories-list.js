(function ($) {
    "use strict";
    
	qodefCore.shortcodes.konsept_core_product_categories_list = {};
	qodefCore.shortcodes.konsept_core_product_categories_list.qodefMasonryLayout = qodef.qodefMasonryLayout;
	qodefCore.shortcodes.konsept_core_product_categories_list.qodefSwiper = qodef.qodefSwiper;

})(jQuery);