<?php

if ( ! function_exists( 'konsept_core_add_product_categories_list_variation_info_on_image' ) ) {
	function konsept_core_add_product_categories_list_variation_info_on_image( $variations ) {
		$variations['info-on-image'] = esc_html__( 'Info On Image', 'konsept-core' );

		return $variations;
	}

	add_filter( 'konsept_core_filter_product_categories_list_layouts', 'konsept_core_add_product_categories_list_variation_info_on_image' );
}