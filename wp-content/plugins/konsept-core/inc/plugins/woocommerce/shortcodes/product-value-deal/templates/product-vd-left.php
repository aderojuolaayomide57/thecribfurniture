<?php if ($query_result->have_posts()): while ($query_result->have_posts()) : $query_result->the_post(); ?>
    <div class="qodef-pvd qodef-pvd-left">
        <div class="qodef-e-inner">
            <div class="qodef-e-image">
                <?php konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/image', '', $params ); ?>
            </div>
            <a class="qodef-e-link" itemprop="url" href="<?php the_permalink(); ?>"
               title="<?php the_title_attribute(); ?>"></a>
            <div class="qodef-e-content">
                <?php
                konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/tagline', '', $params );
                konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/title', '', $params );
                konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/price', '', $params );
                konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/excerpt', '', $params );
                konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/value-deal', '', $params);
                konsept_core_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/post-info/add-to-cart', '', $params )
                ?>
            </div>
        </div>
    </div>
<?php endwhile;
else: ?>
    <li class="qodef-product-pvd-messsage">
        <?php konsept_core_theme_template_part( 'content', 'templates/parts/posts-not-found' ); ?>
    </li>
<?php endif;
wp_reset_postdata();
?>