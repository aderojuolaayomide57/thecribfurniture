<?php
$tagline = ! empty( $tagline ) ? $tagline : '';
?>

<?php if(! empty( $tagline )) { ?>
    <div class="qodef-e-tagline" <?php qode_framework_inline_style( $this_shortcode->get_tagline_styles( $params ) ); ?>>
        <?php echo esc_html($tagline); ?>
    </div>
<?php } ?>
