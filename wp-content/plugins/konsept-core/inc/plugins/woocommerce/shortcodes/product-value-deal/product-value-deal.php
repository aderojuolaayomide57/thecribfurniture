<?php

if ( ! function_exists( 'konsept_core_add_product_value_deal_shortcode' ) ) {
    /**
     * Function that is adding shortcode into shortcodes list for registration
     *
     * @param array $shortcodes - Array of registered shortcodes
     *
     * @return array
     */
    function konsept_core_add_product_value_deal_shortcode( $shortcodes ) {
        $shortcodes[] = 'KonseptCoreProductValueDealShortcode';

        return $shortcodes;
    }

    add_filter( 'konsept_core_filter_register_shortcodes', 'konsept_core_add_product_value_deal_shortcode' );
}

if ( class_exists( 'KonseptCoreListShortcode' ) ) {
    class KonseptCoreProductValueDealShortcode extends KonseptCoreListShortcode {

        public function __construct() {
            $this->set_post_type( 'product' );
            $this->set_layouts( apply_filters( 'konsept_core_filter_product_list_layouts', array() ) );
            $this->set_extra_options( apply_filters( 'konsept_core_filter_product_list_extra_options', array() ) );

            parent::__construct();
        }

        public function map_shortcode() {
            $this->set_shortcode_path( KONSEPT_CORE_PLUGINS_URL_PATH . '/woocommerce/shortcodes/product-value-deal' );
            $this->set_base( 'konsept_core_product_value_deal' );
            $this->set_name( esc_html__( 'Product Value Deal', 'konsept-core' ) );
            $this->set_description( esc_html__( 'Shortcode that displays product with a value deal', 'konsept-core' ) );
            $this->set_category( esc_html__( 'Konsept Core', 'konsept-core' ) );

            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'custom_class',
                'title'      => esc_html__( 'Custom Class', 'konsept-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'tagline',
                'title'      => esc_html__( 'Tagline', 'konsept-core' )
            ) );
            $this->set_option( array(
                'field_type' => 'color',
                'name'       => 'tagline_color',
                'title'      => esc_html__( 'Tagline Color', 'konsept-core' )
            ) );
            $this->set_option( array(
                'field_type'        => 'select',
                'name'  => 'product_vd_image_position',
                'title'     => esc_html__('Image Position', 'konsept-core'),
                'options'       => array(
                    'left' => esc_html__('Left', 'konsept-core'),
                    'middle' => esc_html__('Middle', 'konsept-core')
                )
            ));
            $this->map_list_options( array(
                'exclude_behavior' => array( 'masonry', 'justified-gallery','slider' ),
                'exclude_option'   => array( 'behavior', 'columns', 'space')
            ) );
            $this->map_query_options( array( 'post_type' => $this->get_post_type() ) );
            $this->map_layout_options();
            $this->set_option( array(
                'field_type' => 'text',
                'name'       => 'excerpt_length',
                'title'      => esc_html__( 'Excerpt Length', 'konsept-core' ),
                'group'      => esc_html__( 'Layout', 'konsept-core' )
            ) );
            $this->map_extra_options();
        }

        public static function call_shortcode( $params ) {
            $html = qode_framework_call_shortcode( 'konsept_core_product_value_deal', $params );
            $html = str_replace( "\n", '', $html );

            return $html;
        }

        public function render( $options, $content = null ) {
            parent::render( $options );

            $atts = $this->get_atts();

            $atts['post_type']       = $this->get_post_type();
            $atts['taxonomy_filter'] = $this->get_post_type_taxonomy();
			$atts['additional_query_args'] = $this->get_additional_query_args( $atts );
            $atts['holder_classes'] = $this->get_holder_classes( $atts );
            $atts['query_result']   = new \WP_Query( konsept_core_get_query_params( $atts ) );

            $atts['this_shortcode'] = $this;
            return konsept_core_get_template_part( 'plugins/woocommerce/shortcodes/product-value-deal', 'templates/product-vd', $atts['product_vd_image_position'], $atts );
        }

        private function get_holder_classes(  ) {
            $holder_classes = $this->init_holder_classes();

            $holder_classes[] = 'qodef-woo-shortcode';
            $holder_classes[] = 'qodef-woo-product-value-deal';

            return implode( ' ', $holder_classes );
        }

        public function get_item_classes( $atts ) {
            $item_classes      = $this->init_item_classes();
            $list_item_classes = $this->get_list_item_classes( $atts );

            $item_classes = array_merge( $item_classes, $list_item_classes );

            return implode( ' ', $item_classes );
        }

        public function get_title_styles( $atts ) {
            $styles = array();

            if ( ! empty( $atts['text_transform'] ) ) {
                $styles[] = 'text-transform: ' . $atts['text_transform'];
            }

            return $styles;
        }

        public function get_tagline_styles( $atts ) {
            $styles = array();

            if ( ! empty( $atts['tagline_color'] ) ) {
                $styles[] = 'color: ' . $atts['tagline_color'];
            }

            return $styles;
        }
    }
}