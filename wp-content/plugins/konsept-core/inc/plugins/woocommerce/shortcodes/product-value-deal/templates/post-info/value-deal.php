<?php

$sale_price_to = get_post_meta(get_the_ID(), '_sale_price_dates_to', true);

if (!empty($sale_price_to)) {

    $sale_price_date = date("Y/m/d",$sale_price_to);

    if (!empty($sale_price_date)) {
        $id = mt_rand(1000, 9999);
        $countdown_params = array(
                'layout'  => 'boxed',
                'custom_class'  => 'qodef-vd-countdown',
                'date' => $sale_price_date
        );

        ?>

        <div class="qodef-vd-countdown-holder">
            <div class="qodef-vd-countdown-holder-inner">
                <?php
				echo KonseptCoreCountdownShortcode::call_shortcode($countdown_params);
                ?>
            </div>
        </div>

    <?php }
} ?>