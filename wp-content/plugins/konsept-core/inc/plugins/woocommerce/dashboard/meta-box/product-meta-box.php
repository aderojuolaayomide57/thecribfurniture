<?php

if ( ! function_exists( 'konsept_core_add_product_single_meta_box' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function konsept_core_add_product_single_meta_box() {
		$qode_framework = qode_framework_get_framework_root();

        $single = $qode_framework->add_options_page(
            array(
                'scope' => array( 'product' ),
                'type'  => 'meta',
                'slug'  => 'product-single',
                'title' => esc_html__( 'Product Single', 'konsept-core' )
            )
        );

		$page = $qode_framework->add_options_page(
			array(
				'scope' => array( 'product' ),
				'type'  => 'meta',
				'slug'  => 'product-list',
				'title' => esc_html__( 'Product List', 'konsept-core' )
			)
		);

		if ( $single ) {

            $single->add_field_element(
                array(
                    'field_type' => 'select',
                    'name' => 'qodef_product_layout',
                    'title' => esc_html__('Product layout', 'konsept-core'),
                    'description' => esc_html__('Choose layout for product single page', 'konsept-core'),
                    'options' => array(
                        'default' => esc_html__('Default', 'konsept-core'),
                        'gallery' => esc_html__('Gallery', 'konsept-core'),
                        'big-images' => esc_html__('Big Images', 'konsept-core'),
                        'sticky-info' => esc_html__('Sticky Info', 'konsept-core')
                    )
                )
            );

            $single->add_field_element(
                array(
                    'field_type' => 'image',
                    'name' => 'qodef_product_images_gallery',
                    'title' => esc_html__('Upload Gallery Images', 'konsept-core'),
                    'multiple' => 'yes',
                    'dependency' => array(
                        'hide' => array(
                            'qodef_product_layout' => array(
                                'values' => 'default',
                                'default_value' => 'default'
                            )
                        )
                    )
                )
            );
        }

        if ( $page ) {

			$page->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_product_list_image',
					'title'       => esc_html__( 'Product List Image', 'konsept-core' ),
					'description' => esc_html__( 'Upload image to be displayed on product list instead of featured image', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_masonry_image_dimension_product',
					'title'       => esc_html__( 'Image Dimension', 'konsept-core' ),
					'description' => esc_html__( 'Choose an image layout for product list. If you are using fixed image proportions on the list, choose an option other than default', 'konsept-core' ),
					'options'     => konsept_core_get_select_type_options_pool( 'masonry_image_dimension' )
				)
			);

            $page->add_field_element(
                array(
                    'field_type'  => 'color',
                    'name'        => 'qodef_product_list_hover_color',
                    'title'       => esc_html__( 'Hover Color for Product List', 'konsept-core' ),
                    'description' => esc_html__( 'Set the background color for hovers on Product List', 'konsept-core' )
                )
            );

            $page->add_field_element(
                array(
                    'field_type'  => 'text',
                    'name'        => 'qodef_product_masonry_list_padding',
                    'title'       => esc_html__( 'Product Padding for Masonry List', 'konsept-core' ),
                    'description' => esc_html__( 'Set padding for the product to appear asymmetric on Masonry Product List', 'konsept-core' )
                )
            );

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_show_new_sign',
					'title'         => esc_html__( 'Show New Sign', 'konsept-core' ),
					'description'   => esc_html__( 'Enabling this option will show "New Sign" mark on product.', 'konsept-core' ),
					'options'       => konsept_core_get_select_type_options_pool( 'no_yes' ),
					'default_value' => 'no'
				)
			);

			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_product_single_meta_box_map', $page );
		}
	}

	add_action( 'konsept_core_action_default_meta_boxes_init', 'konsept_core_add_product_single_meta_box' );
}