<div class="qodef-instagram-holder qodef-swiper-arrows-outside <?php echo esc_attr($holder_classes) ?>" <?php qode_framework_inline_attr( $slider_attr, 'data-options' ); ?>>
	<?php echo do_shortcode( "[instagram-feed class=qodef-instagram-swiper-container $instagram_params]" ); // XSS OK ?>
	<div class="swiper-button-next swiper-button-next-<?php echo esc_attr($unique); ?> swiper-button-outside"><?php konsept_svg_icon('arrow'); ?></div>
	<div class="swiper-button-prev swiper-button-prev-<?php echo esc_attr($unique); ?> swiper-button-outside"><?php konsept_svg_icon('arrow'); ?></div>
</div>