<?php

if ( ! function_exists( 'konsept_core_add_instagram_list_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_instagram_list_widget( $widgets ) {
		$widgets[] = 'KonseptCoreInstagramListWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_instagram_list_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreInstagramListWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$this->set_widget_option(
				array(
					'field_type' => 'text',
					'name'       => 'widget_title',
					'title'      => esc_html__( 'Title', 'konsept-core' )
				)
			);
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'konsept_core_instagram_list'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'konsept_core_instagram_list' );
				$this->set_name( esc_html__( 'Konsept Instagram List', 'konsept-core' ) );
				$this->set_description( esc_html__( 'Add a instagram list element into widget areas', 'konsept-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[konsept_core_instagram_list $params]" ); // XSS OK
		}
	}
}