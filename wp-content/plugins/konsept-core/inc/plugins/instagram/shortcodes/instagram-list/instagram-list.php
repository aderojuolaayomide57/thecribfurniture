<?php

if ( ! function_exists( 'konsept_core_add_instagram_list_shortcode' ) ) {
	/**
	 * Function that is adding shortcode into shortcodes list for registration
	 *
	 * @param array $shortcodes - Array of registered shortcodes
	 *
	 * @return array
	 */
	function konsept_core_add_instagram_list_shortcode( $shortcodes ) {
		if( qode_framework_is_installed( 'instagram' ) ) {
			$shortcodes[] = 'KonseptCoreInstagramListShortcode';
		}
		
		return $shortcodes;
	}
	
	add_filter( 'konsept_core_filter_register_shortcodes', 'konsept_core_add_instagram_list_shortcode' );
}

if ( class_exists( 'KonseptCoreShortcode' ) ) {
	class KonseptCoreInstagramListShortcode extends KonseptCoreShortcode {
		
		public function map_shortcode() {
			$this->set_shortcode_path( KONSEPT_CORE_PLUGINS_URL_PATH . '/instagram/shortcodes/instagram-list' );
			$this->set_base( 'konsept_core_instagram_list' );
			$this->set_name( esc_html__( 'Instagram List', 'konsept-core' ) );
			$this->set_description( esc_html__( 'Shortcode that displays instagram list', 'konsept-core' ) );
			$this->set_category( esc_html__( 'Konsept Core', 'konsept-core' ) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'custom_class',
				'title'      => esc_html__( 'Custom Class', 'konsept-core' )
			) );
            $this->set_option( array(
                'field_type'  => 'text',
                'name'        => 'user',
                'title'       => esc_html__( 'User Account', 'konsept-core' ),
                'description' => esc_html__( 'If left empty, default user account will be displayed', 'konsept-core' )

            ) );
			$this->set_option( array(
				'field_type' => 'text',
				'name'       => 'photos_number',
				'title'      => esc_html__( 'Number of Photos', 'konsept-core' )
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'columns_number',
				'title'         => esc_html__( 'Number of Columns', 'konsept-core' ),
				'options'       => array(
					'1'  => esc_html__( '1', 'konsept-core' ),
					'2'  => esc_html__( '2', 'konsept-core' ),
					'3'  => esc_html__( '3', 'konsept-core' ),
					'4'  => esc_html__( '4', 'konsept-core' ),
					'5'  => esc_html__( '5', 'konsept-core' ),
					'6'  => esc_html__( '6', 'konsept-core' ),
					'7'  => esc_html__( '7', 'konsept-core' ),
					'8'  => esc_html__( '8', 'konsept-core' ),
					'9'  => esc_html__( '9', 'konsept-core' ),
					'10' => esc_html__( '10', 'konsept-core' ),
				),
				'default_value' => '3'
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'space',
				'title'         => esc_html__( 'Padding Around Images', 'konsept-core' ),
				'options'       => konsept_core_get_select_type_options_pool( 'items_space' ),
				'default_value' => 'small'
			) );
			$this->set_option( array(
				'field_type'    => 'select',
				'name'          => 'image_resolution',
				'title'         => esc_html__( 'Image Resolution', 'konsept-core' ),
				'options'       => array(
					'auto'   => esc_html__( 'Auto-detect (recommended)', 'konsept-core' ),
					'thumb'  => esc_html__( 'Thumbnail (150x150)', 'konsept-core' ),
					'medium' => esc_html__( 'Medium (306x306)', 'konsept-core' ),
					'full'   => esc_html__( 'Full (640x640)', 'konsept-core' )
				),
				'default_value' => 'auto'
			) );
			if ( ! class_exists( 'SB_Instagram_Feed_Pro' ) ) {
				$this->set_option( array(
					'field_type'    => 'select',
					'name'          => 'behavior',
					'title'         => esc_html__( 'List Appearance', 'konsept-core' ),
					'options'       => konsept_core_get_select_type_options_pool( 'list_behavior', false, array( 'masonry', 'justified-gallery' ) ),
					'default_value' => 'columns',
				) );
			}
		}
		
		public function render( $options, $content = null ) {
			parent::render( $options );
			
			$atts = $this->get_atts();

			$atts['unique'] 		  = wp_rand(999, 9999);
			$atts['behavior']         = isset( $atts['behavior'] ) ? $atts['behavior'] : '';
			$atts['holder_classes']   = $this->get_holder_classes( $atts );
			$atts['instagram_params'] = $this->get_instagram_params( $atts );
			$atts['slider_attr']      = $this->get_slider_data( $atts );
			
			return konsept_core_get_template_part( 'plugins/instagram/shortcodes/instagram-list', 'templates/instagram-list', $atts['behavior'], $atts );
		}
		
		private function get_holder_classes( $atts ) {
			$holder_classes = $this->init_holder_classes();
			
			$holder_classes[] = 'qodef-instagram-list';
			$holder_classes[] = ! empty( $atts['behavior'] ) ? 'qodef-layout--' . $atts['behavior'] : '';
			$holder_classes[] = ! empty( $atts['space'] ) ? 'qodef-gutter--' . $atts['space'] : '';
			$holder_classes[] = ! empty( $atts['columns_number'] ) ? 'qodef-col-num--' . $atts['columns_number'] : '';

			$holder_classes = array_merge( $holder_classes );
			
			return implode( ' ', $holder_classes );
		}
		
		private function get_instagram_params( $atts ) {
			$params = array();
			
			$params['num']              = isset( $atts['photos_number'] ) && ! empty( $atts['photos_number'] ) ? $atts['photos_number'] : 6;
			$params['cols']             = isset( $atts['columns_number'] ) && ! empty( $atts['columns_number'] ) ? $atts['columns_number'] : 3;
			$params['imagepadding']     = isset( $atts['space'] ) && ! empty( $atts['space'] ) ? konsept_core_get_space_value( $atts['space'] ) : 10;
			$params['imagepaddingunit'] = 'px';
			$params['showheader']       = false;
			$params['showfollow']       = false;
			$params['showbutton']       = false;
			$params['imageres']         = isset( $atts['image_resolution'] ) && ! empty( $atts['image_resolution'] ) ? $atts['image_resolution'] : 'auto';
			$params['user']             = isset( $atts['user'] ) && ! empty( $atts['user'] ) ? $atts['user'] : '';

			if ( is_array( $params ) && count( $params ) ) {
				foreach ( $params as $key => $value ) {
					if ( $value !== '' ) {
						$params[] = $key . "='" . esc_attr( str_replace( ' ', '', $value ) ) . "'";
					}
				}
			}
			
			return implode( ' ', $params );
		}
		
		private function get_slider_data( $atts ) {
			$data = array();

			$data['unique'] = $atts['unique'];
			$data['outsideNavigation'] = 'yes';
			$data['slidesPerView'] = isset( $atts['columns_number'] ) ? $atts['columns_number'] : 4;
			$data['spaceBetween']  = isset( $atts['space'] ) ? konsept_core_get_space_value( $atts['space'] ) : 0;
			
			return json_encode( $data );
		}

	}
}