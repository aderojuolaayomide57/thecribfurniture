<div class="qodef-centered-header-wrapper">

<?php // Include widget area two ?>
    <div class="qodef-widget-holder">
        <?php konsept_core_get_header_widget_area( '', 'two' ); ?>
    </div>
    <?php
    // Include logo
    konsept_core_get_header_logo_image();


    // Include widget area one ?>
    <div class="qodef-widget-holder">
        <?php konsept_core_get_header_widget_area(); ?>
    </div>
</div>

<?php

// Include main navigation
konsept_core_template_part( 'header', 'templates/parts/navigation' ); ?>
