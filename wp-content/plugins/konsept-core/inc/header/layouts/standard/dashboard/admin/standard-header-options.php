<?php

if ( ! function_exists( 'konsept_core_add_standard_header_options' ) ) {
	function konsept_core_add_standard_header_options( $page, $general_header_tab ) {
		
		$section = $general_header_tab->add_section_element(
			array(
				'name'        => 'qodef_standard_header_section',
				'title'       => esc_html__( 'Standard Header', 'konsept-core' ),
				'description' => esc_html__( 'Standard header settings', 'konsept-core' ),
				'dependency'  => array(
					'show'    => array(
						'qodef_header_layout' => array(
							'values' => 'standard',
							'default_value' => ''
						)
					)
				)
			)
		);

		$section->add_field_element(
			array(
				'field_type'  => 'yesno',
				'name'        => 'qodef_standard_header_in_grid',
				'title'       => esc_html__( 'Content in Grid', 'konsept-core' ),
				'description' => esc_html__( 'Set content to be in grid', 'konsept-core' ),
				'default_value' => 'no',
				'args'        => array(
					'suffix' => esc_html__( 'px', 'konsept-core' )
				)
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'  => 'text',
				'name'        => 'qodef_standard_header_height',
				'title'       => esc_html__( 'Header Height', 'konsept-core' ),
				'description' => esc_html__( 'Enter header height', 'konsept-core' ),
				'args'        => array(
					'suffix' => esc_html__( 'px', 'konsept-core' )
				)
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'  => 'text',
				'name'        => 'qodef_standard_header_side_padding',
				'title'       => esc_html__( 'Header Side Padding', 'konsept-core' ),
				'description' => esc_html__( 'Enter side padding for header area', 'konsept-core' ),
				'args'        => array(
					'suffix' => esc_html__( 'px or %', 'konsept-core' )
				)
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'  => 'color',
				'name'        => 'qodef_standard_header_background_color',
				'title'       => esc_html__( 'Header Background Color', 'konsept-core' ),
				'description' => esc_html__( 'Enter header background color', 'konsept-core' )
			)
		);
		
		$section->add_field_element(
			array(
				'field_type'    => 'select',
				'name'          => 'qodef_standard_header_menu_position',
				'title'         => esc_html__( 'Menu position', 'konsept-core' ),
				'default_value' => 'right',
				'options'       => array(
					'left'   => esc_html__( 'Left', 'konsept-core' ),
					'center' => esc_html__( 'Center', 'konsept-core' ),
					'right'  => esc_html__( 'Right', 'konsept-core' ),
				)
			)
		);
	}
	
	add_action( 'konsept_core_action_after_header_options_map', 'konsept_core_add_standard_header_options', 10, 2 );
}