<?php

if ( ! function_exists( 'konsept_core_add_standard_centered_header_global_option' ) ) {
	/**
	 * This function set header type value for global header option map
	 */
	function konsept_core_add_standard_centered_header_global_option( $header_layout_options ) {
		$header_layout_options['standard-centered'] = array(
			'image' => KONSEPT_CORE_HEADER_LAYOUTS_URL_PATH . '/standard-centered/assets/img/standard-centered-header.png',
			'label' => esc_html__( 'Standard Centered', 'konsept-core' )
		);

		return $header_layout_options;
	}

	add_filter( 'konsept_core_filter_header_layout_option', 'konsept_core_add_standard_centered_header_global_option' );
}

if ( ! function_exists( 'konsept_core_register_standard_centered_header_layout' ) ) {
	function konsept_core_register_standard_centered_header_layout( $header_layouts ) {
		$header_layout = array(
			'standard-centered' => 'StandardCenteredHeader'
		);

		$header_layouts = array_merge( $header_layouts, $header_layout );

		return $header_layouts;
	}

	add_filter( 'konsept_core_filter_register_header_layouts', 'konsept_core_register_standard_centered_header_layout');
}

if ( ! function_exists( 'konsept_core_register_standard_centered_menu' ) ) {
    function konsept_core_register_standard_centered_menu($menus) {

        $menus['standard-centered-navigation']  = esc_html__( 'Standard Centered Header Navigation', 'konsept-core' );

        return $menus;
    }
    add_filter('konsept_filter_register_navigation_menus','konsept_core_register_standard_centered_menu');
}