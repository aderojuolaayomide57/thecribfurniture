<?php if ( has_nav_menu( 'standard-centered-navigation' ) ) : ?>
	<nav class="qodef-header-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Standard Centered Header Menu', 'konsept-core' ); ?>">
		<?php wp_nav_menu(
			array(
				'theme_location' => 'standard-centered-navigation',
				'container'      => '',
				'link_before'    => '<span class="qodef-menu-item-text">',
				'link_after'     => '</span>',
				'walker'         => new KonseptCoreRootMainMenuWalker()
			)
		); ?>
	</nav>
<?php endif; ?>