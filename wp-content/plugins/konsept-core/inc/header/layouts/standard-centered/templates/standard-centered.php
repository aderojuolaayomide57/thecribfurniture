<?php
// Include divided left navigation
konsept_core_template_part( 'header/layouts/standard-centered', 'templates/parts/standard-centered-navigation' );

// Include logo
konsept_core_get_header_logo_image();

// Include widget area one
if ( is_active_sidebar( 'qodef-header-widget-area-one' ) ) { ?>
    <div class="qodef-widget-holder">
        <?php konsept_core_get_header_widget_area(); ?>
    </div>
<?php } ?>
