<?php

class StandardCenteredHeader extends KonseptCoreHeader {
	private static $instance;

	public function __construct() {
		$this->set_slug( 'standard-centered' );
		$this->default_header_height = 100;

		parent::__construct();
	}
	
	public static function get_instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		
		return self::$instance;
	}
}