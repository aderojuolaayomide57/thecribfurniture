<div class="qodef-header-section qodef-header-section-top">
    <?php

    // Include logo
    konsept_core_get_header_logo_image();
    ?>

    <div class="qodef-widget-holder">
        <?php
        // Include widget area one
        if ( is_active_sidebar( 'qodef-header-widget-area-one' ) ) { ?>
            <?php konsept_core_get_header_widget_area(); ?>
        <?php } ?>
    </div>
</div>
<div class="qodef-header-section qodef-header-section-bottom">
    <?php konsept_core_get_extended_dropdown_menu(); ?>

    <?php // Include main navigation
    konsept_core_template_part( 'header', 'templates/parts/navigation' ); ?>

    <div class="qodef-widget-holder">
        <?php
        // Include widget area two
        if ( is_active_sidebar( 'qodef-header-widget-area-two' ) ) { ?>
            <?php konsept_core_get_header_widget_area( '', 'two' ); ?>
        <?php } ?>
    </div>
</div>


