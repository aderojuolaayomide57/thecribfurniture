// (function ($) { TBR
//     "use strict";
//
//     $(window).on('load', function () {
//         qodefExtendedDropDown.init();
//     });
//
//     /**
//      * Function object that represents extended dropdown menu.
//      * @returns {{init: Function}}
//      */
//
//     var qodefExtendedDropDown = {
//         init: function () {
//             var menuItemsHolders = $('.qodef-extended-dropdown-menu > ul > li.qodef-menu-item--wide > .sub-menu');
//
//             menuItemsHolders.each(function () {
//                 var menuItemsHolder = $(this);
//
//                 var dropDownItems = menuItemsHolder.find('> li');
//                 var dropDownItemsCount = dropDownItems.length > 2 ? 2 : dropDownItems.length;
//
//                 var menuItemsWidth = dropDownItems.outerWidth();
//                 menuItemsHolder.css({'width': dropDownItemsCount * menuItemsWidth});
//             })
//         }
//     };
//
// })(jQuery);