<?php

if ( ! function_exists( 'konsept_core_add_standard_extended_header_options' ) ) {
    function konsept_core_add_standard_extended_header_options( $page, $general_header_tab ) {

        $section = $general_header_tab->add_section_element(
            array(
                'name'       => 'qodef_standard_extended_header_section',
                'title'      => esc_html__( 'Standard Extended Header', 'konsept-core' ),
                'dependency' => array(
                    'show' => array(
                        'qodef_header_layout' => array(
                            'values' => 'standard-extended',
                            'default_value' => ''
                        )
                    )
                )
            )
        );

        $section->add_field_element(
            array(
                'field_type'    => 'select',
                'name'          => 'show_extended_dropdown',
                'title'         => esc_html__( 'Show Extended Dropdown', 'konsept-core' ),
                'options'     => konsept_core_get_select_type_options_pool( 'yes_no' )
            )
        );
        $section->add_field_element(
            array(
                'field_type'    => 'text',
                'name'          => 'extended_dropdown_opener_label',
                'default_value' => '',
                'title'         => esc_html__('Extended Dropdown Opener Label', 'konsept-core'),
                'description'   => esc_html__('Set Extended Dropdown Opener Label, or leave empty for default value.', 'konsept-core'),
            )
        );

        $section->add_field_element(
            array(
                'field_type'   => 'select',
                'name'          => 'extended_dropdown_always_opened',
                'default_value' => '',
                'title'         => esc_html__('Extended Dropdown - Always Opened', 'konsept-core'),
                'options'     => konsept_core_get_select_type_options_pool( 'yes_no' )
            )
        );


        $section->add_field_element(
            array(
                'field_type'            => 'select',
                'name'            => 'disable_menu_area',
                'default_value'   => '',
                'title'           => esc_html__( 'Disable Menu Area', 'konsept-core' ),
                'description'     => esc_html__( 'Remove Menu Area in Extended Header Type', 'konsept-core' ),
                'options'         => konsept_core_get_select_type_options_pool( 'yes_no' )
            )
        );

        $section->add_field_element(
            array(
                'field_type'    => 'yesno',
                'name'          => 'qodef_standard_extended_header_in_grid',
                'title'         => esc_html__( 'Content in Grid', 'konsept-core' ),
                'description'   => esc_html__( 'Set content to be in grid', 'konsept-core' ),
                'default_value' => 'no'
            )
        );

        $section->add_field_element(
            array(
                'field_type'  => 'text',
                'name'        => 'qodef_standard_extended_header_height',
                'title'       => esc_html__( 'Header Height', 'konsept-core' ),
                'description' => esc_html__( 'Enter header height', 'konsept-core' ),
                'args'        => array(
                    'suffix' => esc_html__( 'px', 'konsept-core' )
                )
            )
        );

        $section->add_field_element(
            array(
                'field_type'  => 'color',
                'name'        => 'qodef_standard_extended_header_background_color',
                'title'       => esc_html__( 'Header Background Color', 'konsept-core' ),
                'description' => esc_html__( 'Enter header background color', 'konsept-core' ),
                'args'        => array(
                    'suffix' => esc_html__( 'px', 'konsept-core' )
                )
            )
        );

        $section->add_field_element(
            array(
                'field_type'    => 'select',
                'name'          => 'qodef_standard_extended_menu_position',
                'title'         => esc_html__( 'Menu position', 'konsept-core' ),
                'default_value' => 'right',
                'options'       => array(
                    'left'   => esc_html__( 'Left', 'konsept-core' ),
                    'center' => esc_html__( 'Center', 'konsept-core' ),
                    'right'  => esc_html__( 'Right', 'konsept-core' ),
                )
            )
        );
    }

    add_action( 'konsept_core_action_after_header_options_map', 'konsept_core_add_standard_extended_header_options', 10, 2 );
}