<?php

class StandardExtendedHeader extends KonseptCoreHeader {
    private static $instance;

    public function __construct() {
        $this->set_slug( 'standard-extended' );
        $this->default_header_height = 160;

        parent::__construct();
    }

    public static function get_instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}