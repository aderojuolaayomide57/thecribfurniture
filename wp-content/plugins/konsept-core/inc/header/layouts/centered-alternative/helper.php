<?php
if ( ! function_exists( 'konsept_core_add_centered_alternative_header_global_option' ) ) {
	/**
	 * This function set header type value for global header option map
	 */

	function konsept_core_add_centered_alternative_header_global_option( $header_layout_options ) {
		$header_layout_options['centered-alternative'] = array(
			'image' => KONSEPT_CORE_HEADER_LAYOUTS_URL_PATH . '/centered/assets/img/centered-header.png',
			'label' => esc_html__( 'Centered Alternative', 'konsept-core' )
		);

		return $header_layout_options;
	}

	add_filter( 'konsept_core_filter_header_layout_option', 'konsept_core_add_centered_alternative_header_global_option' );
}


if ( ! function_exists( 'konsept_core_register_centered_alternative_header_layout' ) ) {
	function konsept_core_register_centered_alternative_header_layout( $header_layouts ) {
		$header_layout = array(
			'centered-alternative' => 'CenteredAlternativeHeader'
		);

		$header_layouts = array_merge( $header_layouts, $header_layout );

		return $header_layouts;
	}

	add_filter( 'konsept_core_filter_register_header_layouts', 'konsept_core_register_centered_alternative_header_layout');
}