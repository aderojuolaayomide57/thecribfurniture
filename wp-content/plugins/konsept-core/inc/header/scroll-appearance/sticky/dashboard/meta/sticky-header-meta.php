<?php

if ( ! function_exists( 'konsept_core_add_sticky_header_meta_options' ) ) {
	function konsept_core_add_sticky_header_meta_options( $section, $custom_sidebars ) {
		
		if ( $section ) {
			
			$section->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_sticky_header_skin',
					'title'       => esc_html__( 'Sticky Header Skin', 'konsept-core' ),
					'description' => esc_html__( 'Choose a predefined sticky header style for header elements', 'konsept-core' ),
					'options'     => array(
						''      => esc_html__( 'Default', 'konsept-core' ),
						'none'  => esc_html__( 'None', 'konsept-core' ),
						'light' => esc_html__( 'Light', 'konsept-core' ),
						'dark'  => esc_html__( 'Dark', 'konsept-core' ),
					),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => '',
							),
						),
					),
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_sticky_header_scroll_amount',
					'title'       => esc_html__( 'Sticky Scroll Amount', 'konsept-core' ),
					'description' => esc_html__( 'Enter scroll amount for sticky header to appear', 'konsept-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'konsept-core' )
					),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_sticky_header_side_padding',
					'title'       => esc_html__( 'Sticky Header Side Padding', 'konsept-core' ),
					'description' => esc_html__( 'Enter side padding for sticky header area', 'konsept-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px or %', 'konsept-core' )
					),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_sticky_header_background_color',
					'title'       => esc_html__( 'Sticky Header Background Color', 'konsept-core' ),
					'description' => esc_html__( 'Enter sticky header background color', 'konsept-core' ),
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => ''
							)
						)
					)
				)
			);
			
			$section->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_sticky_header_custom_widget_area_one',
					'title'       => esc_html__( 'Choose Custom Sticky Header Widget Area One', 'konsept-core' ),
					'description' => esc_html__( 'Choose custom widget area to display in sticky header widget area', 'konsept-core' ),
					'options'     => $custom_sidebars,
					'dependency'  => array(
						'show' => array(
							'qodef_header_scroll_appearance' => array(
								'values'        => 'sticky',
								'default_value' => '',
							),
						),
					),
				)
			);
		}
	}
	
	add_action( 'konsept_core_action_after_header_scroll_appearance_meta_options_map', 'konsept_core_add_sticky_header_meta_options', 10, 2 );
}

if ( ! function_exists( 'konsept_core_add_sticky_header_logo_meta_options' ) ) {
	function konsept_core_add_sticky_header_logo_meta_options( $logo_tab, $header_logo_section ) {
		
		if ( $header_logo_section ) {
			
			$header_logo_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_sticky',
					'title'       => esc_html__( 'Logo - Sticky', 'konsept-core' ),
					'description' => esc_html__( 'Choose sticky logo image', 'konsept-core' ),
					'multiple'    => 'no'
				)
			);
		}
	}
	
	add_action( 'konsept_core_action_after_page_logo_meta_map', 'konsept_core_add_sticky_header_logo_meta_options', 10, 2 );
}