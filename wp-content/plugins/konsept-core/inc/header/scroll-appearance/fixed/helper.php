<?php

if ( ! function_exists( 'konsept_core_add_fixed_header_option' ) ) {
	/**
	 * This function set header scrolling appearance value for global header option map
	 */
	function konsept_core_add_fixed_header_option( $options ) {
		$options['fixed'] = esc_html__( 'Fixed', 'konsept-core' );

		return $options;
	}

	add_filter( 'konsept_core_filter_header_scroll_appearance_option', 'konsept_core_add_fixed_header_option' );
}