<?php
if ( ! function_exists( 'konsept_core_add_top_area_meta_options' ) ) {
	function konsept_core_add_top_area_meta_options( $page ) {
		$top_area_section = $page->add_section_element(
			array(
				'name'       => 'qodef_top_area_section',
				'title'      => esc_html__( 'Top Area', 'konsept-core' ),
				'dependency' => array(
					'hide' => array(
						'qodef_header_layout' => array(
							'values'        => konsept_core_dependency_for_top_area_options(),
							'default_value' => ''
						)
					)
				)
			)
		);

		$top_area_section->add_field_element(
			array(
				'field_type'  => 'select',
				'name'        => 'qodef_top_area_header',
				'title'       => esc_html__( 'Top Area', 'konsept-core' ),
				'description' => esc_html__( 'Enable top area', 'konsept-core' ),
				'options'     => konsept_core_get_select_type_options_pool( 'yes_no' )
			)
		);

		$top_area_options_section = $top_area_section->add_section_element(
			array(
				'name'        => 'qodef_top_area_options_section',
				'title'       => esc_html__( 'Top Area Options', 'konsept-core' ),
				'description' => esc_html__( 'Set desired values for top area', 'konsept-core' ),
				'dependency'  => array(
					'show' => array(
						'qodef_top_area_header' => array(
							'values'        => 'yes',
							'default_value' => 'no'
						)
					)
				)
			)
		);

        $top_area_options_section->add_field_element(
            array(
                'field_type'    => 'yesno',
                'name'          => 'qodef_top_area_header_in_grid',
                'title'         => esc_html__( 'Content in Grid', 'konsept-core' ),
                'description'   => esc_html__( 'Set content to be in grid', 'konsept-core' ),
                'default_value' => 'no'
            )
        );

        $top_area_options_section->add_field_element(
            array(
                'field_type'    => 'yesno',
                'name'          => 'qodef_top_area_bottom_border',
                'title'         => esc_html__( 'Bottom Border', 'konsept-core' ),
                'description'   => esc_html__( 'Enable bottom border on top area', 'konsept-core' ),
                'default_value' => 'no'
            )
        );

        $top_area_options_section->add_field_element(
            array(
                'field_type'    => 'yesno',
                'name'          => 'qodef_top_area_border_in_grid',
                'title'         => esc_html__( 'Bottom Border in Grid', 'konsept-core' ),
                'description'   => esc_html__( 'Set bottom border on top area in grid', 'konsept-core' ),
                'default_value' => 'no',
                'dependency'  => array(
                    'show' => array(
                        'qodef_top_area_bottom_border' => array(
                            'values'        => 'yes',
                            'default_value' => 'no'
                        )
                    )
                )
            )
        );

		$top_area_options_section->add_field_element(
			array(
				'field_type'  => 'color',
				'name'        => 'qodef_top_area_header_background_color',
				'title'       => esc_html__( 'Top Area Background Color', 'konsept-core' ),
				'description' => esc_html__( 'Choose top area background color', 'konsept-core' )
			)
		);

		$top_area_options_section->add_field_element(
			array(
				'field_type'  => 'text',
				'name'        => 'qodef_top_area_header_height',
				'title'       => esc_html__( 'Top Area Height', 'konsept-core' ),
				'description' => esc_html__( 'Enter top area height (default is 30px)', 'konsept-core' ),
				'args'        => array(
					'suffix' => esc_html__( 'px', 'konsept-core' )
				)
			)
		);

		$top_area_options_section->add_field_element(
			array(
				'field_type' => 'text',
				'name'       => 'qodef_top_area_header_side_padding',
				'title'      => esc_html__( 'Top Area Side Padding', 'konsept-core' ),
				'args'       => array(
					'suffix' => esc_html__( 'px or %', 'konsept-core' )
				)
			)
		);
	}

	add_action( 'konsept_core_action_after_page_header_meta_map', 'konsept_core_add_top_area_meta_options', 20 );
}