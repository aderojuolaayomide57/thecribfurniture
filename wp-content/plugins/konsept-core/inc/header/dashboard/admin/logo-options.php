<?php

if ( ! function_exists( 'konsept_core_add_logo_options' ) ) {
	function konsept_core_add_logo_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => KONSEPT_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'logo',
				'icon'        => 'fa fa-cog',
				'title'       => esc_html__( 'Logo', 'konsept-core' ),
				'description' => esc_html__( 'Global Logo Options', 'konsept-core' ),
				'layout'      => 'tabbed'
			)
		);

		if ( $page ) {

			$header_tab = $page->add_tab_element(
				array(
					'name'        => 'tab-header',
					'icon'        => 'fa fa-cog',
					'title'       => esc_html__( 'Header Logo Options', 'konsept-core' ),
					'description' => esc_html__( 'Set options for initial headers', 'konsept-core' )
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_logo_height',
					'title'       => esc_html__( 'Logo Height', 'konsept-core' ),
					'description' => esc_html__( 'Enter logo height', 'konsept-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px', 'konsept-core' )
					)
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'    => 'image',
					'name'          => 'qodef_logo_main',
					'title'         => esc_html__( 'Logo - Main', 'konsept-core' ),
					'description'   => esc_html__( 'Choose main logo image', 'konsept-core' ),
					'default_value' => defined( 'KONSEPT_ASSETS_ROOT' ) ? KONSEPT_ASSETS_ROOT . '/img/logo.png' : '',
					'multiple'      => 'no'
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_dark',
					'title'       => esc_html__( 'Logo - Dark', 'konsept-core' ),
					'description' => esc_html__( 'Choose dark logo image', 'konsept-core' ),
					'multiple'    => 'no'
				)
			);

			$header_tab->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_logo_light',
					'title'       => esc_html__( 'Logo - Light', 'konsept-core' ),
					'description' => esc_html__( 'Choose light logo image', 'konsept-core' ),
					'multiple'    => 'no'
				)
			);

			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_header_logo_options_map', $page, $header_tab );
		}
	}

	add_action( 'konsept_core_action_default_options_init', 'konsept_core_add_logo_options', konsept_core_get_admin_options_map_position( 'logo' ) );
}