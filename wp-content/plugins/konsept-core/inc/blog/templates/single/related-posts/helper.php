<?php

if ( ! function_exists( 'konsept_core_include_blog_single_related_posts_template' ) ) {
	/**
	 * Function which includes additional module on single posts page
	 */
	function konsept_core_include_blog_single_related_posts_template() {
		if ( is_single() ) {
			include_once KONSEPT_CORE_INC_PATH . '/blog/templates/single/related-posts/templates/related-posts.php';
		}
	}
	
	add_action( 'konsept_action_after_blog_post_item', 'konsept_core_include_blog_single_related_posts_template', 55 );  // permission 55 is set to define template position
}