<?php
$post_id       = get_the_ID();
$is_enabled    = konsept_core_get_post_value_through_levels( 'qodef_blog_single_enable_related_posts' );
$related_posts = konsept_core_get_custom_post_type_related_posts( $post_id, konsept_core_get_blog_single_post_taxonomies( $post_id ) );

if ( $is_enabled === 'yes' && ! empty( $related_posts ) && class_exists( 'KonseptCoreBlogListShortcode' ) ) { ?>
	<div id="qodef-related-posts">
        <h3 class="qodef-m-title"><?php echo esc_html__('Related Posts','konsept-core'); ?></h3>
		<?php
		$params = apply_filters( 'konsept_core_filter_blog_single_related_posts_params', array(
			'custom_class'      => 'qodef--no-bottom-space',
			'columns'           => '3',
			'posts_per_page'    => 3,
			'layout'            => 'standard',
			'additional_params' => 'id',
			'post_ids'          => $related_posts['items'],
			'title_tag'         => 'h4',
			'excerpt_length'    => '60',
            'images_proportion' => 'custom',
            'custom_image_width' => '800',
            'custom_image_height' => '983'
		) );
		
		echo KonseptCoreBlogListShortcode::call_shortcode( $params ); ?>
	</div>
<?php } ?>