<div class="qodef-e-media">
	<?php switch ( get_post_format() ) {
		case 'gallery':
			konsept_core_theme_template_part( 'blog', 'templates/parts/post-format/gallery' );
			break;
		case 'video':
			konsept_core_theme_template_part( 'blog', 'templates/parts/post-format/video' );
			break;
		case 'audio':
			konsept_core_theme_template_part( 'blog', 'templates/parts/post-format/audio' );
			break;
		default:
			konsept_core_template_part( 'blog/shortcodes/blog-list', 'templates/post-info/image','', $params );
			break;
	} ?>
</div>