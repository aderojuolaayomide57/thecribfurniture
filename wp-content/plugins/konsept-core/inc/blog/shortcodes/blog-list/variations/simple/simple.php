<?php

if ( ! function_exists( 'konsept_core_add_blog_list_variation_simple' ) ) {
	function konsept_core_add_blog_list_variation_simple( $variations ) {
		$variations['simple'] = esc_html__( 'Simple', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_blog_list_layouts', 'konsept_core_add_blog_list_variation_simple' );
}