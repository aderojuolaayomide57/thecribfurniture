<?php

if ( ! function_exists( 'konsept_core_add_blog_list_variation_standard_extended' ) ) {
	function konsept_core_add_blog_list_variation_standard_extended( $variations ) {
		$variations['standard-extended'] = esc_html__( 'Standard Extended', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_blog_list_layouts', 'konsept_core_add_blog_list_variation_standard_extended' );
}

if ( ! function_exists( 'konsept_core_load_blog_list_variation_standard_extended_assets' ) ) {
	function konsept_core_load_blog_list_variation_standard_extended_assets( $is_enabled, $params ) {
		
		if ( $params['layout'] === 'standard-extended' ) {
			$is_enabled = true;
		}
		
		return $is_enabled;
	}
	
	add_filter( 'konsept_core_filter_load_blog_list_assets', 'konsept_core_load_blog_list_variation_standard_extended_assets', 10, 2 );
}

if ( ! function_exists( 'konsept_core_register_blog_list_standard_extended_scripts' ) ) {
	/**
	 * Function that register modules 3rd party scripts
	 *
	 * @param array $scripts
	 *
	 * @return array
	 */
	function konsept_core_register_blog_list_standard_extended_scripts( $scripts ) {

		$scripts['wp-mediaelement'] = array(
			'registered'	=> true
		);
		$scripts['mediaelement-vimeo'] = array(
			'registered'	=> true
		);

		return $scripts;
	}

	add_filter( 'konsept_core_filter_blog_list_register_scripts', 'konsept_core_register_blog_list_standard_extended_scripts' );
}

if ( ! function_exists( 'konsept_core_register_blog_list_standard_extended_styles' ) ) {
	/**
	 * Function that register modules 3rd party scripts
	 *
	 * @param array $styles
	 *
	 * @return array
	 */
	function konsept_core_register_blog_list_standard_extended_styles( $styles ) {

		$styles['wp-mediaelement'] = array(
			'registered'	=> true
		);

		return $styles;
	}

	add_filter( 'konsept_core_filter_blog_list_register_styles', 'konsept_core_register_blog_list_standard_extended_styles' );
}