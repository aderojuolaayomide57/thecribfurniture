<?php

if ( ! function_exists( 'konsept_core_add_blog_list_variation_minimal' ) ) {
	function konsept_core_add_blog_list_variation_minimal( $variations ) {
		$variations['minimal'] = esc_html__( 'Minimal', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_blog_list_layouts', 'konsept_core_add_blog_list_variation_minimal' );
}