<?php

if ( ! function_exists( 'konsept_core_add_working_hours_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function konsept_core_add_working_hours_options() {
		$qode_framework = qode_framework_get_framework_root();

		$page = $qode_framework->add_options_page(
			array(
				'scope'       => KONSEPT_CORE_OPTIONS_NAME,
				'type'        => 'admin',
				'slug'        => 'working-hours',
				'icon'        => 'fa fa-book',
				'title'       => esc_html__( 'Working Hours', 'konsept-core' ),
				'description' => esc_html__( 'Global Working Hours Options', 'konsept-core' )
			)
		);

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_monday',
					'title'      => esc_html__( 'Working Hours For Monday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_tuesday',
					'title'      => esc_html__( 'Working Hours For Tuesday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_wednesday',
					'title'      => esc_html__( 'Working Hours For Wednesday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_thursday',
					'title'      => esc_html__( 'Working Hours For Thursday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_friday',
					'title'      => esc_html__( 'Working Hours For Friday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_saturday',
					'title'      => esc_html__( 'Working Hours For Saturday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_sunday',
					'title'      => esc_html__( 'Working Hours For Sunday', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'checkbox',
					'name'       => 'qodef_working_hours_special_days',
					'title'      => esc_html__( 'Special Days', 'konsept-core' ),
					'options'    => array(
						'monday'    => esc_html__( 'Monday', 'konsept-core' ),
						'tuesday'   => esc_html__( 'Tuesday', 'konsept-core' ),
						'wednesday' => esc_html__( 'Wednesday', 'konsept-core' ),
						'thursday'  => esc_html__( 'Thursday', 'konsept-core' ),
						'friday'    => esc_html__( 'Friday', 'konsept-core' ),
						'saturday'  => esc_html__( 'Saturday', 'konsept-core' ),
						'sunday'    => esc_html__( 'Sunday', 'konsept-core' ),
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type' => 'text',
					'name'       => 'qodef_working_hours_special_text',
					'title'      => esc_html__( 'Featured Text For Special Days', 'konsept-core' )
				)
			);

			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_working_hours_options_map', $page );
		}
	}

	add_action( 'konsept_core_action_default_options_init', 'konsept_core_add_working_hours_options', konsept_core_get_admin_options_map_position( 'working-hours' ) );
}