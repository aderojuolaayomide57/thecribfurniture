<?php

if ( ! function_exists( 'konsept_core_is_back_to_top_enabled' ) ) {
	function konsept_core_is_back_to_top_enabled() {
		return konsept_core_get_post_value_through_levels( 'qodef_back_to_top' ) !== 'no';
	}
}

if ( ! function_exists( 'konsept_core_add_back_to_top_to_body_classes' ) ) {
	function konsept_core_add_back_to_top_to_body_classes( $classes ) {
		$classes[] = konsept_core_is_back_to_top_enabled() ? 'qodef-back-to-top--enabled' : '';
		
		return $classes;
	}
	
	add_filter( 'body_class', 'konsept_core_add_back_to_top_to_body_classes' );
}

if ( ! function_exists( 'konsept_core_load_back_to_top' ) ) {
	/**
	 * Loads Back To Top HTML
	 */
	function konsept_core_load_back_to_top() {
		
		if ( konsept_core_is_back_to_top_enabled() ) {
			$parameters = array();
			
			konsept_core_template_part( 'back-to-top', 'templates/back-to-top', '', $parameters );
		}
	}
	
	add_action( 'konsept_action_before_wrapper_close_tag', 'konsept_core_load_back_to_top' );
}