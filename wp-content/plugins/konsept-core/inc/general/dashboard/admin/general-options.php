<?php

if ( ! function_exists( 'konsept_core_add_general_options' ) ) {
	/**
	 * Function that add general options for this module
	 */
	function konsept_core_add_general_options( $page ) {

		if ( $page ) {
			$page->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_page_background_color',
					'title'       => esc_html__( 'Page Background Color', 'konsept-core' ),
					'description' => esc_html__( 'Set background color', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_page_background_image',
					'title'       => esc_html__( 'Page Background Image', 'konsept-core' ),
					'description' => esc_html__( 'Set background image', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_background_repeat',
					'title'       => esc_html__( 'Page Background Image Repeat', 'konsept-core' ),
					'description' => esc_html__( 'Set background image repeat', 'konsept-core' ),
					'options'     => array(
						''          => esc_html__( 'Default', 'konsept-core' ),
						'no-repeat' => esc_html__( 'No Repeat', 'konsept-core' ),
						'repeat'    => esc_html__( 'Repeat', 'konsept-core' ),
						'repeat-x'  => esc_html__( 'Repeat-x', 'konsept-core' ),
						'repeat-y'  => esc_html__( 'Repeat-y', 'konsept-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_background_size',
					'title'       => esc_html__( 'Page Background Image Size', 'konsept-core' ),
					'description' => esc_html__( 'Set background image size', 'konsept-core' ),
					'options'     => array(
						''        => esc_html__( 'Default', 'konsept-core' ),
						'contain' => esc_html__( 'Contain', 'konsept-core' ),
						'cover'   => esc_html__( 'Cover', 'konsept-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'select',
					'name'        => 'qodef_page_background_attachment',
					'title'       => esc_html__( 'Page Background Image Attachment', 'konsept-core' ),
					'description' => esc_html__( 'Set background image attachment', 'konsept-core' ),
					'options'     => array(
						''       => esc_html__( 'Default', 'konsept-core' ),
						'fixed'  => esc_html__( 'Fixed', 'konsept-core' ),
						'scroll' => esc_html__( 'Scroll', 'konsept-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_content_padding',
					'title'       => esc_html__( 'Page Content Padding', 'konsept-core' ),
					'description' => esc_html__( 'Set padding that will be applied for page content in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_page_content_padding_mobile',
					'title'       => esc_html__( 'Page Content Padding Mobile', 'konsept-core' ),
					'description' => esc_html__( 'Set padding that will be applied for page content on mobile screens (1024px and below) in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'konsept-core' )
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_boxed',
					'title'         => esc_html__( 'Boxed Layout', 'konsept-core' ),
					'description'   => esc_html__( 'Set boxed layout', 'konsept-core' ),
					'default_value' => 'no'
				)
			);

			$boxed_section = $page->add_section_element(
				array(
					'name'       => 'qodef_boxed_section',
					'title'      => esc_html__( 'Boxed Layout Section', 'konsept-core' ),
					'dependency' => array(
						'hide' => array(
							'qodef_boxed' => array(
								'values'        => 'no',
								'default_value' => ''
							)
						)
					)
				)
			);

			$boxed_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_boxed_background_color',
					'title'       => esc_html__( 'Boxed Background Color', 'konsept-core' ),
					'description' => esc_html__( 'Set boxed background color', 'konsept-core' )
				)
			);

            $boxed_section->add_field_element(
                array(
                    'field_type'  => 'image',
                    'name'        => 'qodef_boxed_background_pattern',
                    'title'       => esc_html__( 'Boxed Background Pattern', 'konsept-core' ),
                    'description' => esc_html__( 'Set boxed background pattern', 'konsept-core' )
                )
            );

            $boxed_section->add_field_element(
                array(
                    'field_type'  => 'select',
                    'name'        => 'qodef_boxed_background_pattern_behavior',
                    'title'       => esc_html__( 'Boxed Background Pattern Behavior', 'konsept-core' ),
                    'description' => esc_html__( 'Set boxed background pattern behavior', 'konsept-core' ),
                    'options'     => array(
                        'fixed'  => esc_html__( 'Fixed', 'konsept-core' ),
                        'scroll' => esc_html__( 'Scroll', 'konsept-core' )
                    ),
                )
            );

			$page->add_field_element(
				array(
					'field_type'    => 'yesno',
					'name'          => 'qodef_passepartout',
					'title'         => esc_html__( 'Passepartout', 'konsept-core' ),
					'description'   => esc_html__( 'Enabling this option will display a passepartout around website content', 'konsept-core' ),
					'default_value' => 'no'
				)
			);

			$passepartout_section = $page->add_section_element(
				array(
					'name'       => 'qodef_passepartout_section',
					'title'      => esc_html__( 'Passepartout Section', 'konsept-core' ),
					'dependency' => array(
						'hide' => array(
							'qodef_passepartout' => array(
								'values'        => 'no',
								'default_value' => ''
							)
						)
					)
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'color',
					'name'        => 'qodef_passepartout_color',
					'title'       => esc_html__( 'Passepartout Color', 'konsept-core' ),
					'description' => esc_html__( 'Choose background color for passepartout', 'konsept-core' )
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'image',
					'name'        => 'qodef_passepartout_image',
					'title'       => esc_html__( 'Passepartout Background Image', 'konsept-core' ),
					'description' => esc_html__( 'Set background image for passepartout', 'konsept-core' )
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_passepartout_size',
					'title'       => esc_html__( 'Passepartout Size', 'konsept-core' ),
					'description' => esc_html__( 'Enter size amount for passepartout', 'konsept-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px or %', 'konsept-core' )
					)
				)
			);

			$passepartout_section->add_field_element(
				array(
					'field_type'  => 'text',
					'name'        => 'qodef_passepartout_size_responsive',
					'title'       => esc_html__( 'Passepartout Responsive Size', 'konsept-core' ),
					'description' => esc_html__( 'Enter size amount for passepartout for smaller screens (1024px and below)', 'konsept-core' ),
					'args'        => array(
						'suffix' => esc_html__( 'px or %', 'konsept-core' )
					)
				)
			);

			$page->add_field_element(
				array(
					'field_type'    => 'select',
					'name'          => 'qodef_content_width',
					'title'         => esc_html__( 'Initial Width of Content', 'konsept-core' ),
					'description'   => esc_html__( 'Choose the initial width of content which is in grid (applies to pages set to "Default Template" and rows set to "In Grid")', 'konsept-core' ),
					'options'       => konsept_core_get_select_type_options_pool( 'content_width', false ),
					'default_value' => '1100'
				)
			);

			// Hook to include additional options after module options
			do_action( 'konsept_core_action_after_general_options_map', $page );
			
			$page->add_field_element(
				array(
					'field_type'  => 'textarea',
					'name'        => 'qodef_custom_js',
					'title'       => esc_html__( 'Custom JS', 'konsept-core' ),
					'description' => esc_html__( 'Enter your custom Javascript here', 'konsept-core' )
				)
			);
		}
	}

	add_action( 'konsept_core_action_default_options_init', 'konsept_core_add_general_options', konsept_core_get_admin_options_map_position( 'general' ) );
}