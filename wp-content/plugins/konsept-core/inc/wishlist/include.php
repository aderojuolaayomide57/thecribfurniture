<?php

include_once KONSEPT_CORE_INC_PATH . '/wishlist/helper.php';

if ( ! function_exists( 'konsept_core_wishlist_include_widgets' ) ) {
	/**
	 * Function that includes widgets
	 */
	function konsept_core_wishlist_include_widgets() {
		foreach ( glob( KONSEPT_CORE_INC_PATH . '/wishlist/widgets/*/include.php' ) as $widget ) {
			include_once $widget;
		}
	}
	
	add_action( 'qode_framework_action_before_widgets_register', 'konsept_core_wishlist_include_widgets' );
}