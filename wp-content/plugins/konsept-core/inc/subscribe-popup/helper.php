<?php

if ( ! function_exists( 'konsept_core_get_subscribe_popup' ) ) {
	/**
	 * Loads subscribe popup HTML
	 */
	function konsept_core_get_subscribe_popup() {
		if ( konsept_core_get_option_value( 'admin', 'qodef_enable_subscribe_popup' ) === 'yes' && konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_contact_form' ) !== '' ) {
			konsept_core_load_subscribe_popup_template();
		}
	}

	// Get subscribe popup HTML
	add_action( 'konsept_action_before_wrapper_close_tag', 'konsept_core_get_subscribe_popup' );
}

if ( ! function_exists( 'konsept_core_load_subscribe_popup_template' ) ) {
	/**
	 * Loads HTML template with params
	 */
	function konsept_core_load_subscribe_popup_template() {
		$params                     = array();
		$params['title']            = konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_title' );
		$params['subtitle']         = konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_subtitle' );
		$background_image           = konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_background_image' );
		$params['content_style']    = ! empty( $background_image ) ? 'background-image: url(' . esc_url( wp_get_attachment_url( $background_image ) ) . ')' : '';
		$params['contact_form']     = konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_contact_form' );
		$params['enable_prevent']   = konsept_core_get_option_value( 'admin', 'qodef_enable_subscribe_popup_prevent' );
		$params['prevent_behavior'] = konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_prevent_behavior' );
		$params['disable_bg_on_mobile'] = konsept_core_get_option_value( 'admin', 'qodef_subscribe_popup_background_image_disable_on_mobile' );

		$holder_classes           = array();
		$holder_classes[]         = ( $params['disable_bg_on_mobile'] === 'yes' ) ? 'qodef-sp-disable-bg-mobile' : '';
		$holder_classes[]         = ! empty( $params['prevent_behavior'] ) ? 'qodef-sp-prevent-' . $params['prevent_behavior'] : 'qodef-sp-prevent-session';
		$params['holder_classes'] = implode( ' ', $holder_classes );

		echo konsept_core_get_template_part( 'subscribe-popup', 'templates/subscribe-popup', '', $params );
	}
}