(function ($) {
	"use strict";

	$(document).ready(function () {
		qodefShareIcons.init();
	});

	var qodefShareIcons = {
		init: function () {
			var $elements = $('.qodef-social-share .qodef-share-link')

			if ($elements.length) {
				$elements.append('<svg class="qodef-svg-circle"><circle cx="50%" cy="50%" r="49%"></circle></svg>');
			}
		}
	};

})(jQuery);