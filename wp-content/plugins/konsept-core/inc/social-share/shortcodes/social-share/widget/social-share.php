<?php

if ( ! function_exists( 'konsept_core_add_social_share_widget' ) ) {
	/**
	 * Function that add widget into widgets list for registration
	 *
	 * @param array $widgets
	 *
	 * @return array
	 */
	function konsept_core_add_social_share_widget( $widgets ) {
		$widgets[] = 'KonseptCoreSocialShareWidget';
		
		return $widgets;
	}
	
	add_filter( 'konsept_core_filter_register_widgets', 'konsept_core_add_social_share_widget' );
}

if ( class_exists( 'QodeFrameworkWidget' ) ) {
	class KonseptCoreSocialShareWidget extends QodeFrameworkWidget {
		
		public function map_widget() {
			$widget_mapped = $this->import_shortcode_options( array(
				'shortcode_base' => 'konsept_core_social_share'
			) );
			if( $widget_mapped ) {
				$this->set_base( 'konsept_core_social_share' );
				$this->set_name( esc_html__( 'Konsept Social Share', 'konsept-core' ) );
				$this->set_description( esc_html__( 'Add a social share element into widget areas', 'konsept-core' ) );
			}
		}
		
		public function render( $atts ) {
			$params = $this->generate_string_params( $atts );
			
			echo do_shortcode( "[konsept_core_social_share $params]" ); // XSS OK
		}
	}
}