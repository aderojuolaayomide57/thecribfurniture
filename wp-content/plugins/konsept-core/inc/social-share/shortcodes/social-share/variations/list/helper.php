<?php

if ( ! function_exists( 'konsept_core_add_social_share_variation_list' ) ) {
	function konsept_core_add_social_share_variation_list( $variations ) {
		
		$variations['list'] = esc_html__( 'List', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_social_share_layouts', 'konsept_core_add_social_share_variation_list' );
}
