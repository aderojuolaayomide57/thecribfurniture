<?php

if ( ! function_exists( 'konsept_core_add_social_share_variation_text' ) ) {
	function konsept_core_add_social_share_variation_text( $variations ) {
		
		$variations['text'] = esc_html__( 'Text', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_social_share_layouts', 'konsept_core_add_social_share_variation_text' );
}
