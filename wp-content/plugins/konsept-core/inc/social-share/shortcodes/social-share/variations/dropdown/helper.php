<?php

if ( ! function_exists( 'konsept_core_add_social_share_variation_dropdown' ) ) {
	function konsept_core_add_social_share_variation_dropdown( $variations ) {
		
		$variations['dropdown'] = esc_html__( 'Dropdown', 'konsept-core' );
		
		return $variations;
	}
	
	add_filter( 'konsept_core_filter_social_share_layouts', 'konsept_core_add_social_share_variation_dropdown' );
}
